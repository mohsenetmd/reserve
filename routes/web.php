<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Google Login
|--------------------------------------------------------------------------
 */


Auth::routes();
Route::get('/', function () {
    if (Auth::check()) {
        $user = \Illuminate\Support\Facades\Auth::user();
        return view('welcome', ['user' => $user->name]);
    }
    return view('welcome');
});


/*
|--------------------------------------------------------------------------
| Hotel
| Insert Delete Update Hotel
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->middleware('admin')->group(function () {
    Route::any('hotel', 'HotelController@show')->name('hotel.show');
    Route::get('hotel/insert', 'HotelController@insert')->name('hotel.insert');
    Route::get('hotel/update/{id}', 'HotelController@update')->name('hotel.update');
    Route::post('hotel/save', 'HotelController@save')->name('hotel.save');
    Route::put('hotel/save', 'HotelController@updated')->name('hotel.updated');
    Route::get('hotel/delete', 'HotelController@delete')->name('hotel.delete');
    Route::post('/hotel/savetime','HotelController@savetime')->name('hotel.savetime');
});



/*
|--------------------------------------------------------------------------
| Date
| Insert Delete Update Date
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    Route::get('date', 'DateController@show')->name('date.show');
    Route::post('date', 'DateController@show')->name('date.show');
    Route::get('date/insert', 'DateController@insert')->name('date.insert');
    Route::get('date/update', 'DateController@update')->name('date.update');
    Route::post('date/save', 'DateController@save')->name('date.save');
    Route::put('date/save', 'DateController@updated')->name('date.updated');
    Route::get('date/delete', 'DateController@delete')->name('date.delete');
});

/*
|--------------------------------------------------------------------------
| City
| Insert Delete Update City
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    Route::get('city', 'CityController@show')->name('city.show');
    Route::post('city', 'CityController@show')->name('city.show');
    Route::get('city/insert', function () {
        return view('city.insert');
    })->name('city.insert');
    Route::get('city/update', 'CityController@update')->name('city.update');
    Route::post('city/save', 'CityController@save')->name('city.save');
    Route::put('city/save', 'CityController@updated')->name('city.updated');
});


/*
|--------------------------------------------------------------------------
| Hotel Type
| Insert Delete Update Hotel Type
|--------------------------------------------------------------------------
 */


Route::prefix('admin')->group(function () {
    Route::get('hoteltype', 'HotelTypeController@show')->name('hoteltype.show');
    Route::post('hoteltype', 'HotelTypeController@show')->name('hoteltype.show');
    Route::get('hoteltype/insert', function () {
        return view('hoteltype.insert');
    })->name('hoteltype.insert');
    Route::get('hoteltype/update', 'HotelTypeController@update')->name('hoteltype.update');
    Route::post('hoteltype/save', 'HotelTypeController@save')->name('hoteltype.save');
    Route::put('hoteltype/save', 'HotelTypeController@updated')->name('hoteltype.updated');
});


/*
|--------------------------------------------------------------------------
| Bed Type
| Insert Delete Update Bed Type
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    Route::get('bedtype', 'BedTypeController@show')->name('bedtype.show');
    Route::post('bedtype', 'BedTypeController@show')->name('bedtype.show');
    Route::get('bedtype/insert', function () {
        return view('bedtype.insert');
    })->name('bedtype.insert');
    Route::get('bedtype/update/{id}', 'BedTypeController@update')->name('bedtype.update');
    Route::post('bedtype/save', 'BedTypeController@save')->name('bedtype.save');
    Route::put('bedtype/save', 'BedTypeController@updated')->name('bedtype.updated');
});


/*
|--------------------------------------------------------------------------
| Hotel Star
| Insert Delete Update Hotel Star
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    Route::get('hotelstar', 'HotelStarController@show')->name('hotelstar.show');
    Route::post('hotelstar', 'HotelStarController@show')->name('hotelstar.show');
    Route::get('hotelstar/insert', function () {
        return view('hotelstar.insert');
    })->name('hotelstar.insert');
    Route::get('hotelstar/update', 'HotelStarController@update')->name('hotelstar.update');
    Route::post('hotelstar/save', 'HotelStarController@save')->name('hotelstar.save');
    Route::put('hotelstar/save', 'HotelStarController@updated')->name('hotelstar.updated');
});


/*
|--------------------------------------------------------------------------
| Reserve Type
| Insert Delete Update Reserve Type
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    Route::get('reservetype', 'ReserveTypeController@show')->name('reservetype.show');
    Route::post('reservetype', 'ReserveTypeController@show')->name('reservetype.show');
    Route::get('reservetype/insert', function () {
        return view('reservetype.insert');
    })->name('reservetype.insert');
    Route::get('reservetype/update', 'ReserveTypeController@update')->name('reservetype.update');
    Route::post('reservetype/save', 'ReserveTypeController@save')->name('reservetype.save');
    Route::put('reservetype/save', 'ReserveTypeController@updated')->name('reservetype.updated');
});


/*
|--------------------------------------------------------------------------
| Food Type
| Insert Delete Update Food Type
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    Route::get('foodtype', 'FoodTypeController@show')->name('foodtype.show');
    Route::post('foodtype', 'FoodTypeController@show')->name('foodtype.show');
    Route::get('foodtype/insert', function () {
        return view('foodtype.insert');
    })->name('foodtype.insert');
    Route::get('foodtype/update', 'FoodTypeController@update')->name('foodtype.update');
    Route::post('foodtype/save', 'FoodTypeController@save')->name('foodtype.save');
    Route::put('foodtype/save', 'FoodTypeController@updated')->name('foodtype.updated');
});


/*
|--------------------------------------------------------------------------
| Room Type
| Insert Delete Update Room Type
|--------------------------------------------------------------------------
 */
Route::prefix('admin')->group(function () {
    Route::get('roomtype', 'RoomTypeController@show')->name('roomtype.show');
    Route::post('roomtype', 'RoomTypeController@show')->name('roomtype.show');
    Route::get('roomtype/insert', function () {
        return view('roomtype.insert');
    })->name('roomtype.insert');
    Route::get('roomtype/update', 'RoomTypeController@update')->name('roomtype.update');
    Route::post('roomtype/save', 'RoomTypeController@save')->name('roomtype.save');
    Route::put('roomtype/save', 'RoomTypeController@updated')->name('roomtype.updated');
});

/*
|--------------------------------------------------------------------------
| Room
| Insert Delete Update Room
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    Route::get('room', 'RoomController@show')->name('room.show');
    Route::post('room', 'RoomController@show')->name('room.show');
    Route::get('room/insert', 'RoomController@insert')->name('room.insert');
    Route::get('room/update', 'RoomController@update')->name('room.update');
    Route::post('room/save', 'RoomController@save')->name('room.save');
    Route::post('room/copy', 'RoomController@copy')->name('room.copy');
    Route::put('room/save', 'RoomController@updated')->name('room.updated');
    Route::get('room/delete', 'RoomController@delete')->name('room.delete');
});


/*
|--------------------------------------------------------------------------
| Save Reserve
|--------------------------------------------------------------------------
 */

Route::prefix('admin')->group(function () {
    //namayeh list reserve ha
    Route::get('savereserve', 'SaveReserveController@show')->name('savereserve.show');
    Route::post('savereserve', 'SaveReserveController@show')->name('savereserve.show');
    //update reserve ha
    Route::get('savereserve/update', 'SaveReserveController@upsavereserve')->name('savereserve.update');
    Route::put('savereserve/updatesave', 'SaveReserveController@updatesave')->name('savereserve.update');
    //save reserve ha
    Route::post('savereserve/save', 'SaveReserveController@save')->name('savereserve.save');
    //verify pardakht
});

Route::get('pardakht/verify', 'SaveReserveController@pardakhtVerify');
Route::post('pardakht', 'SaveReserveController@pardakht')->name('pardakht');


/*
|--------------------------------------------------------------------------
| Update Database
|--------------------------------------------------------------------------
 */


//Route::get('/update/hotel', 'UpdateController@HotelUpdate');
//Route::get('/update/room', 'UpdateController@RoomUpdate');
//Route::get('test', 'UpdateController@numberBed');

/*
|--------------------------------------------------------------------------
| User Dashboard
|--------------------------------------------------------------------------
 */

Route::prefix('user')->group(function () {
    Route::get('panel', 'UserController@show')->name('user.show');
    Route::get('update', 'UserController@update')->name('user.update');
    Route::put('update', 'UserController@updated')->name('user.updated');
    Route::get('change', 'UserController@change')->name('user.change');
    Route::put('change', 'UserController@changePassword')->name('user.changepassword');
    Route::get('logout', function () {
        \Illuminate\Support\Facades\Auth::logout();
        return redirect('/');
    })->name('user.logout');
});


/*
|--------------------------------------------------------------------------
| admin Dashboard
|--------------------------------------------------------------------------
 */

Route::prefix('panel')->group(function () {
    Route::get('admin', 'PanelController@show')->name('panel.show');
    Route::post('admin', 'PanelController@show')->name('panel.show');
    Route::get('update', 'PanelController@update')->name('panel.update');
    Route::put('update', 'PanelController@updated')->name('panel.updated');
});


/*
|--------------------------------------------------------------------------
| Fard
|--------------------------------------------------------------------------
 */

Route::get('fard/save', 'FardController@show')->name('fard.show');
Route::post('fard/save', 'FardController@save')->name('fard.save');


/*
|--------------------------------------------------------------------------
| Voucher
|--------------------------------------------------------------------------
 */

Route::post('resrerve/complete', 'SaveReserveController@complete')->name('reserve.complete');



/*
|--------------------------------------------------------------------------
| nemidoonam baraye chi hast
|--------------------------------------------------------------------------
 */

Route::post('voucher/ajax', 'VoucherController@ajax')->name('voucher.ajax');


Route::get('group/add/{id}','GroupChangesController@add')->name('group.add');
Route::get('group/edit/{id}','GroupChangesController@edit')->name('group.edit');
Route::get('group/copy/{id}','GroupChangesController@copy')->name('group.copy');
Route::get('group/del/{id}','GroupChangesController@del')->name('group.del');
Route::put('group/update','GroupChangesController@update')->name('group.update');

Route::get('/edit/time/hotel/{id}','HotelController@editTime')->name('hotel.edittime');


/*
|--------------------------------------------------------------------------
| pardakht
|--------------------------------------------------------------------------
 */

Route::post('payment/mobile-insert', 'PardakhtController@mobileInsert')->name('pardakht.mobileinsert');
Route::post('payment/person-insert', 'PardakhtController@fardInsert')->name('pardakht.fardinsert');
Route::post('payment/Inquiry', 'PardakhtController@inquiry')->name('pardakht.inquiry');
Route::post('payment/ajax', 'PardakhtController@ajax')->name('pardakht.ajax');
Route::get('payment/verify', 'PardakhtController@verify')->name('pardakht.verify');
Route::post('payment/link', 'PardakhtController@link')->name('pardakht.link');
Route::post('payment/discount', 'PardakhtController@discount')->name('pardakht.discount');

/*
|--------------------------------------------------------------------------
| discount
|--------------------------------------------------------------------------
 */

Route::get('discount/insert','DiscountController@insert')->name('discount.insert');
Route::post('discount/save','DiscountController@save')->name('discount.save');




Route::get('design','DateController@test');
