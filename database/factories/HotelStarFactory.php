<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HotelStar;
use Faker\Generator as Faker;

$factory->define(HotelStar::class, function (Faker $faker) {
    return [
        'name'=>$faker->numberBetween(0,5)
    ];
});
