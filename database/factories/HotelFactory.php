<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hotel;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'city_id'=>$faker->numberBetween(1,100),
        'hotel_star_id'=>$faker->numberBetween(1,5),
        'hotel_type_id'=>$faker->numberBetween(1,20),
        'admin_created' => $faker->name,
        'admin_updated' => $faker->name
    ];
});
