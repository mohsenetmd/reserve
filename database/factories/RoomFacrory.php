<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'hotel_id' => $faker->numberBetween(5, 50),
        'admin_created' => $faker->name,
        'admin_updated' => $faker->name,
        'reserve_type_id' => $faker->numberBetween(1, 10),
        'food_type_id' => $faker->numberBetween(1, 10),
        'bed_type_id' => $faker->numberBetween(1, 10),
        'room_type_id' => $faker->numberBetween(1, 10),
        'person_number' => $faker->numberBetween(1, 10),
        'more_bed' => $faker->boolean,
        'bed_number' => $faker->numberBetween(1, 10),
        'bed_price' => $faker->numberBetween(10000, 100000),
        'child' => $faker->boolean,
        'child_number' => $faker->numberBetween(1, 10),
        'child_price' => $faker->numberBetween(10000, 100000),
        'price' => $faker->numberBetween(10000, 1000000),
        'discount_price' => $faker->numberBetween(10000, 1000000),
        'breakfast' => $faker->boolean,
        'breakfast_price' => $faker->numberBetween(10000, 100000),
        'lunch_price' => $faker->numberBetween(10000, 100000),
        'dinner_price' => $faker->numberBetween(10000, 100000),
        'sort' => $faker->numberBetween(1,20),
        'sample_data' => $faker->boolean
    ];
});
