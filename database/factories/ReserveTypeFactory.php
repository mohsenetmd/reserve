<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ReserveType;
use Faker\Generator as Faker;

$factory->define(ReserveType::class, function (Faker $faker) {
    return [
        'name'=>$faker->name
    ];
});
