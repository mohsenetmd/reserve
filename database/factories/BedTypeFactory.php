<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BedType;
use Faker\Generator as Faker;

$factory->define(BedType::class, function (Faker $faker) {
    return [
        'name'=>$faker->name
    ];
});
