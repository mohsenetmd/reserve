<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HotelType;
use Faker\Generator as Faker;

$factory->define(HotelType::class, function (Faker $faker) {
    return [
        'name'=>$faker->name
    ];
});
