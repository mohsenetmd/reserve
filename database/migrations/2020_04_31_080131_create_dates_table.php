<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dates', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('room_id');
            $table->unsignedBigInteger('reserve_type_id');
            $table->string('admin_created')->default(0);
            $table->string('admin_updated')->default(0);
            $table->integer('number');
            $table->integer('bed_price')->nullable();
            $table->integer('child_price')->nullable();
            $table->integer('discount_price')->default(0);
            $table->string('date_booking_start')->nullable();
            $table->string('date_booking_end')->nullable();
            $table->boolean('one_night')->default(0)->nullable();
            $table->boolean('status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('reserve_type_id')->references('id')->on('reserve_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dates');
    }
}
