<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buys', function (Blueprint $table) {
            $table->id('reserve_id');
            $table->integer('user_id');
            $table->integer('otagh_id');
            $table->integer('hotel_id');
            $table->string('json_date');
            $table->string('price_pay');
            $table->string('json_breakfast');
            $table->string('json_lunch');
            $table->string('json_dinner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buys');
    }
}
