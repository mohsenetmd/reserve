<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_hotels', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('mobile_number');
            $table->string('name');
            $table->string('family');
            $table->string('post_number');
            $table->string('melli_number');
            $table->string('password_number');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_hotels');
    }
}
