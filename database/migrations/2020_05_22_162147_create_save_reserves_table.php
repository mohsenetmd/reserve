<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaveReservesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('save_reserves', function (Blueprint $table) {
            $table->id();
            $table->string('room')->default(0);
            $table->string('total')->default(0);
            $table->string('tedad')->default(0);
            $table->string('online')->default(0);
            $table->string('total_bed_price')->default(0);
            $table->string('voucher')->default(0);
            $table->string('status')->default(0);
            $table->string('condition')->default(0);
            $table->string('admin_created')->default(0);
            $table->string('admin_updated')->default(0);
            $table->string('date_booking_start')->nullable();
            $table->string('date_booking_end')->nullable();
            $table->string('ref_id')->nullable();
            $table->string('time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('save_reserves');
    }
}
