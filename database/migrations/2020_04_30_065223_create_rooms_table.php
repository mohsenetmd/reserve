<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
                $table->id('id');
                $table->unsignedBigInteger('hotel_id');
                $table->string('pic');
                $table->unsignedBigInteger('room_number')->default(0)->nullable();
                $table->string('admin_created')->default(0);
                $table->string('admin_updated')->default(0);
                $table->unsignedBigInteger('reserve_type_id')->default(0);
                $table->unsignedBigInteger('room_type_id')->default(0);
                $table->unsignedBigInteger('bed_type_id')->default(0);
                $table->integer('person_number')->default(0);
                $table->boolean('more_bed')->default(0);
                $table->string('bed_number')->nullable();
                $table->integer('bed_price')->nullable();
                $table->boolean('child')->default(0);
                $table->integer('child_number')->nullable();
                $table->integer('child_price')->nullable();
                $table->string('price')->default(0);
                $table->string('discount_price')->default(0);
                $table->boolean('breakfast')->default(0);
                $table->boolean('lunch')->default(0);
                $table->boolean('dinner')->default(0);
                $table->integer('breakfast_price')->nullable();
                $table->integer('lunch_price')->nullable();
                $table->integer('dinner_price')->nullable();
                $table->unsignedBigInteger('food_type_id')->nullable();
                $table->integer('price_food_type')->nullable();
                $table->boolean('sample_data')->nullable();
                $table->boolean('status')->default(1)->nullable();
                $table->unsignedBigInteger('sort')->nullable();
                $table->unsignedBigInteger('wp_postmeta')->nullable();
                $table->timestamps();
                $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
                $table->foreign('room_type_id')->references('id')->on('room_types')->onDelete('cascade');
                $table->foreign('reserve_type_id')->references('id')->on('reserve_types')->onDelete('cascade');
                $table->foreign('bed_type_id')->references('id')->on('bed_types')->onDelete('cascade');
                $table->foreign('food_type_id')->references('id')->on('food_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
