<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('hotel_star_id');
            $table->unsignedBigInteger('hotel_type_id');
            $table->string('start_price');
            $table->string('wp_post_id');
            $table->string('admin_created');
            $table->string('admin_updated');
            $table->boolean('status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('hotel_star_id')->references('id')->on('hotel_stars')->onDelete('cascade');
            $table->foreign('hotel_type_id')->references('id')->on('hotel_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
