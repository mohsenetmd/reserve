<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<=1000;$i++){
        DB::table('admins')->insert([
            'name' => Str::random(10),
            'family' => Str::random(10),
            'user_name' => Str::random(10),
            'password' => Hash::make('password'),
        ]);}
    }
}
