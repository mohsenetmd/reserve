import Home from "./components/Home";
import Hotel from "./components/Hotel";
import Otagh from "./components/Otagh";
import Date from "./components/Date";
import User from "./components/User";
import Buy from "./components/Buy";
import Admin from "./components/Admin";
import Reserve from "./components/Reserve";

let perfix_patch='/panel/';
export default {
    mode:'history',
    linkActiveClass:'active',
    routes:[
        {
            path:perfix_patch+'home',
            component:Home,
            name:'home'
        },
        {
            path:perfix_patch+'hotel',
            component:Hotel,
            name:'hotel'
        },
        {
           path:perfix_patch+'otagh',
           component:Otagh,
            name:'otagh'
        },

        {
            path:perfix_patch+'date',
            component:Date,
            name:'date'
        },
        {
            path:perfix_patch+'user',
            component:User,
            name:'user'
        },
        {
            path:perfix_patch+'buy',
            component:Buy,
            name:'buy'
        },
        {
            path:perfix_patch+'admin',
            component:Admin,
            name:'admin'
        },
        {
            path:perfix_patch+'reserve',
            component:Reserve,
            name:'reserve'
        }
    ]
}
