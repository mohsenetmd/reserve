@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست رزروها</h1>
    <hr>
    <table class="table table-bordered data-table" style="zoom:0.9;">
        <thead>
        <tr>
            <th></th>
			<th>شماره واچر</th>
			<th>نام</th>
			<th>تاریخ درخواست رزرو</th>
			<th>ساعت رزرو</th>
			<th>ساعت پاسخگویی</th>
            <th>تاریخ ورود</th>
            <th>تاریخ خروج</th>
            <th>شماره موبایل رزرو کننده</th>
            <th>وضعیت</th>
            <th>ویرایش</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

@endsection
@section('script')
    <script type="text/javascript">
	        function format(d) {
            // `d` is the original data object for the row
			console.log(d);
			var show;
			show='<div class="container"><div class="row">';
			show=show + '<div class=col-lg-3>' + d.hotel + '</div>';
			show=show + '<div class=col-lg-3>' + d.room + '</div>';
			show=show + '<div class=col-lg-6>پاسخ دهنده استعلام:' + d.admin + '</div>';
			show= show + '</div></div>';
			return show;
        	}


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                "order": [[ 1, "desc" ]],
                stateSave: true,
                ajax: {
                    url: "{{route('savereserve.show')}}",
                    method: 'POST'
                },
                columns: [
					{
                        className: 'details-control',
                        orderable: false,
                        data: null,
                        defaultContent: '+'
                    },
                    {data: 'voucher', name: 'voucher'},
					{data: 'fard', name: 'fard'},
					{data: 'jalali', name: 'jalali'},
					{data: 'houre', name: 'houre'},
					{data: 'houre_update', name: 'houre_update'},
                    {data: 'date_booking_start', name: 'date_booking_start'},
                    {data: 'date_booking_end', name: 'date_booking_end'},
					{data: 'user', name: 'user', orderable: false, searchable: false},
                    {data: 'condition', name: 'condition'},
                    {data: 'edit', name: 'edit', orderable: false, searchable: false},
                ]
            });
			 $('.data-table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        });
    </script>
@endsection
