@extends('layouts.app')
@section('content')
    <form method="get" action="{{ route('reserve.showhotel') }}">
        <div id="app">
            <date-component></date-component>
        </div>
        <div class="form-group col-lg-4">
            <label>شهر</label>
            <select class="city_search form-control" name="city_id">
                @foreach($city as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <input type="text" name="id" value="">
        <button type="submit">ارسال</button>
    </form>
@endsection
@section('script')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $('#fileupload').fileupload();
        $(document).ready(function() {
            $('.city_search').select2();
            $('.star_search').select2();
            $('.type_search').select2();
        });
    </script>
@endsection

