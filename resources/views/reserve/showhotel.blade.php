@extends('layouts.app')
@section('content')
    {{ var_dump($total) }}
    <div id="app">
        <date-component></date-component>
    </div>
    <input type="hidden" class="getinfo" value="test">
    <input type="hidden" class="wp_post_id" value="12431">
    <div class="writeinfo">
    </div>
    <div class="postbutton">ارسال</div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".postbutton").click(function () {
                $.ajax({
                    /* the route pointing to the post function */
                    url: '/reserve/ajax',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, message: $(".getinfo").val(), id: $(".wp_post_id").val(),date_booking_start: $("input[name='date_booking_start']").val()
                    ,date_booking_end: $("input[name='date_booking_end']").val()},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                        console.log(data);
                        $(".writeinfo").html(data);
                    }
                });
            });
        });
    </script>
@endsection
