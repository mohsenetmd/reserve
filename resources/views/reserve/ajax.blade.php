@extends('layouts.app')
@section('content')
    <div id="app">
        <date-component></date-component>
    </div>
    <input type="hidden" class="wp_post_id" value="{{ request()->wp_post_id }}">
    <input type="hidden" name="id" value="{{ request()->wp_post_id }}">
    <div class="writeinfo">
    </div>
    <div class="btn btn-danger postbutton">ارسال</div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".postbutton").click(function () {
                $.ajax({
                    /* the route pointing to the post function */
                    url: '/reserve/ajax',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, message: $(".getinfo").val(), id: $(".wp_post_id").val(),date_booking_start: $("input[name='date_booking_start']").val()
                        ,date_booking_end: $("input[name='date_booking_end']").val()},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                        $('.writeinfo').html(data);
                    }
                });
            });
        });
        function addperson(n) {
            let discount_price = $('.discount_price');
            let person_number = $('.person_number');
            let bed_number = $('.bed_number');
            let bed_price = $('.bed_price');
            let tedad = $('.tedad');
            let input_tedad = $('.input_tedad');
            let total_bed_price = $('.total_bed_price');
            let input_total_bed_price = $('.input_total_bed_price');
            if(parseInt(($(tedad[n]).html()))< (parseInt(($(person_number[n]).html())) + parseInt(($(bed_number[n]).html())))){
                let inputperson=parseInt($(tedad[n]).html());
                if(parseInt(($(tedad[n]).html()))>= parseInt(($(person_number[n]).html()))){
                    let inputbed=parseInt($(total_bed_price[n]).html())+parseInt($(bed_price[n]).html());
                    $(total_bed_price[n]).html(inputbed);
                    $(input_total_bed_price[n]).val(inputbed);
                }
                inputperson = inputperson + 1;
                $(tedad[n]).html(inputperson);
                $(input_tedad[n]).val(inputperson);
            }
        }
        function minusperson(n) {
            let discount_price = $('.discount_price');
            let person_number = $('.person_number');
            let bed_number = $('.bed_number');
            let bed_price = $('.bed_price');
            let tedad = $('.tedad');
            let input_tedad = $('.input_tedad');
            let total_bed_price = $('.total_bed_price');
            let input_total_bed_price = $('.input_total_bed_price');
            if(parseInt($(tedad[n]).html()) > 0){
                let inputperson=parseInt($(tedad[n]).html());
                if(parseInt(($(tedad[n]).html()))> parseInt(($(person_number[n]).html()))){
                    let inputbed=parseInt($(total_bed_price[n]).html())-parseInt($(bed_price[n]).html());
                    $(total_bed_price[n]).html(inputbed);
                    $(input_total_bed_price[n]).val(inputbed);
                }
                inputperson = inputperson - 1;
                $(tedad[n]).html(inputperson);
                $(input_tedad[n]).val(inputperson);
            }
        }
    </script>
@endsection
