@extends('layouts.app')
@section('content')
    <div id="app2" class="container form-otah">
        <form method="post" action="{{route('reserve.complete')}}">
            @csrf
            <label>تعداد افراد</label>
            <button @click="person_number++" v-if="person_number<person_number_max">افزودن</button>
            <button @click="person_number++" v-if="person_number>=person_number_max" disabled>افزودن</button>
            @{{ persion_number }}
            <button @click="person_number--" v-if="person_number<=0" disabled>کاهش</button>
            <button @click="person_number--" v-if="person_number>0">کاهش</button>

            @if( $totalPrice['more_bed'] == 1)
                <label>تعداد تخت</label>
                <button @click="bednumberplus()" v-if="bed_number<bed_number_max">افزودن</button>
                <button v-if="bed_number>=bed_number_max" disabled>افزودن</button>
                @{{ bed_number }}
                <button v-if="bed_number<=0" disabled>کاهش</button>
                <button @click="bednumberminus()" v-if="bed_number>0">کاهش</button>
            @endif

            @if( $totalPrice['child'] == 1)
                <label>تعداد کودک</label>
                <button @click="childnumberplus()" v-if="child_number<child_number_max">افزودن</button>
                <button v-if="child_number>=child_number_max" disabled>افزودن</button>
                @{{ child_number }}
                <button v-if="child_number<=0" disabled>کاهش</button>
                <button @click="childnumberminus()" v-if="child_number>0">کاهش</button>
            @endif

            @if( $totalPrice['breakfast'] == 0)
                @if( $totalPrice['breakfast_price'] != 0)
                    <label>صبحانه</label>
                    <input @click="breakfastfunction()" type="checkbox">
                @endif
            @endif
            @if( $totalPrice['dinner'] == 0)
                @if( $totalPrice['dinner_price'] != 0)
                    <label>ناهار</label>
                    <input @click="dinnerfunction()" type="checkbox">
                @endif
            @endif
            @if( $totalPrice['lunch'] == 0)
                @if( $totalPrice['lunch_price'] != 0)
                    <label>شام</label>
                    <input @click="lunchfunction()" type="checkbox">
                @endif
            @endif


            <p>هزینه اتاق</p>
            @{{ price }}
            <p>هزینه اتاق با تخفیف</p>
            @{{ discount_price }}
            <p>میزان تخفیف</p>
            @{{ price - discount_price }}
            <input type="hidden" name="bed_number" v-model="bed_number">
            <input type="hidden" name="bed_price" v-model="bed_price_total">
            <input type="hidden" name="breakfast" v-model="breakfast">
            <input type="hidden" name="breakfast_price" v-model="breakfast_price">
            <input type="hidden" name="child_number" v-model="child_number">
            <input type="hidden" name="child_price" v-model="child_price_total">
            <input type="hidden" name="date_booking_end" value="{{$dateBookingEnd}}">
            <input type="hidden" name="date_booking_start" value="{{$dateBookingStart}}">
            <input type="hidden" name="dinner" v-model="dinner">
            <input type="hidden" name="dinner_price" v-model="dinner_price">
            <input type="hidden" name="lunch" v-model="lunch">
            <input type="hidden" name="lunch_price" v-model="lunch_price">
            <input type="hidden" name="person_number" v-model="person_number">
            <input type="hidden" name="room_id" value="{{ $room_id }}">
            @if( $totalPrice['more_bed'] == 1)
                <p>هزینه تخت اضافه</p>
                @{{ bed_price_total }}
            @endif
            @if( $totalPrice['child'] == 1)
                <p>هزینه کودک</p>
                @{{ child_price_total }}
            @endif
            @if( $totalPrice['breakfast'] == 0)
                @if( $totalPrice['breakfast_price'] != 0)
                    <p v-if="breakfast">@{{ breakfast_price }}</p>
                @endif
            @endif
            @if( $totalPrice['dinner'] == 0)
                @if( $totalPrice['dinner_price'] != 0)
                    <p v-if="lunch">@{{ lunch_price }}</p>
                @endif
            @endif
            @if( $totalPrice['lunch'] == 0)
                @if( $totalPrice['lunch_price'] != 0)
                    <p v-if="dinner">@{{ dinner_price }}</p>
                @endif
            @endif
            <button type="submit" class="btn btn-success">ارسال</button>
        </form>
    </div>




    <script>
        let app2 = new Vue({
            el: '#app2',
            data: {
                bed_number: 0,
                bed_number_max: {{ $bed_number }},
                breakfast: false,
                lunch: false,
                dinner: false,
                breakfast_price:{{ $totalPrice['breakfast_price'] }},
                lunch_price:{{ $totalPrice['lunch_price'] }},
                dinner_price:{{ $totalPrice['dinner_price'] }},
                bed_price: {{ $totalPrice['bed_price'] }},
                bed_price_total: '',
                child_number: 0,
                child_number_max:{{ $child_number }},
                child_price: {{ $totalPrice['child_price'] }},
                child_price_total: '',
                discount_price: {{ $totalPrice['discount_price'] }},
                person_number: '',
                person_number_max: {{ $person_number }},
                price: {{ $totalPrice['price'] }},
            },
            methods: {
                // roomnumberplus() {
                //     this.room_number=this.room_number+1;
                //     this.person_number_max_total = this.person_number_max*this.room_number;
                //     this.bed_number_max_total = this.bed_number_max*this.room_number;
                //     this.child_number_max_total = this.child_number_max*this.room_number;
                //     this.discount_price_total = this.discount_price*this.room_number;
                //     this.price_total = this.price*this.room_number;
                // },
                // roomnumberminus() {
                //     this.room_number=this.room_number-1;
                //     this.person_number_max_total = this.person_number_max*this.room_number;
                //     this.bed_number_max_total = this.bed_number_max*this.room_number;
                //     this.child_number_max_total = this.child_number_max*this.room_number;
                //     if(this.person_number>=this.person_number_max_total){
                //         this.person_number=this.person_number_max_total;
                //     }
                //     if(this.bed_number>=this.bed_number_max_total){
                //         this.bed_number=this.bed_number_max_total;
                //         this.bed_price_total=this.bed_price*this.bed_number;
                //     }
                //     if(this.child_number>=this.child_number_max_total){
                //         this.child_number=this.child_number_max_total;
                //         this.child_price_total=this.child_price*this.child_number;
                //     }
                //     this.discount_price_total = this.discount_price*this.room_number;
                //     this.price_total = this.price*this.room_number;
                // },
                bednumberplus() {
                    this.bed_number = this.bed_number + 1;
                    this.person_number_max = this.person_number_max + 1;
                    this.bed_price_total = this.bed_price * this.bed_number;
                },
                bednumberminus() {
                    this.bed_number = this.bed_number - 1;
                    this.bed_price_total = this.bed_price * this.bed_number;
                },
                childnumberplus() {
                    this.child_number = this.child_number + 1;
                    this.child_price_total = this.child_price * this.child_number;
                },
                childnumberminus() {
                    this.child_number = this.child_number - 1;
                    this.child_price_total = this.child_price * this.child_number;
                },
                breakfastfunction() {
                    this.breakfast = !this.breakfast;
                },
                lunchfunction() {
                    this.lunch = !this.lunch;
                },
                dinnerfunction() {
                    this.dinner = !this.dinner;
                }
            }
        });
    </script>
@endsection
