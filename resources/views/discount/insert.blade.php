@extends('layouts.app')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li> {{$error}}</li>
            @endforeach
        </ul>
    @endif
    <style>
        .required:after {
            content: " *";
            color: red;
        }
    </style>
    <form method="post" action="{{route('discount.save')}}">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <label class="required">انتخاب هتل ها</label>
                <select class="js-example-basic-multiple" name="hotel_id[]" multiple="multiple"
                        style="width:100%;height:300px;">
                    @for($i=0;$i<count($hotel);$i++)
                        <option value="{{ $hotel[$i]->id }}">{{ $hotel[$i]->name }}</option>
                    @endfor
                </select>
            </div>
            <div class="col-lg-4">
                <label class="required">کد تخفیف</label>
                <input class="form-control" type="text" name="discount_code" required>
            </div>
            <div class="col-lg-4">
                <label class="required">تعداد استفاده</label>
                <input class="form-control" type="text" name="number_use" required>
            </div>
            <div class="col-lg-4">
                <label class="required">درصد تخفیف</label>
                <input class="form-control" type="text" name="percent" required>
            </div>
            <div class="col-lg-4">
                <label class="required">مبلغ تخفیف</label>
                <input class="form-control" type="text" name="price" required>
            </div>
            <div class="col-lg-10">
                <label>توضیحات</label>
                <textarea class="form-control" name="detail" placeholder="توضیحات خود را وارد کنید"></textarea>
            </div>
        </div>
        <div id="app">
            <date-component></date-component>
        </div>
        <button class="btn btn-primary" type="submit">ارسال</button>
    </form>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endsection
