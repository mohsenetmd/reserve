@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <form action="{{route('user.updated')}}" method="POST" style="">
        @csrf
        @method('put')
        <h1 class="titr">وارد کردن مشخصات کاربری</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نام کاربری</label>
                <input type="text" class="form-control" name="name" required value="{{$user->name}}">
            </div>
            <div class="form-group col-lg-4">
                <label>نام و نام خانوادگی</label>
                <input type="text" class="form-control" name="family" required value="{{$user->family}}">
            </div>
            <div class="form-group col-lg-4">
                <label>شماره موبایل</label>
                <input type="text" class="form-control" name="mobile" required value="{{$user->mobile}}">
            </div>
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
@endsection
