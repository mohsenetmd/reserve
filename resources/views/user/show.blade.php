@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                نام و نام خانوادگی
            </div>
            <div class="col-md-3">
                {{ $user->family }}
            </div>
            <div class="col-md-3">
                شماره موبایل
            </div>
            <div class="col-md-3">
                {{ $user->mobile }}
            </div>
            <div class="col-md-3">
                ایمیل
            </div>
            <div class="col-md-3">
                {{ $user->email }}
            </div>
            <div>
            <a href="{{route('user.update')}}" class="btn btn-primary">بروز رسانی اطلاعات کاربران</a>
            <a href="{{route('user.changepassword')}}" class="btn btn-primary">تغییر پسورد</a>
            <a href="{{route('user.logout')}}" class="btn btn-primary">خروج</a>
            </div>
        </div>
    </div>


@endsection
