@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <form action="{{route('user.changepassword')}}" method="POST" style="">
        @csrf
        @method('put')
        <h1 class="titr">وارد کردن مشخصات کاربری</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>رمز عبور</label>
                <input v-validate="'required'" type="password" class="form-control" name="password" required>
            </div>
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
    <script>

    </script>
@endsection
