@extends('layouts.app')
@section('content')
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست نوع رزرو</h1>
    <a class="btn btn-primary" href="{{route('reservetype.insert')}}">افزودن</a>
    <hr>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>نوع رزرو</th>
            <th>تارخ ایجاد</th>
            <th>تاریخ بروزرسانی</th>
            <th>مشاهده</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

@endsection
@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('reservetype.show')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'link_reservetype', name: 'link_reservetype', orderable: false, searchable: false},
                ]
            });
            console.log(table);
        });
    </script>
@endsection
