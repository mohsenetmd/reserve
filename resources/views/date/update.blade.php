@extends('layouts.app')
@section('content')
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form method="post" action="{{route('date.updated')}}">
                    @csrf
                    @method('put')
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات اتاق</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    @foreach($reservetype as $item)
                                        <option value="{{$item->id}}"
                                        @if($date->reserve_type_id == $item->id)
                                            selected
                                            @endif
                                            >{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد اتاق</label>
                                <input type="text" class="form-control" name="number" required
                                       value="{{$date->number}}">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت مصوب</label>
                                <input type="text" class="form-control" name="price"
                                       value="{{$date->price}}"
                                       required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نرخ واریزی مسافر</label>
                                <input type="text" class="form-control" name="discount_price"
                                       value="{{$date->discount_price}}"
                                       required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نرخ واریزی هتل</label>
                                <input type="text" class="form-control" name="hotel_price"
                                       value="{{$date->hotel_price}}"
                                       required>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price" value="{{$date->bed_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price" value="{{$date->child_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4" style="display: none;">
                                <label>نوع غذا</label>
                                <select class="foodtype_search form-control" name="food_type_id">
                                    @foreach($foodtype as $item)
                                        <option
                                            value="{{$item->id}}"
                                                @if($date->food_type_id == $item->id)
                                                selected
                                            @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>تاریخ شروع</label>
                                <input type="text" class="form-control" name="date_booking_start" value="{{$date->date_booking_start}}">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>تاریخ پایان</label>
                                <input type="text" class="form-control" name="date_booking_end" value="{{$date->date_booking_end}}">
                            </div>
                            <input type="hidden" class="form-control" name="room_id" value="{{request()->room_id}}">
                            <input type="hidden" class="form-control" name="date_id" value="{{request()->date_id}}">
                            <input type="hidden" class="form-control" name="admin_created" value="{{$date->admin_created}}">
                            <input type="hidden" class="form-control" name="admin_updated" value="{{ Auth::user()->name }}">
                        </div>
                    </div>
                    <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>

    </script>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
