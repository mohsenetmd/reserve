@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست تاریخ ها</h1>
    <a class="btn btn-primary" href="{{route('date.insert',['room_id'=>request()->room_id])}}">افزودن</a>
    <a class="btn btn-success" href="{{route('room.show',['hotel_id'=>$hotel_id])}}" style="float: left;margin-left: 20px;">بازگشت</a>
    <hr>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>بروز رسانی</th>
            <th>قیمت با تخفیف</th>
            <th>تاریخ شروع</th>
            <th>تاریخ پایان</th>
            <th>تاریخ بروزرسانی</th>
            <th>مشاهده</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "https://hihotel.org/admin/date?room_id={{request()->room_id}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'admin_updated', name: 'admin_updated'},
                    {data: 'discount_price', name: 'discount_price'},
                    {data: 'date_booking_start', name: 'date_booking_start'},
                    {data: 'date_booking_end', name: 'date_booking_end'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

        });
    </script>
@endsection
