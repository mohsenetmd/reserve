@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست شهرها</h1>
    <a class="btn btn-primary" href="{{route('city.insert')}}">افزودن</a>
    <hr>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>نام شهر</th>
            <th>تارخ ایجاد</th>
            <th>تاریخ بروزرسانی</th>
			<th>تعداد هتل ها</th>
            <th>مشاهده</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

@endsection
@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('city.show')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'jalali_create', name: 'jalali_create'},
                    {data: 'jalali_update', name: 'jalali_update'},
					{data: 'number_hotel', name: 'number_hotel'},
                    {data: 'link_city', name: 'link_city', orderable: false, searchable: false},
                ]
            });
            console.log(table);
        });
    </script>
@endsection
