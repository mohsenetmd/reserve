@extends('layouts.app')
@section('content')
    <form method="post" action="{{route('group.update')}}">
        @csrf
        @method('put')
        <div class="container">
            <div class="row">
                @for($i=0;$i<count($date);$i++)
                    @if($date[$i][0]->status == 1)
                        <div class="col-lg-3">{{$data[0]['hotel_name']}}</div>
                        <div class="col-lg-3">{{$data[$i]['room_name']}}</div>
                        <div class="col-lg-3">
                            <label>انتخاب نوع رزرو</label>
                            <select class="form-control" name="reserve_type_id[]">
                                @for($j=0;$j<count($reserve_type);$j++)
                                    <option value="{{ $reserve_type[$j]->id }}"
                                            @if($date[$i][0]->reserve_type_id == $reserve_type[$j]->id)
                                            selected
                                        @endif
                                    >{{ $reserve_type[$j]->name }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-lg-12"></div>
                        <div class="col-lg-3">
                            <label style="margin-left: 20px;">تعداد افراد اتاق</label>
                            <span>{{ $data[$i]['person_number'] }}</span>
                        </div>
                        <div class="col-lg-3">
                            <label style="margin-left: 20px;">قیمت مصوب</label>
                            <input class="form-control" name="price[]" type="text" value="{{$date[$i]->pricee}}">
                            <small>قیمت پیش فرض اتاق {{$price[$i]}}</small>
                        </div>
                        <div class="col-lg-3">
                            <label style="margin-left: 20px;">نرخ واریزی هتل</label>
                            <input class="form-control" name="hotel_price[]" type="text" value="{{$date[$i]->hotel_price}}">
                        </div>
                        <div class="col-lg-3">
                            <label style="margin-left: 20px;">نرخ واریزی مسافر</label>
                            <input class="form-control" name="discount_price[]" type="text" value="{{$date[$i]->discount_price}}">
                            <small>قیمت پیش فرض رزرو مسافر {{$date[$i]->discount_price}}</small>
                        </div>
                        <div class="col-lg-12" style="margin-top: 5px;margin-bottom: 5px;"></div>
                        @if($data[$i]['breakfast']==1)
                            <div class="col-lg-3">
                        <span class="badge-pill btn-success">
                        صبحانه دارد
                        </span>
                            </div>
                        @else
                            <div class="col-lg-3">
                        <span class="badge-pill btn-danger">
                        صبحانه ندارد
                        </span>
                            </div>
                        @endif
                        @if($data[$i]['lunch']==1)
                            <div class="col-lg-3">
                        <span class="badge-pill badge-success">
                        ناهار دارد
                        </span>
                            </div>
                        @else
                            <div class="col-lg-3">
                        <span class="badge-pill badge-danger">
                        ناهار ندارد
                        </span>
                            </div>
                        @endif
                        @if($data[$i]['dinner']==1)
                            <div class="col-lg-3">
                        <span class="badge-pill badge-success">
                        شام دارد
                        </span>
                            </div>
                        @else
                            <div class="col-lg-3">
                        <span class="badge-pill badge-danger">
                        شام ندارد
                        </span>
                            </div>
                        @endif
                        <div class="col-lg-12" style="margin-bottom: 5px;"></div>
                        <label style="margin-left: 20px;margin-right: 20px;">حذف</label>
                        <input type="checkbox" name="delete[]" value="{{$date[$i][0]->id}}">
                        <input type="hidden" name="id[]" value="{{$date[$i][0]->id}}">
                        <div class="col-lg-12">
                            <hr>
                        </div>
                    @endif
                @endfor
            </div>
        </div>
        <div id="app">
            <date-component></date-component>
        </div>
        <input type="hidden" name="group_id" value="{{request()->id}}">
        <button class="btn btn-success" type="submit">ذخیره</button>
    </form>
@endsection
@section('script')
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
@endsection
