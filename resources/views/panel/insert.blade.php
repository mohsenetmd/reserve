@extends('layouts.app')
@section('content')
    <form action="{{route('foodtype.save')}}" method="POST" style="">
        @csrf
        <h1 class="titr">وارد کردن نوع غذا</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نوع غذا</label>
                <input type="text" class="form-control" name="name" required value="{{ old('name')}}">
            </div>
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
@endsection
