@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <form action="{{route('panel.updated')}}" method="POST" style="">
        @csrf
        @method('put')
        <h1 class="titr">وارد کردن مشخصات کاربری</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نام کاربری</label>
                <select name="admin_level" class="form-control">
                    <option value="0" @if($user->admin_level==0)   selected @endif>کاربر عادی</option>
                    <option value="1" @if($user->admin_level==1)   selected @endif>کاربر هتل</option>
                    <option value="2" @if($user->admin_level==2)   selected @endif>ادمین فروش</option>
                    <option value="3" @if($user->admin_level==3)   selected @endif >ادمین سرپرست</option>
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label>نام کاربری</label>
                <input type="text" class="form-control" name="name" required value="{{$user->name}}">
            </div>
            <div class="form-group col-lg-4">
                <label>نام و نام خانوادگی</label>
                <input type="text" class="form-control" name="family" required value="{{$user->family}}">
            </div>
            <div class="form-group col-lg-4">
                <label>شماره موبایل</label>
                <input type="text" class="form-control" name="mobile" required value="{{$user->mobile}}">
            </div>
            <input type="hidden" class="form-control" name="id" value="{{$user->id}}">
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
@endsection
