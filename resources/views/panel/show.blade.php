@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست کاربران</h1>
    <hr>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>نام</th>
            <th>نام خانوادگی</th>
            <th>شماره موبایل</th>
            <th>ایمیل</th>
            <th>سطح دسترسی</th>
            <th>تغییر سطح دسترسی</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

@endsection
@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('panel.show')}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'family', name: 'family'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'email', name: 'email'},
                    {data: 'admin_level_show', name: 'admin_level_show', orderable: false, searchable: false},
                    {data: 'link_panel', name: 'link_panel', orderable: false, searchable: false}
                ]
            });
            console.log(table);
        });
    </script>
@endsection
