@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست اتاق ها</h1>
    <a class="btn btn-primary" href="{{route('room.insert',['hotel_id'=>request()->hotel_id])}}">افزودن</a>
    <a class="btn btn-success" href="{{route('hotel.show')}}" style="float: left;margin-left: 20px;">بازگشت</a>
    <hr>
    <form method="post" action="{{route('room.copy')}}">
        @csrf
        <table class="table table-bordered data-table">
            <thead>
            <tr>
                <th>آپدیت شده توسط</th>
                <th>نوع اتاق</th>
                <th>قیمت با تخفیف</th>
                <th>تاریخ بروزرسانی</th>
                <th>مشاهده</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <select name="cd">
            <option value="1">کپی</option>
            <option value="0">حذف</option>
        </select>
        <button type="submit">ارسال</button>
    </form>
@endsection
@section('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "http://localhost:8000/admin/room?hotel_id={{request()->hotel_id}}",
                    method: 'POST'
                },
                columns: [
                    {data: 'admin_updated', name: 'admin_updated'},
                    {data: 'room_type', name: 'room_type'},
                    {data: 'discount_price', name: 'discount_price'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'otagh', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection
