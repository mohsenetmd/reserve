@extends('layouts.app')
@section('content')
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form enctype="multipart/form-data" method="post" action="{{route('room.updated')}}">
                    @csrf
                    @method('put')
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات اتاق</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    @foreach($reservetype as $item)
                                        <option value="{{$item->id}}
                                        @if($room->resrerve_type_id == $item->id)
                                            selected
                                            @endif
                                            ">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع اتاق</label>
                                <select class="roomtype_search form-control" name="room_type_id">
                                    @foreach($roomtype as $item)
                                        <option value="{{$item->id}}"
                                                @if($room->room_type_id == $item->id)
                                                selected
                                            @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع تخت</label>
                                <select class="bedtype_search form-control" name="bed_type_id">
                                    @foreach($bedtype as $item)
                                        <option value="{{$item->id}}"
                                                @if($room->bed_type_id == $item->id)
                                                selected
                                            @endif
                                        >{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="app2" class="input-group">
								<label for="imageInput">عکس را وارد کنید</label>
                                <input class="form-control-file" data-preview="#preview" name="pic" type="file" id="imageInput">
                                <div v-if="showimage" style="display: block;width: 100%;">
                                <img style="margin: auto;display: block;height: 300px;" src="{{$room->pic}}" >
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد افراد</label>
                                <input type="text" class="form-control" name="person_number" required
                                       value="{{$room->person_number}}">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت</label>
                                <input type="text" class="form-control" name="price" required value="{{$room->price}}">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت با تخفیف</label>
                                <input type="text" class="form-control" name="discount_price"
                                       value="{{$room->discount_price}}"
                                       required>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تخت اضافه</label>
                                <select name="more_bed" class="form-control">
                                    <option value="0"
                                            @if($room->more_bed == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($room->more_bed == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_number" value="{{$room->bed_number}}">
                                <small class="form-text text-muted">تعداد تختی که قابلیت اضافه کردن را دارد انتخاب
                                    کنید</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price" value="{{$room->bed_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>کودک</label>
                                <select name="child" class="form-control">
                                    <option value="0"
                                            @if($room->child == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($room->child == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد کودک</label>
                                <input type="text" class="form-control" name="child_number" value="{{$room->child_number}}">
                                <small class="form-text text-muted">تعداد کودکی که قابلیت اضافه شدن را دارد مشخص
                                    کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price" value="{{$room->child_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>صبحانه</label>
                                <input type="text" class="form-control" name="breakfast_price" value="{{$room->breakfast_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای صبحانه را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>ناهار</label>
                                <input type="text" class="form-control" name="lunch_price" value="{{$room->lunch_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای ناهار را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>شام</label>
                                <input type="text" class="form-control" name="dinner_price" value="{{$room->dinner_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای شام را وارد کنید.</small>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>صبحانه رایگان</label>
                                <select name="breakfast" class="form-control">
                                    <option value="0"
                                            @if($room->breakfast == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($room->breakfast == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ناهار رایگان</label>
                                <select name="lunch" class="form-control">
                                    <option value="0"
                                            @if($room->lunch == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($room->lunch == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>شام رایگان</label>
                                <select name="dinner" class="form-control">
                                    <option value="0"
                                            @if($room->dinner == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($room->dinner == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع غذا</label>
                                <select class="foodtype_search form-control" name="food_type_id">
                                    @foreach($foodtype as $item)
                                        <option value="{{$item->id}}"
                                                @if($room->food_type_id == $item->id)
                                                selected
                                            @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>قیمت نوع غذا</label>
                                <input type="text" class="form-control" name="price_food_type" value="{{$room->price_food_type}}">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ترتیب اتاق</label>
                                <input type="text" class="form-control" name="sort" value="{{$room->sort}}">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>شماره اتاق</label>
                                <input type="text" class="form-control" name="room_number" value="{{$room->room_number}}">
                            </div>
                            <div class="form-check col-lg-12">
                                <label>در صورت نبود اطلاعات در یک تاریخ این اطلاعات جایگزین شود؟</label>
                                <select class="form-control" name="sample_data">
                                    <option value="0"
                                            @if($room->sample_data == 0)
                                            selected
                                        @endif>بله
                                    </option>
                                    <option value="1"
                                            @if($room->sample_data == 1)
                                            selected
                                        @endif>خیر
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-12" style="margin-top: 20px">
                                <label class="form-check-label" for="defaultCheck1">
                                    راهنما مطالعه شده است؟
                                </label>
                            </div>
                            <input type="hidden" class="form-control" name="hotel_id" value="{{request()->hotel_id}}">
                            <input type="hidden" class="form-control" name="id" value="{{request()->room_id}}">
                            <input type="hidden" class="form-control" name="admin_created" value="{{ $room->admin_created }}">
                            <input type="hidden" class="form-control" name="admin_updated" value="{{ Auth::user()->name }}">
                            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        let app2 = new Vue({
            el: '#app2',
            data: {
                showimage: true,
            },
        });
        $(document).ready(function () {
            $('.reservetype_search').select2();
            $('.foodtype_search').select2();
            $('.bedtype_search').select2();
            $('.roomtype_search').select2();
        });
        $('#lfm').filemanager('image');
    </script>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
