@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <form action="{{route('hotel.save')}}" method="POST" style="">
        @csrf
        @method('put')
        <h1 class="titr">وارد کردن اطلاعات هتل</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نام هتل</label>
                <input type="text" class="form-control" name="name" required value="{{ $hotel->name }}">
            </div>
            <div class="form-group col-lg-4">
                <label>شروع قیمت</label>
                <input type="text" class="form-control" name="start_price" required value="{{ $hotel->start_price }}">
            </div>
            <div class="form-group col-lg-4">
                <label>شهر</label>
                <select class="city_search form-control" name="city_id" value="26">
                    @foreach($city as $item)
                        <option value="{{$item->id}}" @if($item->id == $hotel->city_id) selected @endif >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label>ستاره</label>
                <select class="star_search form-control" name="hotel_star_id" value="5">
                    @foreach($hotelstar as $item)
                        <option value="{{$item->id}}" @if($item->id == $hotel->hotel_star_id) selected @endif >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label>نوع هتل</label>
                <select class="type_search form-control" name="hotel_type_id">
                    @foreach($hoteltype as $item)
                        <option value="{{$item->id}}" @if($item->id == $hotel->hotel_type_id) selected @endif >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="admin_created" value="{{ $hotel->admin_created }}">
            <input type="hidden" name="admin_updated" value="{{ Auth::user()->name }}">
            <input type="hidden" name="id" value="{{$hotel->id}}">
            <input type="hidden" name="wp_post_id" value="{{$hotel->wp_post_id}}">
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
@endsection
@section('script')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.city_search').select2();
            $('.star_search').select2();
            $('.type_search').select2();
        });
    </script>

@endsection
