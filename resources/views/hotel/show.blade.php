@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست هتل ها</h1>
    <a class="btn btn-primary" href="{{route('hotel.insert')}}">افزودن</a>
    <hr>
    <form method="post" action="{{route('hotel.show')}}">
        @csrf
        <div class="row">
            <div class="col-lg-3">
                <select class="form-control" name="city_id">
                    <option value="0">گزینه ای انتخاب نشده است.</option>
                    @foreach($city as $item)
                        <option class="form-control" value="{{$item->id}}"
                                @if(session('city_id')==$item->id)
                                selected
                            @endif
                        >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-3">
                <select class="form-control" name="hotel_star_id">
                    <option value="0">گزینه ای انتخاب نشده است.</option>
                    @foreach($hotelStar as $item)
                        <option value="{{$item->id}}"
                                @if(session('hotel_star_id')==$item->id)
                                selected
                            @endif
                        >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-3">
                <select class="form-control" name="hotel_type_id">
                    <option value="0">گزینه ای انتخاب نشده است.</option>
                    @foreach($hotelType as $item)
                        <option value="{{$item->id}}"
                                @if(session('hotel_type_id')==$item->id)
                                selected
                            @endif
                        >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-3">
                <button class="btn btn-success" type="submit">جستجو</button>
                @if(session('city_id') || session('hotel_star_id') || session('hotel_type_id'))
                    <a class="btn btn-danger" href="{{route('hotel.show',['destroy'=>'1'])}}">حذف جستجو</a>
                @endif
            </div>
        </div>
    </form>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>تاریخ</th>
            <th>نام هتل</th>
            <th>بروزرسانی</th>
            <th>تاریخ بروزرسانی</th>
            <th>مشاهده</th>
        </tr>
        </thead>
        <tbody>
        <tfoot>
        <tr>
            <th>مشاهده جزئیات</th>
            <th>نام هتل</th>
            <th>بروزرسانی</th>
            <th>تاریخ بروزرسانی</th>
            <th>مشاهده</th>
        </tr>
        </tfoot>
    </table>
@endsection
@section('script')
    <script type="text/javascript">
        function format(d) {
            // `d` is the original data object for the row
            var show;
            d.start_time.forEach(function (item) {
                if (show == undefined) {
                    show = '<div class="container"><div class="row"><div class="col-3"><div>' + item + '</div>';
                } else {
                    show = show + '<div>' + item + '</div>';
                }
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.end_time.forEach(function (item) {
                show = show + '<div>' + item + '</div>';
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.id.forEach(function (item) {
                show = show + '<div><a href="https://hihotel.org/group/edit/' + item + '" >edit</a></div>';
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.id.forEach(function (item) {
                show = show + '<div><a href="https://hihotel.org/group/copy/' + item + '" >copy</a></div>';
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.id.forEach(function (item) {
                show = show + '<div><a href="https://hihotel.org/group/del/' + item + '" >del</a></div>';
                return show;
            });
            show = show + '</div></div></div>';
            return show;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "{{route('hotel.show')}}",
                    method: 'POST'
                },
                columns: [
                    {
                        className: 'details-control',
                        orderable: false,
                        data: 'more',
                        defaultContent: '+'
                    },
                    {data: 'name', name: 'name'},
                    {data: 'admin_updated', name: 'admin_updated'},
                    {data: 'jalali_update', name: 'jalali_update'},
                    {data: 'link_room', name: 'link_room'},
                ]
            });
            $('.data-table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        });
    </script>
@endsection
