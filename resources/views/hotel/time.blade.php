@extends('layouts.app')
@section('content')
    <?php $i = 0; ?>
    <form method="post" action="{{route('hotel.savetime')}}">
        @csrf
        @foreach($room as $item)
            <h5>{{$item->name}}
            @if($rooms[$i]->breakfast == 1)
                صبحانه
            @endif
            @if($rooms[$i]->lunch == 1)
                ناهار
            @endif
            @if($rooms[$i]->dinner == 1)
                شام
            @endif
            </h5>
            @foreach($date[$i] as $item2)
                <div class="row">
                    <div class="col-lg-3">
                        <label>انتخاب نوع رزرو</label>
                        <select class="form-control" name="reserve_type_id[]">
                            @for($j=0;$j<count($reserve_type);$j++)
                                <option value="{{ $reserve_type[$j]->id }}"
                                        @if($item2->reserve_type_id == $reserve_type[$j]->id)
                                        selected
                                    @endif
                                >{{ $reserve_type[$j]->name }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <input type="hidden" name="id[]" value="{{$item2->id}}">
                        <label>تاریخ شروع</label>
                        @if($item2->gr != 1)
                        <input class="form-control" type="text" name="start[]" value="{{$item2->date_booking_start}}">
                         @else
                            <input class="form-control" type="hidden" name="start[]" value="{{$item2->date_booking_start}}">
                        <p>{{$item2->date_booking_start}}</p>
                         @endif
                    </div>
                    <div class="col-lg-3">
                        <label>تاریخ پایان</label>
                        @if($item2->gr != 1)
                            <input class="form-control" type="text" name="end[]" value="{{$item2->date_booking_end}}">
                        @else
                            <input class="form-control" type="hidden" name="end[]" value="{{$item2->date_booking_end}}">
                            <p>{{$item2->date_booking_end}}</p>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        <label>قیمت مصوب</label>
                        <input class="form-control" type="text" name="price[]" value="{{$item2->price}}">
                    </div>
                    <div class="col-lg-3">
                        <label>قیمت مسافر</label>
                        <input class="form-control" type="text" name="discount_price[]"
                               value="{{$item2->discount_price}}">
                    </div>
                    <div class="col-lg-3">
                        <label>قیمت هتل</label>
                        <input class="form-control" type="text" name="hotel_price[]" value="{{$item2->hotel_price}}">
                    </div>
                    <div class="col-lg-3">
                        <label>حذف</label>
                        <input class="form-control" type="checkbox" name="del[]" value="{{$item2->id}}">
                    </div>
                </div>
                <hr>
            @endforeach
            <hr style="border-bottom: 2px solid black">
            <?php $i += 1; ?>
        @endforeach
        <button type="submit">ذخیره</button>
    </form>
@endsection
