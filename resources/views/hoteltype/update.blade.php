@extends('layouts.app')
@section('content')
    @include('sweet::alert')
    <form action="{{route('hoteltype.updated')}}" method="POST" style="">
        @csrf
        @method('put')
        <h1 class="titr">وارد کردن اطلاعات نوع هتل</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نوع هتل</label>
                <input type="text" class="form-control" name="name" required value="{{$hoteltype->name}}">
                <input type="hidden" name="id" value="{{$hoteltype->id}}">
            </div>
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
@endsection
