<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>سامانه رزرواسیون های هتل</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Fonts -->

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>


<body>
<style>
    #content .btn-primary{
        float: left;
    }
</style>
<!-- Start vertical navbar -->
<header id="header">
    <div class="vertical-nav bg-white" id="sidebar">
        <p class="text-gray font-weight-bold text-uppercase px-3 small pb-3 mb-2 mt-3">پنل کاربری</p>
        @can('Admin')
        <ul class="nav flex-column bg-white mb-0">
            <li class="nav-item">
                <a href="{{route('hotel.show')}}">نمایش هتل ها</a>
            </li>
            <li class="nav-item">
                <a href="{{route('city.show')}}">نمایش شهرها</a>
            </li>
            <li class="nav-item">
                <a href="{{route('hoteltype.show')}}">نمایش نوع هتل ها</a>
            </li>
            <li class="nav-item">
                <a href="{{route('hotelstar.show')}}">نمایش ستاره ها</a>
            </li>
            <li class="nav-item">
                <a href="{{route('bedtype.show')}}">نمایش نوع تخت</a>
            </li>
            <li class="nav-item"><a href="{{route('roomtype.show')}}">
                    نمایش نوع اتاق</a>
            </li>
            <li class="nav-item">
                <a href="{{route('foodtype.show')}}">نمایش نوع غذا</a>
            </li>
            <li class="nav-item">
                <a href="{{route('reservetype.show')}}">نمایش نوع رزرو</a>
            </li>
            <li class="nav-item">
                <a href="{{route('savereserve.show')}}">مشاهده استعلام</a>
            </li>
        </ul>
        @endcan
    </div>
	<div class="mobile">
	@can('Admin')
            <span class="btn">
                <a href="{{route('hotel.show')}}">نمایش هتل ها</a>
            </span>
            <span class="btn">
                <a href="{{route('city.show')}}">نمایش شهرها</a>
            </span>
            <span class="btn">
                <a href="{{route('hoteltype.show')}}">نمایش نوع هتل ها</a>
            </span>
            <span class="btn">
                <a href="{{route('hotelstar.show')}}">نمایش ستاره ها</a>
            </span>
            <span class="btn">
                <a href="{{route('bedtype.show')}}">نمایش نوع تخت</a>
            </span>
            <span class="btn"><a href="{{route('roomtype.show')}}">
                    نمایش نوع اتاق</a>
            </span>
            <span class="btn">
                <a href="{{route('foodtype.show')}}">نمایش نوع غذا</a>
            </span>
            <span class="btn">
                <a href="{{route('reservetype.show')}}">نمایش نوع رزرو</a>
            </span>
            <span class="btn">
                <a href="{{route('savereserve.show')}}">مشاهده استعلام</a>
            </span>
        @endcan
	</div>
	<style>
		.mobile{
			display:none;
		}
	@media only screen and (max-width: 768px) {
		.mobile{
			display:block;
		}
		.flex-column{
			display:none;
		}
	}
	</style>
</header>
<!-- Start Page content holder -->
<div class="page-content p-5" id="content">
    <button id="sidebarCollapse" type="button" class="btn btn-danger" style="margin-top: -60px;margin-bottom: 20px;">
            <svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 -53 384 384" width="15px" class=""><g><path d="m368 154.667969h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/><path d="m368 32h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/><path d="m368 277.332031h-352c-8.832031 0-16-7.167969-16-16s7.167969-16 16-16h352c8.832031 0 16 7.167969 16 16s-7.167969 16-16 16zm0 0" data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/></g> </svg>
        </button>
    @yield('content')
</div>
</body>
<script>
    $(function() {
        // Sidebar toggle behavior
        $('#sidebarCollapse').on('click', function() {
            $('#sidebar, #content').toggleClass('active');
        });
    });
</script>
@yield('script')
</html>
