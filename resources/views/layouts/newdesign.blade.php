
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Hotel Reservation</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>سامانه رزرواسیون های هتل</title>
    <!-- GOOGLE FONT -->
<!--     <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet"> -->
    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="https://hihotel.org/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="https://hihotel.org/css/owl.carousel.min.css">
    <link rel="shortcut icon" type="text/css" href="https://hihotel.ir/fav.png" />
    <link rel="stylesheet" type="text/css" href="https://hihotel.org/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://hihotel.org/bootstrap.css">
    <!-- MAIN STYLE -->
    <link rel="stylesheet" href="https://hihotel.org/css/styles.css">

</head>

<body>

    <!-- HEADER -->
    <header class="header-sky">

        <div class="container">
            <!--HEADER-TOP-->
            <div class="header-top" style="text-align: right;">
                <div class="header-top-left" >
                    <span><i class="ion-ios-location-outline"></i> آدرس: استان خراسان رضوی، مشهد، خیابان بهار، بهار 44، پلاک 195</span>
                    <span><i class="fa fa-phone" aria-hidden="true"></i> 051-38584050</span>
                </div>
                <div class="header-top-right">
                    <ul>
                        <li class="dropdown"><a href="login.html" title="LOGIN" class="dropdown-toggle">ورود</a></li>
                        <li class="dropdown"><a href="register.html" title="REGISTER" class="dropdown-toggle">ثبت نام</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">فارسی <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="active"><a href="#">فارسی</a></li>
                                <li><a href="#">ENG</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END/HEADER-TOP -->
        </div>
        <!-- MENU-HEADER -->
        <div class="menu-header">
            <nav class="navbar navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header ">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar "></span>
                            <span class="icon-bar "></span>
                            <span class="icon-bar "></span>
                        </button>
                        <a class="navbar-brand" href="https://hihotel.ir" title="Skyline"><img src="https://hihotel.ir/wp-content/themes/Hotell/image/logo5.png" alt="logo" width="100" height="auto"></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="https://hihotel.ir/blog/" title="About">وبلاگ</a></li>
                            <li><a href="https://hihotel.ir/aboutus/" title="About">درباره ما</a></li>
                            <li><a href="https://hihotel.ir/%d8%aa%d9%85%d8%a7%d8%b3-%d8%a8%d8%a7-%d9%85%d8%a7/" title="Contact">تماس با ما</a></li>
<!--                                     <li class="submenu-hover1">
                                        <a href="restaurant_1.html" class="dropdown-toggle" data-toggle="dropdown">Restaurant <b class="caret"></b></a>
                                        <ul class="dropdown-menu dropdown-margin">
                                            <li><a href="restaurant_1.html">Restaurant 1</a></li>
                                            <li><a href="restaurant_2.html">Restaurant 2</a></li>
                                            <li><a href="restaurant_3.html">Restaurant 3</a></li>
                                            <li><a href="restaurant_4.html">Restaurant 4</a></li>
                                        </ul>
                                    </li> -->

<!--                             <li class="dropdown ">
                                <a href="room_1.html" title="Room & Rate" class="dropdown-toggle" data-toggle="dropdown">Room & Rate<b class="caret"></b></a>
                                <ul class="dropdown-menu icon-fa-caret-up submenu-hover">
                                    <li><a href="room_1.html" title="">Room 1</a></li>
                                    <li><a href="room_2.html" title="">Room 2</a></li>
                                    <li><a href="room_3.html" title="">Room 3</a></li>
                                    <li><a href="room_4.html" title="">Room 4</a></li>
                                    <li><a href="room_5.html" title="">Room 5</a></li>
                                    <li><a href="room_6.html" title="">Room 6</a></li>
                                    <li><a href="room_detail.html" title="">Room Detail</a></li>
                                </ul>
                            </li> -->
                            <li class="dropdown ">
                                <a href="https://hihotel.ir" title="Home" class="dropdown-toggle" data-toggle="dropdown">خانه<b class="caret"></b></a>
                                <ul class="dropdown-menu icon-fa-caret-up submenu-hover">
                                    <li><a href="https://hihotel.ir/mashhadhotels/" title="">هتل های مشهد</a></li>
                                    <li><a href="https://hihotel.ir/kishhotels/" title="">هتل های کیش</a></li>
                                    <li><a href="https://hihotel.ir/tehran-hotels/" title="">هتل های تهران</a></li>
                                    <li><a href="https://hihotel.ir/shirazhotels/" title="">هتل های شیراز</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <!-- END / MENU-HEADER -->
    </header>
    <!-- END-HEADER --><html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	@yield('content')

    <footer class="footer-sky footer-top">
        <!-- /footer-top -->
        <div class="footer-mid">
            <div class="container">
                <div class="row padding-footer-mid">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="footer-logo text-center list-content">
                            <a href="https://hihotel.ir" title="Skyline"><img src="https://hihotel.ir/wp-content/themes/Hotell/image/logo5.png" alt="Image"></a>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 col-lg-offset-1 col-md-offset-1  ">
                        <div class="list-content">
                            <ul>
                                <li><a href="https://trustseal.enamad.ir/?id=132893&Code=Gyj2YFtus8Nf1aaWmL0R" title="">نماد اعتماد الکترونیکی</a></li>
                                <li><a href="https://hihotel.ir/enamad/" title="">قوانین و مقررات</a></li>
                                <li><a href="#" title="">به ما اعتماد کنید</a></li>
                                <li><a href="#" title="">پشتیبانی 24 ساعته</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 col-lg-offset-1 col-md-offset-1 ">
                        <div class="list-content ">
                            <ul>
                                <li><a href="#" title="">موقعیت ما در نقشه</a></li>
                                <li><a href="#" title="">نماد اعتماد الکترونیکی</a></li>
                                <li><a href="about.html" title="">درباره ما</a></li>
                                <li><a href="contact.html" title="">تماس با ما</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 col-lg-offset-1 col-md-offset-1">
                        <div class="list-content ">
                            <ul>
                                <li><a href="#" title="">پرسش های متدوال</a></li>
                                <li><a href="#" title="">اخبار گردشگری</a></li>
                                <li><a href="gallery_1.html" title="">تصاویر و ویدئوها</a></li>
                                <li><a href="restaurant_1.html" title="">تورهای گردشگری</a></li>
                                <li><a href="#" title="">بلیط هواپیما</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-padding">
                        Copyright © 2017 by <a href="https://hihotel.ir" title="">HiHotel Developer.</a><br> HiHotel Revolution in Tourism Industry </div>
    <form name="contentForm" enctype="multipart/form-data" method="post" action="">
      <div class="ct-footer-pre text-center-lg">
		  <div ><a href="https://hihotel.ir/enamad/" target="_blank">
			<img class="namad-icon" src="https://hihotel.ir/wp-content/themes/Hotell/image/enamad6.png" width="60" height="60">
			<img class="namad-icon" src="https://hihotel.ir/wp-content/themes/Hotell/image/samandehi3.png" width="60" height="60">
			<img class="namad-icon" src="https://hihotel.ir/wp-content/themes/Hotell/image/pasargad.png" width="60" height="60">		
			  </a>
		</div>
      </div>

    </form>
                </div>
            </div>
        </div>
    </footer>
    <!-- END / FOOTER-->
    <!-- LOAD JQUERY -->
    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- Custom jQuery -->
    <script type="text/javascript" src="js/sky.js"></script>
   
</body>
<style>
	.footer-top{
		margin-top:15%;
	}
	.textreserve{
		flex: 3 0 25%;
		max-width: 50%;
		left: 60%;
	}
	.social-box{
		    position: absolute;
    right: 30px;
    top: 50%;
    transform: translateY(-50%);
	}
    .slant-box {
        content: '';
        display: inline-block;
        height: 523px;
        transform: rotate(41deg);
        width: 3px;
        background: white;
    }
    .insert_number {
        background-color: rgb(203, 42, 48);
        width: 100vw;
        height: 100vh;
    }

    .insert_shomare_mobile {
        text-align: center;
        width: 250px;
        height: 170px;
        background-color: #ffffff54;
        padding: 20px;
        display: block;
/*        margin: auto;
*/        margin-top: calc(50vh - 70px);
        border-radius: 20px;
        box-shadow: 14px 19px 20px 12px #1d212452;
    }

    .text_mobile {
        height: 200px;
        margin: auto;
        margin-top: calc(50vh - 150px);
        width:900px;
        color: white;
        text-align: justify;
        font-size: 18px;
        font-weight: bold;
    }

    html,
    body {
        padding: 0;
        margin: 0;
        font-family: IRANSans, sans-serif !important;
        direction: rtl;
        text-align: right;
    }

    #triangle-down {
        width: 0;
        height: 0;
        border-left: 120px solid transparent;
        border-right: 30px solid transparent;
        border-top: 30px solid white;
        display: block;
        margin: auto;
    }
@media (min-width: 1200px) {
	.backvid {
		position: absolute; 
		z-index: 0; 
		top: 0px; 
		left: 0px; 
		bottom: 0px; 
		right: 0px; 
		overflow: hidden; 
		background-size: cover; 
		background-color: transparent; 
		background-repeat: no-repeat; 
		background-position: 50% 50%; 
		background-image: none;
	}
	.vide{
		margin: auto;
		position: absolute;
		z-index: -1;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		visibility: visible;
		opacity: 1;
		/* width: 1351px; */
		/* height: auto; */
	}
}
@media (max-width:768px) {
	.footer-sky .footer-mid .padding-footer-mid .list-content{
		display: none;
	}
	.footer-top{
		margin-top:70px;
	}
    .insert_shomare_mobile {
	    text-align: center;
	    width: 250px;
	    /* height: 170px; */
	    background-color: #ffffff54;
	    padding: 15px;
	    display: block;
	    /* margin: auto; */
	    margin-top: calc(50vh - 190px);
	    border-radius: 20px;
	    box-shadow: -20px 19px 20px 12px #1d212452;
	    margin-right: 65px;
		}
   
    .text_mobile {
		height: 0px;
		margin: auto;
		/*        margin-top: calc(50vh - 150px);
		*/        width:200px;
		color: white;
		text-align: justify;
		font-size: 18px;
		font-weight: bold;
    }
	.vide{
		margin: auto;
	    position: absolute;
	    z-index: -1;
	    top: 15%;
	    left: 50%;
	    transform: translate(-50%, -50%);
	    visibility: visible;
	    opacity: 1;
	    width: 290%;
	    /* height: 500px !important; */
	    position: fixed;
	}
	.header-sky{
		position: fixed;
	    z-index: 999;
	    right: 0;
	    left: 0;
	    top: 0;
	    width: 360px;
	}
	.social-box {
    position: absolute;
    right: 30px;
    top: 33%;
    transform: translateY(-50%);
}
</style>
