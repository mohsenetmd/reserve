<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Fonts -->

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div>
    <div class="container-fluid">
        <div class="row">
            <ul>
                <li><a href="{{route('hotel.show')}}">نمایش هتل ها</a></li>
                <li><a href="{{route('city.show')}}">نمایش شهرها</a></li>
                <li><a href="{{route('hoteltype.show')}}">نمایش نوع هتل ها</a></li>
                <li><a href="{{route('hotelstar.show')}}">نمایش ستاره ها</a></li>
                <li><a href="{{route('bedtype.show')}}">نمایش نوع تخت</a></li>
                <li><a href="{{route('roomtype.show')}}">نمایش نوع اتاق</a></li>
                <li><a href="{{route('foodtype.show')}}">نمایش نوع غذا</a></li>
                <li><a href="{{route('reservetype.show')}}">نمایش نوع رزرو</a></li>

            </ul>
            <div class="container" style="width: 80vw;">
                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>
@yield('script')
</html>
