<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>سامانه رزرواسیون های هتل</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script src="{{ mix('js/app.js') }}"></script>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ $user }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="text-align: right">
                        <a class="dropdown-item" href="{{route('user.show')}}">پنل کاربری</a>
                        <a class="dropdown-item" href="{{ route('logout') }}">خروج</a>
                    </div>
                </div>
            @else
                <a href="{{ route('login') }}">ورود</a>
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            @if (Route::has('login'))
            <a class="btn btn-dark" href="{{route('hotel.show')}}">ورود به پنل کاربری</a>
            @endif
        </div>
    </div>
</div>
</body>
</html>
