<?php
$start = "1399/02/30";
$end = "1399/03/14";
$date[0] = [1, 25, 1];
$date[1] = [26, 50, 2];
$date[2] = [51, 75, 3];
$date[3] = [76, 100, 4];
function nthday($inputday)
{
    $inputday = str_replace("/", '', $inputday);
    $time = str_split($inputday, 2);
    $year = $time[0] . $time[1];
    $month = $time[2];
    $day = $time[3];
    if ($year > 1399) {
        echo 'ok';
    }

    if ($month > 6) {
        $days = 6 * 31 + ($month - 7) * 30;
    } else {
        $days = 31 * ($month - 1);
    }

    $days = $days + $day;
    return $days;
}
$parcham=0;
$startday=26;
$enddate=74;
$price=0;
for ($i = 0; $i < count($date); $i++) {
    if ($date[$i][0] <= $startday && $enddate <= $date[$i][1]) {
        for ($j = $startday; $j < $enddate; $j++) {
            $price = $price + $date[$i][2];
            echo $date[$i][2];
            echo '<br>';
            echo $price;
            echo '<br>';
            $parcham+=1;
        }
    }
    if ($date[$i][0] <= $startday && $startday <= $date[$i][1] && $enddate>=$date[$i][1]){
        for ($j = $startday; $j <= $date[$i][1]; $j++) {
            $price = $price + $date[$i][2];
            $parcham+=1;
            echo $date[$i][2];
            echo '<br>';
            echo $price;
            echo '<br>';
        }
        $startday=$date[$i][1]+1;
    }
}
dd($price,$parcham);



$varname = 'room';
$classname = 'Room';
$faname = "نوع اتاق";
?>


Route::get('admin/{{$varname}}','{{$classname}}Controller@show')->name('{{$varname}}.show');
Route::post('admin/{{$varname}}','{{$classname}}Controller@show')->name('{{$varname}}.show');
Route::get('admin/{{$varname}}/insert',function (){return view('{{$varname}}.insert');})->name('{{$varname}}.insert');
Route::get('admin/{{$varname}}/update','{{$classname}}Controller@update')->name('{{$varname}}.update');
Route::post('admin/{{$varname}}/save','{{$classname}}Controller@save')->name('{{$varname}}.save');
Route::put('admin/{{$varname}}/save','{{$classname}}Controller@updated')->name('{{$varname}}.updated');

use App\{{$classname}};
use Yajra\DataTables\Facades\DataTables;

public function update(Request $request)
{
return view('{{$varname}}.update', ['{{$varname}}' => {{$classname}}::find($request->id)]);
}


public function save()
{
{{$classname}}::create([
'name' => \request()->name,
]);
alert()->success('{{$faname}} با موفقیت ذخیره شد.', 'ذخیره {{$faname}}');
return redirect(route('{{$varname}}.show'));
}

public function updated()
{
{{$classname}}::where('id', \request()->id)->update([
'name' => \request()->name,
]);
return redirect(route('{{$varname}}.show'));
}

public function show(Request $request)
{
if ($request->ajax()) {
$data = {{$classname}}::latest()->get();
return Datatables::of($data)
->addIndexColumn()
->addColumn('link_{{$varname}}', function ($row) {
$btn = '<a href="/admin/{{$varname}}/update?id=' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
return $btn;
})
->rawColumns(['link_{{$varname}}'])
->make(true);
}
return view('{{$varname}}.show');
}


