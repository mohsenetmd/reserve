<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Hotel Reservation</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="https://hihotel.org/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://hihotel.org/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="https://hihotel.org/css/owl.carousel.min.css">
    <link rel="shortcut icon" type="text/css" href="https://hihotel.ir/fav.png"/>
    <link rel="stylesheet" type="text/css" href="https://hihotel.org/css/bootstrap-select.min.css">
    <!-- MAIN STYLE -->
    <link rel="stylesheet" href="https://hihotel.org/css/styles.css">
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Fonts -->

    <!-- Styles -->
    <!--     <link href="https://hihotel.org/css/app.css" rel="stylesheet">
     -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152335471-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-152335471-2');
    </script>

    <!-- <style>
        #content .btn-primary{
            float: left;
        }
    </style> -->
</head>

<body>

<!-- HEADER -->
<header class="header-sky">

    <div class="container">
        <!--HEADER-TOP-->
        <div class="header-top" style="text-align: right;">
            <div class="header-top-left">
                <span><i class="ion-ios-location-outline"></i> آدرس: استان خراسان رضوی، مشهد، خیابان بهار، بهار 44، پلاک 195</span>
                <span><i class="fa fa-phone" aria-hidden="true"></i> 051-38584050</span>
            </div>
            <div class="header-top-right">
                <ul>
                    <li class="dropdown"><a href="login.html" title="LOGIN" class="dropdown-toggle">ورود</a></li>
                    <li class="dropdown"><a href="register.html" title="REGISTER" class="dropdown-toggle">ثبت نام</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">فارسی <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="active"><a href="#">فارسی</a></li>
                            <li><a href="#">ENG</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END/HEADER-TOP -->
    </div>
    <!-- MENU-HEADER -->
    <div class="menu-header">
        <nav class="navbar navbar-fixed-top">
            <div class="container">
                <div class="navbar-header ">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar "></span>
                        <span class="icon-bar "></span>
                        <span class="icon-bar "></span>
                    </button>
                    <a class="navbar-brand" href="https://hihotel.ir" title="Skyline"><img
                                src="https://hihotel.ir/wp-content/themes/Hotell/image/logo5.png" alt="logo" width="100"
                                height="auto"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="https://hihotel.ir/blog/" title="About">وبلاگ</a></li>
                        <li><a href="https://hihotel.ir/aboutus/" title="About">درباره ما</a></li>
                        <li><a href="https://hihotel.ir/%d8%aa%d9%85%d8%a7%d8%b3-%d8%a8%d8%a7-%d9%85%d8%a7/"
                               title="Contact">تماس با ما</a></li>
                        <li class="dropdown ">
                            <a href="https://hihotel.ir" title="Home" class="dropdown-toggle" data-toggle="dropdown">خانه<b
                                        class="caret"></b></a>
                            <ul class="dropdown-menu icon-fa-caret-up submenu-hover">
                                <li><a href="https://hihotel.ir/mashhadhotels/" title="">هتل های مشهد</a></li>
                                <li><a href="https://hihotel.ir/kishhotels/" title="">هتل های کیش</a></li>
                                <li><a href="https://hihotel.ir/tehran-hotels/" title="">هتل های تهران</a></li>
                                <li><a href="https://hihotel.ir/shirazhotels/" title="">هتل های شیراز</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!-- END / MENU-HEADER -->
</header>
<!-- END-HEADER -->

<!-- SLIDER -->
<section>
    <div class="backvid">
        <video autoplay="" loop="" muted="" class="vide">
            <source src="https://hihotel.ir/wp-content/uploads/2020/07/video.mp4" type="video/mp4">
            <source src="video/video.webm" type="video/webm">
            <source src="video/video.ogv" type="video/ogg">
            <!--                 <img alt="Third slide" src="https://hihotel.ir/wp-content/uploads/2020/07/video.mp4" class="img-responsive">
            -->
            <div class="carousel-caption">
                <h1>سامانه رزرواسیون های هتل</h1>
                <p><span class="line-t"></span>خدمات بی نظیر با امکانات عالی <span class="line-b"></span></p>
            </div>
        </video>
        <div class="group-sm group-middle social-box" style="display:grid;">
            <a href="https://www.facebook.com/hihotel" style="margin-top: 5px;"><img
                        src="https://hihotel.ir/wp-content/uploads/2019/07/facebook.svg" width="30" height="30"></a>
            <a href="https://www.linkedin.com/in/hihotel/" style="margin-top: 5px;"><img
                        src="https://hihotel.ir/wp-content/uploads/2019/07/linkedin.svg" width="30" height="30"></a>
            <a href="http://www.instagram.com/hihotel.ir" style="margin-top: 5px;"><img
                        src="https://hihotel.ir/wp-content/uploads/2019/07/instagram.svg" width="30" height="30"></a>
            <a href="https://t.me/hi_hotel" style="margin-top: 5px;"><img
                        src="https://hihotel.ir/wp-content/uploads/2019/07/telegram.svg" width="30" height="30"></a>
        </div>
    </div>
    <div id="formWrapper">
        <div class="cover" style="">
            <div class="num-step active"> ورود اطلاعات</div>
            <div class="num-step deactive-mobile">استعلام</div>
            <div class="num-step deactive-mobile">پرداخت</div>
        </div>
        <div id="form" class="mobilecover" style="direction:rtl;border-bottom:1px solid #909090;">
            <div class="row show-mobile-information" style="width: 80%;margin: auto;">
                <div class="hotel-detail-first col-6" style="text-align: center">
                    <img  class="mobile-img" src="https://hihotel.ir/panel/images/1720hotel-ghasr-talaee-double-atrium-2-1024x683.gif "
                         width="150" height="150" style="border-radius:10px;">
                    <div class="hotelname">{{$name['hotel_name']}}</div>
                    <div class="hotel">{{$hotel_star_name}}</div>
                    <div class="hotel">نام اتاق: {{$name['room_type_name']}}</div>
                    <div class="hotel" style="margin-bottom: 10px;">نوع تخت : {{$name['bed_type_name']}}</div>
                </div>
                <div class="hotel-detail-two col-6" style="text-align: center;margin-right: 15px;">
                    <div class="hotelname">کد رزرو: {{$voucher}}</div>
                    <div class="hotelname">تاریخ ورود: {{$request->date_booking_start}}</div>
                    <div class="hotelname">تاریخ خروج: {{$request->date_booking_end}}</div>
                    <div class="hotel">تعداد نفرات: {{$roomarray->person_number}} نفر</div>
                    <div class="hotel">@if($roomarray->breakfast == 1)
                            صبحانه
                        @endif
                        @if($roomarray->dinner == 1)
                            ناهار
                        @endif
                        @if($roomarray->lunch == 1)
                            شام
                        @endif
                    </div>
                    <div class="hotel">مجموع : {{ number_format($total->price * (intval($request->tedad))) }}تومان
                    </div>
                    <div class="hotel">تخفیف : {{number_format((intval($total->price)*$request->tedad - $amount))}}
                        تومان
                    </div>
                    <div class="hotelname" style="color:red;font-size:18px;">مبلغ نهایی:
                        <span class="price">{{number_format($amount)}}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="fard-form">
                    <form class="form-mobile" action="{{route('pardakht.inquiry')}}" method="POST" style="">
                        @csrf
                        <div class="row">
                            <div class="form-item">
                                <p class="form-label">نام و نام خانوادگی</p>
                                <p class="mobile-label">نام و نام خانوادگی</p>
                                <input type="text" name="family" id="email" class="form-style" autocomplete="on"
                                       required>
                            </div>
                            <div class="form-item">
                                <p class="form-label">ایمیل</p>
                                <p class="mobile-label">ایمیل</p>
                                <input type="email" name="name" id="mobile" class="form-style" autocomplete="on"
                                       required>
                            </div>
                        </div>
                        <input type="hidden" name="sex" value="1" required>
						<input type="hidden" name="melli_number" value="1" required>
                        <div class="col-lg-12">
                            <hr>
                        </div>
                        <div class="form-item">
                            <input type="text" class="discount_code">
                            <input class="form-controll" name="id" type="hidden" value="{{$save_reserve_id}}">
                            <p class="btn btn-success diss" onclick="discount()" style="margin-bottom:0px;">ثبت کد
                                تخفیف</p>
                            <input type="hidden" class="voucher" value="{{$voucher}}">
                            <span class="payam" style="color:red;display:block;"></span>
                            <span class="payam-success success" style="display:block;color: green"></span>
                        </div>
                        <div class="form-item">
                            <input type="hidden" name="save_reserve_id" value="{{$save_reserve_id}}">
                            <input type="submit" class="login pull-right" value="تایید">
                            <div class="clear-fix"></div>
                        </div>
                    </form>
                </div>
                <div class="hotel-detail-first desktop">
                    <img src="{{$roomarray->pic}}" width="150" height="150" style="border-radius:10px;">
                    <div class="hotelname">{{$name['hotel_name']}}</div>
                    <div class="hotel">{{$hotel_star_name}}</div>
                    <div class="hotel">نام اتاق: {{$name['room_type_name']}}</div>
                    <div class="hotel">نوع تخت : {{$name['bed_type_name']}}</div>
                </div>
                <div class="hotel-detail-two desktop">
                    <div class="hotelname">کد رزرو: {{$voucher}}</div>
                    <div class="hotelname">تاریخ ورود : {{$request->date_booking_start}}</div>
                    <div class="hotelname">تاریخ خروج : {{$request->date_booking_end}}</div>
                    <div class="hotel">تعداد نفرات: {{$roomarray->person_number}} نفر</div>
                    <div class="hotel">@if($roomarray->breakfast == 1)
                            صبحانه
                        @endif
                        @if($roomarray->dinner == 1)
                            ناهار
                        @endif
                        @if($roomarray->lunch == 1)
                            شام
                        @endif
                    </div>
                    <div class="hotel">مجموع : {{ number_format($total->price * $request->tedad) }} تومان</div>
                    <div class="hotel">تخفیف : {{number_format((intval($total->price) * $request->tedad)-$amount)}}
                        تومان
                    </div>
                    <div class="hotelname" style="color:red;font-size:18px;">مبلغ نهایی: <span
                                class="price">{{number_format($amount)}}</span></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--FOOTER-->
<footer class="footer-sky footer-top">
    <!-- /footer-top -->
    <div class="footer-mid">
        <div class="container">
            <div class="row padding-footer-mid">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="footer-logo text-center list-content">
                        <a href="https://hihotel.ir" title="Skyline"><img src="https://hihotel.ir/wp-content/themes/Hotell/image/logo5.png" alt="Image"></a>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 col-lg-offset-1 col-md-offset-1  ">
                    <div class="list-content">
                        <ul>
                            <li><a href="https://trustseal.enamad.ir/?id=132893&Code=Gyj2YFtus8Nf1aaWmL0R" title="">نماد اعتماد الکترونیکی</a></li>
                            <li><a href="https://hihotel.ir/enamad/" title="">قوانین و مقررات</a></li>
                            <li><a href="#" title="">به ما اعتماد کنید</a></li>
                            <li><a href="#" title="">پشتیبانی 24 ساعته</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 col-lg-offset-1 col-md-offset-1 ">
                    <div class="list-content ">
                        <ul>
                            <li><a href="#" title="">موقعیت ما در نقشه</a></li>
                            <li><a href="#" title="">نماد اعتماد الکترونیکی</a></li>
                            <li><a href="about.html" title="">درباره ما</a></li>
                            <li><a href="contact.html" title="">تماس با ما</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 col-lg-offset-1 col-md-offset-1">
                    <div class="list-content ">
                        <ul>
                            <li><a href="#" title="">پرسش های متدوال</a></li>
                            <li><a href="#" title="">اخبار گردشگری</a></li>
                            <li><a href="gallery_1.html" title="">تصاویر و ویدئوها</a></li>
                            <li><a href="restaurant_1.html" title="">تورهای گردشگری</a></li>
                            <li><a href="#" title="">بلیط هواپیما</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-padding">
                    Copyright © 2017 by <a href="https://hihotel.ir" title="">HiHotel Developer.</a><br> HiHotel Revolution in Tourism Industry </div>
                <form name="contentForm" enctype="multipart/form-data" method="post" action="">
                    <div class="ct-footer-pre text-center-lg">
                        <div ><a href="https://hihotel.ir/enamad/" target="_blank">
                                <img class="namad-icon" src="https://hihotel.ir/wp-content/themes/Hotell/image/enamad6.png" width="60" height="60">
                                <img class="namad-icon" src="https://hihotel.ir/wp-content/themes/Hotell/image/samandehi3.png" width="60" height="60">
                                <img class="namad-icon" src="https://hihotel.ir/wp-content/themes/Hotell/image/pasargad.png" width="60" height="60">
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</footer>
<!-- END / FOOTER-->


<!-- LOAD JQUERY -->
<script type="text/javascript" src="https://hihotel.org/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="https://hihotel.org/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="https://hihotel.org/js/bootstrap.min.js"></script>
<!-- Custom jQuery -->
<script type="text/javascript" src="https://hihotel.org/js/sky.js"></script>

</body>
<style>
    html,
    body {
        padding: 0;
        margin: 0;
        font-family: IRANSans, sans-serif !important;
        direction: rtl;
        text-align: right;
    }

    @media (max-width: 1024px) {
        .num-step {
            background-color: #fff;
            width: 90%;
            padding-right: 10px;
            border-radius: 10px;
            height: 50px;
            padding-top: 10px;
            display: block;
            margin: auto;
        }

		.form-label{
			display:none;
		}

		.mobile-img{
			padding:10px;
		}

		.col-6{
			flex: 0 0 45% !important;
    		max-width: 45% !important;
    		padding: 0px !important;
		}

		.form-mobile{
			width:80%;
			display:block;
			margin:auto;
		}

		div#form {
        position: absolute;
        /*        height: auto;
        */        background-color: #ffffff69;
        margin: auto;
        border-radius: 5px;
        padding-bottom: 20px;
        left: 220px;
        margin-top: -200px;
		left: 0px !important;
    	right: 0px !important;
    	top: 280px;
    	width: 100%
    	}

        .deactive-mobile {
            display: none !important;
        }

        .fard-form {
            width: 90%;
        }

        .information {
            display: block;
            margin: auto;
        }

        div.form-item {
            width: 100%;
        }

        input.form-style {
            width: 100% !important;
        }

        .formLabel {
            display: none;
        }

        div#form {
            left: 0px !important;
            right: 0px !important;
            top: 280px;
        }

        div.form-item {
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .mobile-label {
            margin-bottom: 5px;
        }
        .show-mobile-information{
			border-bottom: 3px dotted;
			margin:0px;
			margin-bottom:10px;
        }
        .desktop{
            display: none;
        }
        .pull-right{
            width: 50% !important;
            display: block;
            margin: auto;
            float: none !important;
        }
        .information{
            margin-bottom: 5px !important;
            margin-top:20px;
        }
        .form-item{
            margin-right: 0px !important;
        }
        .fard-form{
            margin: auto !important;
        }
        .cover{
            margin-top: 10px;
            text-align: center;
        }
    }

    @media (min-width: 1025px) {
        .num-step {
            background-color: #fff;
            width: 28%;
            padding-right: 10px;
            border-radius: 10px;
            height: 50px;
            padding-top: 10px;
            display: inline-block;
        }
        .cover{
            margin-top: 20px;text-align: center;
        }
        .form-item {
            width: 45%;
        }

		div#form {
        position: absolute;
        width: 90%;
        height: 70% !important;
        /*        height: auto;
        */        background-color: #ffffff69;
        margin: auto;
        border-radius: 5px;
        padding: 20px;
        left: 220px;
        margin-top: -200px;
    	}

        .fard-form {
            width: 60%;
            border-left: 5px dotted;
        }

        .hotel-detail-first {
            /*            background-color: #ffffff36;
            */            width: 15%;
            position: absolute;
            right: 65%;
            top:0;
        }

        .hotel-detail-two {
            /*            background-color: #ffffff36;
            */            width: 15%;
            position: absolute;
            right: 80%;
            top:0;
        }

        div#form {
            margin-left: -180px !important;
            top: 320px;
        }

        div.form-item {
            margin-bottom: 20px;
            margin-top: 20px;
        }

        .mobile-label {
            display: none;
        }
        .show-mobile-information{
            display: none;
        }
    }

    .active {
        background-color: #e61b3e;
        color: #fff;
        font-weight: 800;
    }

    .hotelname {
        font-size: 12px;
        font-weight: 800;
        margin-top: 14px;
        margin-right: 5px;
    }

    .hotel {
        font-size: 12px;
        margin-top: 8px;
    }

    .information {
        margin-bottom: 45px;
        width: 150px;
        background-color: #f46694;
        font-weight: 800;
        color: white;
        text-align: center;
        height: 35px;
        padding-top: 8px;
        border-radius: 10px;
        font-size: 14px;
    }

    #formWrapper {
        /*        background: rgba(0, 0, 0, .2); */
        height: 80%;
        position: absolute;
        top: 100px;
        left: 0;
        transition: all .3s ease;
        width: 90%;
        margin-right: 5%;
        margin-left: 5%;
    }

    .darken-bg {
        background: rgba(0, 0, 0, .5) !important;
        transition: all .3s ease;
    }

    div.form-item {
        position: relative;
        display: inline-block;
        margin-right: 20px;
        float: left;
    }

    input {
        transition: all .2s ease;
    }

    input.form-style {
        color: #8a8a8a;
        display: block;
        width: 90%;
        height: 44px;
        padding: 5px 5%;
        border: 1px solid #ccc;
        -moz-border-radius: 27px;
        -webkit-border-radius: 27px;
        border-radius: 27px;
        -moz-background-clip: padding;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        background-color: #fff;
        font-family: 'HelveticaNeue', 'Arial', sans-serif;
        font-size: 105%;
        letter-spacing: .8px;
    }

    div.form-item .form-style:focus {
        outline: none;
        border: 1px solid #5d5d5d;
        color: #5d5d5d;
    }

    div.form-item p.formLabel {
        position: absolute;
        right: 5%;
        top: 12px;
        transition: all .4s ease;
        color: #bbbbbbad;
        font-size: 13px;
    }

    .formTop {
        top: -22px !important;
        left: 0px;
        background-color: #fff;
        padding: 0 5px;
        font-size: 14px;
        color: #5d5d5d !important;
    }

    .formStatus {
        color: #f46694 !important;
    }

    input[type="submit"].login {
        float: right;
        width: 112px;
        height: 37px;
        -moz-border-radius: 19px;
        -webkit-border-radius: 19px;
        border-radius: 19px;
        -moz-background-clip: padding;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        background-color: rgb(230, 27, 62);
        border: 1px solid rgb(230, 27, 62);
        border: none;
        color: #fff;
        font-weight: bold;
    }

    input[type="submit"].login:hover {
        background-color: #fff;
        border: 1px solid #f46694;
        color: #f46694;
        cursor: pointer;
    }

    input[type="submit"].login:focus {
        outline: none;
    }
    .footer-top{
        margin-top:600px;
    }
    .textreserve{
        flex: 3 0 25%;
        max-width: 50%;
        left: 60%;
    }
    .social-box{
        position: absolute;
        right: 30px;
        top: 50%;
        transform: translateY(-50%);
    }
    .slant-box {
        content: '';
        display: inline-block;
        height: 523px;
        transform: rotate(41deg);
        width: 3px;
        background: white;
    }
    .insert_number {
        background-color: rgb(203, 42, 48);
        width: 100vw;
        height: 100vh;
    }

    .insert_shomare_mobile {
        text-align: center;
        width: 250px;
        height: 170px;
        background-color: #ffffff54;
        padding: 20px;
        display: block;
        /*        margin: auto;
        */        margin-top: calc(50vh - 70px);
        border-radius: 20px;
        box-shadow: 14px 19px 20px 12px #1d212452;
    }

    .text_mobile {
        height: 200px;
        margin: auto;
        margin-top: calc(50vh - 150px);
        width:900px;
        color: white;
        text-align: justify;
        font-size: 18px;
        font-weight: bold;
    }


    #triangle-down {
        width: 0;
        height: 0;
        border-left: 120px solid transparent;
        border-right: 30px solid transparent;
        border-top: 30px solid white;
        display: block;
        margin: auto;
    }
    @media (min-width: 1200px) {
        .backvid {
            position: absolute;
            z-index: 0;
            top: 0px;
            left: 0px;
            bottom: 0px;
            right: 0px;
            overflow: hidden;
            background-size: cover;
            background-color: transparent;
            background-repeat: no-repeat;
            background-position: 50% 50%;
            background-image: none;
        }
        .vide{
            margin: auto;
            position: absolute;
            z-index: -1;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            visibility: visible;
            opacity: 1;
            /* width: 1351px; */
            height: auto;
        }
    }
    @media (max-width:768px) {
        .footer-sky .footer-mid .padding-footer-mid .list-content{
            display: none;
        }
		.header-top{
			display:none !important;
		}

        #formWrapper{
            height: 140%;
			top: 50px !important;
			zoom:0.9;
        }
        .footer-top{
            margin-top:820px;
        }
        .insert_shomare_mobile {
            text-align: center;
            width: 250px;
            /* height: 170px; */
            background-color: #ffffff54;
            padding: 15px;
            display: block;
            /* margin: auto; */
            margin-top: calc(50vh - 190px);
            border-radius: 20px;
            box-shadow: -20px 19px 20px 12px #1d212452;
            margin-right: 65px;
        }

        .text_mobile {
            height: 0px;
            margin: auto;
            /*        margin-top: calc(50vh - 150px);
            */        width:200px;
            color: white;
            text-align: justify;
            font-size: 18px;
            font-weight: bold;
        }
        .vide{
            margin: auto;
            position: absolute;
            z-index: -1;
            top: 15%;
            left: 50%;
            transform: translate(-50%, -50%);
            visibility: visible;
            opacity: 1;
            /* height: 500px !important; */
            position: fixed;
        }
        .header-sky{
            position: fixed;
            z-index: 999;
            right: 0;
            left: 0;
            top: 0;
            width: 360px;
        }
        .social-box {
    	zoom: 0.95;
    	position: absolute;
    	right: 30px;
    	top: 197px;
    	transform: translateY(-50%);
    	z-index: 1;
        }
    /*.navbar1-nav{
        display: flex;
        flex-direction: row;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }*/
</style>

<script type="text/javascript">
    function discount() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '{{route('pardakht.discount')}}',
            type: 'POST',
            data: {
                _token: CSRF_TOKEN,
                voucher: $(".voucher").val(),
                discount_code: $(".discount_code").val(),
                id:{{$save_reserve_id}}
            },
            dataType: 'JSON',
            success: function (data) {
                if(parseInt(data)){
                    $('.price').html(data);
                    $('.payam-success').html('کد تخفیف با موفقیت ثبت شد.');
                    $('.discount_code').prop('disabled', true);
                    $('.diss').hide();
                }else{
                    $('.payam').html(data);
                }
                console.log(data);
            }
        });
    $(document).ready(function(){
        var formInputs = $('input[type="email"],input[type="tel"],input[type="text"]');
        formInputs.focus(function() {
            $(this).parent().children('p.formLabel').addClass('formTop');
            $('div#formWrapper').addClass('darken-bg');
        });
        formInputs.focusout(function() {
            if ($.trim($(this).val()).length == 0){
                $(this).parent().children('p.formLabel').removeClass('formTop');
            }
            $('div#formWrapper').removeClass('darken-bg');
        });
        $('p.formLabel').click(function(){
            $(this).parent().children('.form-style').focus();
        });
    });
</script>
</html>