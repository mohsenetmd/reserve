<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<body style="direction:rtl;text-align: right;">
<div id="formWrapper">
    <div id="form" class="mobilecover" style="direction:rtl;">
        <div style="width:95%;">
            <div class="information">های هتل | سیستم جامع رزرواسیون هتل های  ایران</div>
            <div class="" style="margin-right:120px;margin-bottom: 5px;">کد رزرو : {{$save_reserve->voucher}}</div>
            <div class="row" style="padding-right: 150px;">
                <div class="textbox">
                    <span Style="margin-right:50px; margin-top:50px;">مسافر محترم : آقای / خانم {{$fard[0]->family}}</span>
                    <br>
                    <span Style="margin-right:50px;margin-top:5px;font-weight:800;">نام هتل: {{$name['hotel_name']}} </span>
                    <br>
                    <span Style="margin-right:50px;margin-top:5px;">تاریخ ورود: {{$save_reserve->date_booking_start}}</span>
                    <br>
                    <span style="margin-right:50px;margin-top:5px;">تاریخ خروج : {{$save_reserve->date_booking_end}}</span>
                    <br>
                    <span style="margin-right:50px;margin-top:5px;">ساعت تحویل : 14:00</span>
                    <br>
                    <span style="margin-right:50px;margin-top:5px;">ساعت تحویل : 12:00</span>
                </div>
                <div class="textbox">
                    <span Style="margin-right:10px; margin-top:50px;font-weight:800;">با سلام و تقدیم احترام</span>
                    <br>
                    <span Style="margin-right:10px;font-size: 11px;">با توجه به ثبت اطلاعات و رزرو قطعی شما در هتل اطلاعات رزرو کننده به شرح زیر می باشد:</span>
                    <br>
                    <span style="margin-right:10px;font-size: 12px;font-weight:800;">نام رزرو کننده: آقای / خانم {{$fard[0]->family}}</span>
                    <br>
                    <span style="margin-right:10px;font-size: 12px;font-weight:800;">شماره تماس: {{$mobile}}</span>
                    <br>
                    <span style="margin-right:10px;font-size: 12px;font-weight:800; ">نوع  پرداختی: نقدی</span>
                </div>                    </div>
            <div class="row" style="padding-right: 150px;">
                <div class="resinfo" style="font-weight: 800;">اطلاعات اتاق:
                    <br>
                    <table style="margin-top: 0px;">
                        <tr>
                            <td style="width: 5%;">ردیف</td>
                            <td style="width: 20%;">نوع اتاق</td>
                            <td style="width: 14%;">نوع غذا</td>
                            <td style="width: 30%;">نام میهمان</td>
                            <td style="width: 5%;">تعداد نفرات</td>
                            <td style="width: 5%;">نفر اضافه</td>

                        </tr>
                        <tr>
                            <td>1</td>
                            <td>{{$name['room_type_name']}}</td>
                            <td>@if($roomarray->breakfast == 1)
                                    صبحانه
                                @endif
                                @if($roomarray->dinner == 1)
                                    ناهار
                                @endif
                                @if($roomarray->lunch == 1)
                                    شام
                                @endif</td>
                            <td>آقای {{$fard[0]->family}}</td>
                            <td>{{$roomarray->person_number}}</td>
                            <td>ندارد</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row" style="padding-right: 150px;">
                <div class="respoint">ملاحظات:
                    <br>
                    <li>در جزیره کیش زمان تحویل اتاق ساعت : 15:00 و تخلیه اتاق : 14:00 می باشد.</li>
                    <li>در مشهد مقدس . سایر شهرها زمان تحویل اتاق ساعت 14:00 و تخلیه اتاق 12:00 می باشد.</li>
                    <li>هتل چارتر غیرقابل استرداد می باشد.</li>
                    <li>داشتن شناسنامه همراه مسافر الزامی می باشد.</li>
                    <li>قیمت های اعلامی بدون هزینه ترنسفر می باشد.</li>
                </div>
            </div>
            <div class="row" style="padding-right: 150px;">
                <div class="reshihotel">شماره تماس های ضروری
                    <br><span style="font-weight: 800;">پشتیبانی 24 ساعته: </span>
                    <span>09057198001</span>
                    <br><span style="font-weight: 800;">شماره دفتر : </span>
                    <span>38584050-051</span>
                </div>
            </div>
            <div class="row" style="padding-right: 150px;">
                <div class="rescard">اطلاعات حساب بانکی
                    <br><span style="font-weight: 800;">شماره حساب / شبا : .............................</span>
                    <br><span style="font-weight: 800;">شماره کارت : ...................................</span>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<style type="text/css">
    body{
        background: url(https://hihotel.ir/wp-content/uploads/2020/06/backmobilefinal.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;

    }
    table, th, td {
        border: 1px solid grey;
        border-radius: 5px;
    }
    tr{
        text-align: center;
        font-size: 15px;
        font-weight: 400;
    }
    tbody{
        width: 100%;
    }
    li{
        font-size: 14px;
        font-weight: 800;
        padding: 10px;
    }
    .textbox{
        width: 45%;
        border: 1px solid grey;
        height: 250px;
        border-radius: 10px;
        padding-top: 50px;
    }
    .resinfo{
        padding: 20px;
        width: 90%;
        border: 1px solid grey;
        height: 150px;
        border-radius: 10px;
    }
    .respoint{
        padding: 20px;
        width: 90%;
        border: 1px solid grey;
        height: 250px;
        border-radius: 10px;
    }
    .reshihotel{
        padding: 20px;
        width: 90%;
        margin-top: 10px;
        margin-bottom: 10px;
        border: 1px solid grey;
        height: 100px;
        border-radius: 10px;
    }
    .rescard{
        padding: 20px;
        width: 90%;
        margin-top: 10px;
        border: 1px dotted grey;
        height: 150px;
        border-radius: 10px;
    }
    .num-step{
        background-color: #fff;
        width: 28%;
        padding-right: 10px;
        border-radius: 10px;
        height: 50px;
        padding-top: 10px;
        display: inline-block;
    }
    .active{
        background-color: #e61b3e;
        color: #fff;
        font-weight: 800;
    }
    .hotelname{
        font-size: 12px;
        font-weight: 800;
        margin-top: 14px;
        margin-right: 5px;
    }
    .hotel{
        font-size: 12px;
        margin-top: 8px;
    }
    .information{
        margin-bottom: 5px;
        width: 300px;
        background-color: #f46694;
        font-weight: 800;
        color: white;
        text-align: center;
        height: 35px;
        padding-top: 8px;
        border-radius: 10px;
        font-size: 14px;
        margin-right:120px;
    }
    #formWrapper{
        /*    background: rgba(0,0,0,.2);
        */    width:100%;
        height:100%;
        position: absolute;
        top:0;
        left:0;
        transition:all .3s ease;}
    .darken-bg{background: rgba(0,0,0,.5) !important; transition:all .3s ease;}

    /*@media (max-width: 768px){
    .hotelname {
        font-size: 15px;
        font-weight: 800;
        margin-top: 14px;
        margin-right: 5px;
    }*/
    div#form{
        position: absolute;
        width: 90%;
        height: 320px;
        height: auto;
        background-color: #fff;
        margin: auto;
        border-radius: 5px;
        padding: 20px;
        left: 220px;
        top: 320px;
        margin-left: -180px;
        margin-top: -200px;
    }
    div.form-item{position: relative; display: block; margin-bottom: 20px;margin-top: 20px;margin-right: 20px;}
    input{transition: all .2s ease;}
    input.form-style{
        color:#8a8a8a;
        display: block;
        width: 90%;
        height: 44px;
        padding: 5px 5%;
        border:1px solid #ccc;
        -moz-border-radius: 27px;
        -webkit-border-radius: 27px;
        border-radius: 27px;
        -moz-background-clip: padding;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        background-color: #fff;
        font-family:'HelveticaNeue','Arial', sans-serif;
        font-size: 105%;
        letter-spacing: .8px;
    }
    div.form-item .form-style:focus{outline: none; border:1px solid #5d5d5d; color:#5d5d5d; }
    div.form-item p.formLabel {
        position: absolute;
        right: 5%;
        top: 12px;
        transition: all .4s ease;
        color: #bbbbbbad;
        font-size: 13px;}
    .formTop{top:-22px !important; left:0px; background-color: #fff; padding:0 5px; font-size: 14px; color:#5d5d5d !important;}
    .formStatus{color:#f46694 !important;}
    input[type="submit"].login{
        float:right;
        width: 112px;
        height: 37px;
        -moz-border-radius: 19px;
        -webkit-border-radius: 19px;
        border-radius: 19px;
        -moz-background-clip: padding;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        background-color: #f46694;
        border:1px solid #f46694;
        border:none;
        color: #fff;
        font-weight: bold;
    }
    input[type="submit"].login:hover{background-color: #fff; border:1px solid #f46694; color:#f46694; cursor:pointer;}
    input[type="submit"].login:focus{outline: none;}
</style>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        var formInputs = $('input[type="email"],input[type="tel"]');
        formInputs.focus(function() {
            $(this).parent().children('p.formLabel').addClass('formTop');
            $('div#formWrapper').addClass('darken-bg');
        });
        formInputs.focusout(function() {
            if ($.trim($(this).val()).length == 0){
                $(this).parent().children('p.formLabel').removeClass('formTop');
            }
            $('div#formWrapper').removeClass('darken-bg');
        });
        $('p.formLabel').click(function(){
            $(this).parent().children('.form-style').focus();
        });
    });
</script>
