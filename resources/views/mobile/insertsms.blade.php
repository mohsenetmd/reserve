@extends('layouts.app')
@section('content')
    <form method="post" action="{{route('mobile.acceptsms')}}">
        @csrf
		<label>کد تایید را وارد کنید. </label>
        <input type="number" id="accept" name="accept" required>
        <input type="hidden" value="{{request()->mobile}}" name="mobile" required>
        <input type="hidden" name="room" value="{{ request()->room }}">
        <input type="hidden" name="total" value="{{ request()->total }}">
        <input type="hidden" name="date_booking_start" value="{{ request()->date_booking_start }}">
        <input type="hidden" name="date_booking_end" value="{{ request()->date_booking_end }}">
        <input class="input_tedad" type="hidden" name="tedad" value="{{ request()->tedad }}">
        <input class="input_total_bed_price" type="hidden" name="total_bed_price" value="{{ request()->total_bed_price }}">
        <button type="submit">send</button>
    </form>

@endsection
