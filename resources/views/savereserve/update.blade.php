@extends('layouts.app')
@section('content')
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form method="post" action="{{route('date.updated')}}">
                    @csrf
                    @method('put')
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات اتاق</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    @foreach($reservetype as $item)
                                        <option value="{{$item->id}}
                                        @if($date->resrerve_type_id == $item->id)
                                            selected
                                            @endif
                                            ">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد اتاق</label>
                                <input type="text" class="form-control" name="number" required
                                       value="{{$date->number}}">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت</label>
                                <input type="text" class="form-control" name="price" required value="{{$date->price}}">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت با تخفیف</label>
                                <input type="text" class="form-control" name="discount_price"
                                       value="{{$date->discount_price}}"
                                       required>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تخت اضافه</label>
                                <select name="more_bed" class="form-control">
                                    <option value="0"
                                            @if($date->more_bed == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($date->more_bed == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price" value="{{$date->bed_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>کودک</label>
                                <select name="child" class="form-control">
                                    <option value="0"
                                            @if($date->child == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($date->child == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price" value="{{$date->child_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>صبحانه</label>
                                <input type="text" class="form-control" name="breakfast_price" value="{{$date->breakfast_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای صبحانه را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>ناهار</label>
                                <input type="text" class="form-control" name="lunch_price" value="{{$date->lunch_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای ناهار را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>شام</label>
                                <input type="text" class="form-control" name="dinner_price" value="{{$date->dinner_price}}">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای شام را وارد کنید.</small>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>صبحانه رایگان</label>
                                <select name="breakfast" class="form-control">
                                    <option value="0"
                                            @if($date->breakfast == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($date->breakfast == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ناهار رایگان</label>
                                <select name="lunch" class="form-control">
                                    <option value="0"
                                            @if($date->lunch == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($date->lunch == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>شام رایگان</label>
                                <select name="dinner" class="form-control">
                                    <option value="0"
                                            @if($date->dinner == 0)
                                            selected
                                        @endif>ندارد
                                    </option>
                                    <option value="1"
                                            @if($date->dinner == 1)
                                            selected
                                        @endif>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع غذا</label>
                                <select class="foodtype_search form-control" name="food_type_id">
                                    @foreach($foodtype as $item)
                                        <option value="{{$item->id}}"
                                                @if($date->food_type_id == $item->id)
                                                selected
                                            @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>قیمت نوع غذا</label>
                                <input type="text" class="form-control" name="price_food_type" value="{{$date->price_food_type}}">
                            </div>
                            <div class="form-check col-lg-4">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>تاریخ شروع</label>
                                <input type="text" class="form-control" name="date_booking_start" value="{{$date->date_booking_start}}">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>تاریخ پایان</label>
                                <input type="text" class="form-control" name="date_booking_end" value="{{$date->date_booking_end}}">
                            </div>
                            <input type="hidden" class="form-control" name="room_id" value="{{request()->room_id}}">
                            <input type="hidden" class="form-control" name="date_id" value="{{request()->date_id}}">
                            <input type="hidden" class="form-control" name="admin_created" value="{{$date->admin_created}}">
                            <input type="hidden" class="form-control" name="admin_updated" value="{{ Auth::user()->name }}">
                            <input type="hidden" class="form-control" name="person_number" value="0" required>
                            <input type="hidden" class="form-control" name="child_number" value="0">
                            <input type="hidden" class="form-control" name="bed_number" value="0">
                        </div>
                    </div>
                    <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.reservetype_search').select2();
            $('.foodtype_search').select2();
        });
    </script>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
