@extends('layouts.app')
@section('content')
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form method="post" action="{{route('savereserve.save')}}">
                    @csrf
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات رزرو</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    @foreach($reservetype as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد اتاق</label>
                                <input type="text" class="form-control" name="number" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>مجموع قیمت</label>
                                <input type="text" class="form-control" name="price" required v-model="price">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>مجموع قیمت با تخفیف</label>
                                <input type="text" class="form-control" name="discount_price" v-on:keyup="discount()"
                                       required v-model="discount_price">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد افراد</label>
                                <input type="text" class="form-control" name="discount_price" v-on:keyup="discount()"
                                       required v-model="discount_price">
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تخت اضافه</label>
                                <select name="more_bed" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>مجموع هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>کودک</label>
                                <select name="child" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>مجموع هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>مجموع هزینه صبحانه</label>
                                <input type="text" class="form-control" name="breakfast_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای صبحانه را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>مجموع هزینه ناهار</label>
                                <input type="text" class="form-control" name="lunch_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای ناهار را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>مجموع هزینه شام</label>
                                <input type="text" class="form-control" name="dinner_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای شام را وارد کنید.</small>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>صبحانه رایگان</label>
                                <select name="breakfast" class="form-control">
                                    <option value="1">دارد</option>
                                    <option value="0">ندارد</option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ناهار رایگان</label>
                                <select name="lunch" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label> شام رایگان</label>
                                <select name="dinner" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع غذا</label>
                                <select class="foodtype_search form-control" name="food_type_id">
                                    @foreach($foodtype as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>مجموع هزینه قیمت نوع غذا</label>
                                <input type="text" class="form-control" name="price_food_type">
                            </div>
                            <input type="hidden" class="form-control" name="room_id" value="{{request()->room_id}}">
                            <input type="hidden" class="form-control" name="admin_created" value="{{ Auth::user()->name }}">
                            <input type="hidden" class="form-control" name="admin_updated" value="{{ Auth::user()->name }}">
                            <input type="hidden" class="form-control" name="child_number" value="0">
                            <input type="hidden" class="form-control" name="bed_number" value="0">

                        </div>
                    </div>
                    <div id="app">
                        <date-component></date-component>
                    </div>
                    <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.reservetype_search').select2();
            $('.foodtype_search').select2();
        });
    </script>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
