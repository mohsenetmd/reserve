@extends('layouts.app')
@section('content')
    <form method="post" action="{{route('savereserve.update')}}">
        @csrf
        @method('put')
        <div>{{$hotel}}</div>
        <div>{{$room}}</div>
        <div>{{$totalprice}}</div>
        <div>{{$data[0]->date_booking_start}}</div>
        <div>{{$data[0]->date_booking_end}}</div>
		<div>تعداد اتاق{{$data[0]->tedad}}</div>
        <div class="form-group col-lg-4">
            <label>جواب استعلام</label>
            <select class="reservetype_search form-control" name="condition">
                    <option value="2"
                            @if($data[0]->condition == 2)
                                selected
                            @endif
                    >عدم تایید استعلام</option>
                    <option
                        @if($data[0]->condition == 3)
                        selected
                        @endif
                        value="3">تایید استعلام</option>
            </select>
            <input type="hidden" value="{{$update}}" name="admin_updated">
            <input type="hidden" value="{{$data[0]->id}}" name="id">
        </div>
        <button type="submit">ارسال</button>
    </form>
@endsection
