<?php
function toJalaliPlugins($g_y, $g_m, $g_d)
	{
		$g_days_in_month = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		$j_days_in_month = array(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29);

		$gy = $g_y - 1600;
		$gm = $g_m - 1;
		$gd = $g_d - 1;

		$g_day_no = 365 * $gy + divPlugin($gy + 3, 4) - divPlugin($gy + 99, 100) + divPlugin($gy + 399, 400);

		for ($i = 0; $i < $gm; ++$i)
			$g_day_no += $g_days_in_month[$i];
		if ($gm > 1 && (($gy % 4 == 0 && $gy % 100 != 0) || ($gy % 400 == 0)))
			$g_day_no++;
		$g_day_no += $gd;

		$j_day_no = $g_day_no - 79;

		$j_np = divPlugin($j_day_no, 12053);
		$j_day_no = $j_day_no % 12053;

		$jy = 979 + 33 * $j_np + 4 * divPlugin($j_day_no, 1461);

		$j_day_no %= 1461;

		if ($j_day_no >= 366) {
			$jy += divPlugin($j_day_no - 1, 365);
			$j_day_no = ($j_day_no - 1) % 365;
		}

		for ($i = 0; $i < 11 && $j_day_no >= $j_days_in_month[$i]; ++$i)
			$j_day_no -= $j_days_in_month[$i];
		$jm = $i + 1;
		$jd = $j_day_no + 1;

		return array($jy, $jm, $jd);
	}

function divPlugin($a, $b)
	{
		return (int)($a / $b);
	}

function jalaliCreat($date){
	$g_y=($date->created_at->format('y'));
	$g_m=($date->created_at->format('m'));
	$g_d=($date->created_at->format('d'));
	$g_y=2000+$g_y;
	$jalali=toJalaliPlugins($g_y, $g_m, $g_d);
	$showjalali=$jalali[0].'/'.$jalali[1].'/'.$jalali[2];
	return $showjalali;
}

function jalaliUpdate($date){
	$g_y=($date->updated_at->format('y'));
	$g_m=($date->updated_at->format('m'));
	$g_d=($date->updated_at->format('d'));
	$g_y=2000+$g_y;
	$jalali=toJalaliPlugins($g_y, $g_m, $g_d);
	$showjalali=$jalali[0].'/'.$jalali[1].'/'.$jalali[2];
	return $showjalali;
}
