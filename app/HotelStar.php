<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelStar extends Model
{
    protected $guarded=[];
    public function Hotel(){
        return $this->hasMany(Hotel::class);
    }
}
