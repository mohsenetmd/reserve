<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class 	WpPost extends Model
{
    protected $guarded=[];
    protected $primaryKey="ID";
    public $timestamps = false;
}
