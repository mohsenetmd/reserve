<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Date extends Model
{
	use SoftDeletes;
    protected $guarded=[];
    public function Room(){
        return $this->belongsTo(Room::class);
    }
}
