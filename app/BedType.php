<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BedType extends Model
{
    protected $guarded=[];
    public function Room(){
        return $this->hasMany(Room::class);
    }
}
