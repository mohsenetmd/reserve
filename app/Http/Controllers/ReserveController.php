<?php

namespace App\Http\Controllers;

use App\City;
use App\Date;
use App\Hotel;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Shetabit\Payment\Drivers\Pasargad\Utils\RSAProcessor;
use Shetabit\Payment\Invoice;
use Shetabit\Payment\Facade\Payment;
use SoapClient;

/**
 * Class ReserveController
 * @package App\Http\Controllers
 */
class ReserveController extends Controller
{
    public $selectRoom;
    public $onlineBookingCondition = '2';
    public $roomId;

    /*
    |--------------------------------------------------------------------------
    | be name khoda
    | dar in ghesmat ebteda darkhast ajax pardazesh mishavad
    |--------------------------------------------------------------------------
     */

    public function ajax()
    {
        $total = $this->showHotelAjax();
        $hotel = Hotel::where('wp_post_id', \request()->id)->get();
        $room = Room::where('hotel_id', $hotel[0]->id)->get();
        $returnHTML = view('reserve.ajaxshow', ['room' => $room, 'total' => $total])->render();
        return response()->json($returnHTML);
    }

    public function showHotel()
    {
        $hotel = Hotel::find(11)->get();
        $output = array();
        $i = 0;
        foreach ($hotel as $item) {
            $output[$i] = $item;
            //dd($output);
            $room = Room::where('hotel_id', $item->id)->get();
            $j = 0;
            foreach ($room as $item2) {
                $output[$i]->room[$j] = $item2;
                if ($item2->reserve_type_id != 3) {
                    $this->roomId = $item2->id;
                    $total[$j] = array($this->showRoom());
                }
                $j += 1;
            }
            $i += 1;
        }
        return view("reserve.showhotel", ['hotel' => $hotel, 'total' => $total]);
        //return $total;
    }

    public function showHotelAjax()
    {
        $hotel = Hotel::where('wp_post_id', \request()->id)->get();
        $room = Room::where('hotel_id', $hotel[0]->id)->get();
        $j = 0;
        foreach ($room as $item2) {
            if ($item2->reserve_type_id != 3) {
                $this->roomId = $item2->id;
                $total[$j] = array($this->showRoom());
            }
            $j += 1;
        }
        return $total;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRoom()
    {
        $selectRoom = $this->selectRoom();
        $this->selectRoom = $selectRoom->id;
        $totalPrice = $this->totalPrice();
        if ($totalPrice['number'] > 0) {
            return $totalPrice;
        }
    }

    /**
     * @param $inputday
     * @return float|int|mixed
     */
    public function nthday($inputday)
    {
        $inputday = str_replace("/", '', $inputday);
        $time = str_split($inputday, 2);
        $year = $time[0] . $time[1];
        $month = $time[2];
        $day = $time[3];
        if ($year > 1399) {
            echo 'ok';
        }

        if ($month > 6) {
            $days = 6 * 31 + ($month - 7) * 30;
        } else {
            $days = 31 * ($month - 1);
        }

        $days = $days + $day;
        return $days;
    }

    public function inversnthday($inputday)
    {
        $year = 1399 + ($inputday / 365);
        $year = round($year, 0, PHP_ROUND_HALF_DOWN);
        $year = intval($year);
        $month = ($inputday / 31);
        $month = round($month, 0, PHP_ROUND_HALF_DOWN);
        $month = intval($month);
        $month = $month + 1;
        $day = $inputday - (($month - 1) * 31);
        $month = '0' . $month;
        if ($month > 6) {
            $inputday = $inputday - 186;
            $month = 6 + ($inputday / 30);
            $month = round($month, 0, PHP_ROUND_HALF_DOWN);
            $month = intval($month);
            $day = $inputday - 186 - (30 * ($month - 1));
            $month = $month + 1;
            if ($month < 10) {
                $month = '0' . $month;
            }
        }
        if ($day < 10) {
            $day = '0' . $day;
        }
        $output = $year . "/" . $month . "/" . $day;
        return $output;
    }

    /**
     * @return mixed
     */
    public function reservationDays()
    {
        $day = 0;
        $j = 0;
        $reservationDays = array();
        $passengerArrivalTimeDay = $this->nthday(\request()->date_booking_start);
        $checkStart = $passengerArrivalTimeDay;
        $passengerDepartureTimeDay = $this->nthday(\request()->date_booking_end);
        $checkEnd = $passengerDepartureTimeDay;
        $dayNumber = $passengerDepartureTimeDay - $passengerArrivalTimeDay;
        $roomTimeDay = $this->roomTimeDay();
        $min = array(0);
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            $min[$i] = $roomTimeDay[$i]->date_booking_start;
        }
        $minver = min($min);
        if ($passengerArrivalTimeDay < $minver) {
            $passengerArrivalTimeDay = $minver;
            $checkStart = $minver;
        }
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            if ($passengerDepartureTimeDay >= $roomTimeDay[$i]->date_booking_end) {
                if ($passengerArrivalTimeDay >= $roomTimeDay[$i]->date_booking_start && $passengerArrivalTimeDay <= $roomTimeDay[$i]->date_booking_end) {
                    for ($day = $passengerArrivalTimeDay; $day <= $roomTimeDay[$i]->date_booking_end; $day++) {
                        $reservationDays[$j] = $roomTimeDay[$i];
                        $j += 1;
                    }
                    $passengerArrivalTimeDay = $day;
                }
            } elseif ($passengerDepartureTimeDay < $roomTimeDay[$i]->date_booking_end)
                if ($passengerArrivalTimeDay >= $roomTimeDay[$i]->date_booking_start && $passengerArrivalTimeDay <= $roomTimeDay[$i]->date_booking_end) {
                    {
                        for ($day = $passengerArrivalTimeDay; $day <= $passengerDepartureTimeDay; $day++) {
                            $reservationDays[$j] = $roomTimeDay[$i];
                            $j += 1;
                        }
                    }
                }
        }
        $parcham = 0;
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            if ($roomTimeDay[$i]->reserve_type_id == 4) {
                if ($checkStart >= $roomTimeDay[$i]->date_booking_start && $checkStart <= $roomTimeDay[$i]->date_booking_end) {
                    $parcham = 1;
                }
                if ($checkEnd >= $roomTimeDay[$i]->date_booking_start && $checkEnd <= $roomTimeDay[$i]->date_booking_end) {
                    $parcham = 1;
                }
                if ($checkStart <= $roomTimeDay[$i]->date_booking_start && $checkEnd >= $roomTimeDay[$i]->date_booking_end) {
                    $parcham = 1;
                }
            }
        }
        $room = $this->roomDay();
        for ($i = count($reservationDays); $i < $dayNumber; $i++) {
            $reservationDays[$i] = $room;
            $reservationDays[$i]->number = 1;
        }
        $reservationDays['parcham'] = $parcham;
        return $reservationDays;
    }

    /**
     * @return mixed
     */
    public function roomTimeDay()
    {
        $room = Room::find($this->selectRoom);
        $roomTime = $room->Date()->get();
        $roomTimeDay = $roomTime;
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            $roomTimeDay[$i]->date_booking_start = $this->nthday($roomTimeDay[$i]->date_booking_start);
            $roomTimeDay[$i]->date_booking_end = $this->nthday($roomTimeDay[$i]->date_booking_end);
        }
        return $roomTimeDay;
    }

    public function roomDay()
    {
        $room = Room::find($this->selectRoom);
        return $room;
    }

    public function selectRoom()
    {
        $room = Room::find($this->roomId);
        return $room;
    }


    /**
     * @return mixed
     */
    public function totalPrice()
    {
        $reservationDays = $this->reservationDays();
        $bed_price = 0;
        $child_price = 0;
        $discount_price = 0;
        $number = 0;
        for ($i = 0; $i < (count($reservationDays) - 1); $i++) {
            if ($number >= $reservationDays[$i]->number || $number == 0) {
                $number = $reservationDays[$i]->number;
            }
            $bed_price = $bed_price + $reservationDays[$i]->bed_price;
            $child_price = $child_price + $reservationDays[$i]->child_price;
            $discount_price = $discount_price + $reservationDays[$i]->discount_price;
        }
        $total = [
            'bed_price' => $bed_price,
            'child_price' => $child_price,
            'discount_price' => $discount_price,
            'number' => $number,
            'all' => $reservationDays,
        ];
        return $total;
    }

    public function complete()
    {
        //$onlineBookingCondition
        $room = Room::find(\request()->room_id);
        $date = $room->Date()->get();
        if ($room->reserve_type_id == $this->onlineBookingCondition) {
            Date::create([
                'admin_created' => Auth::user()->id,
                'admin_updated' => Auth::user()->id,
                'bed_price' => $room->bed_price,
                'child_price' => $room->child_price,
                'date_booking_end' => $this->inversnthday(\request()->date_booking_end),
                'date_booking_start' => $this->inversnthday(\request()->date_booking_start),
                'discount_price' => $room->discount_price,
                'number' => '0',
                'reserve_type_id' => 4,
                'room_id' => $room->id,
            ]);
        }
        dd('ok');
    }

    public function test()
    {
        $day = 0;
        $j = 0;
        $reservationDays = array();
        $passengerArrivalTimeDay = 70;
        $checkStart = $passengerArrivalTimeDay;
        $passengerDepartureTimeDay = 95;
        $checkEnd = $passengerDepartureTimeDay;
        $dayNumber = $passengerDepartureTimeDay - $passengerArrivalTimeDay;
        $room = Room::find('15410')->get();
        $roomTime = Date::where('room_id', 15410)->get();
        $roomTimeDay = $roomTime;
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            $roomTimeDay[$i]->date_booking_start = $this->nthday($roomTimeDay[$i]->date_booking_start);
            $roomTimeDay[$i]->date_booking_end = $this->nthday($roomTimeDay[$i]->date_booking_end);
        }
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            $min[$i] = $roomTimeDay[$i]->date_booking_start;
        }
        if ($passengerArrivalTimeDay < min($min)) {
            $passengerArrivalTimeDay = min($min);
            $checkStart = min($min);
        }
        $testReserve = $passengerArrivalTimeDay;
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            if ($passengerDepartureTimeDay >= $roomTimeDay[$i]->date_booking_end) {
                var_dump($passengerArrivalTimeDay, $roomTimeDay[$i]->date_booking_start, $roomTimeDay[$i]->date_booking_end);
                echo '<br>';
                if ($passengerArrivalTimeDay >= $roomTimeDay[$i]->date_booking_start && $passengerArrivalTimeDay <= $roomTimeDay[$i]->date_booking_end) {
                    for ($day = $passengerArrivalTimeDay; $day <= $roomTimeDay[$i]->date_booking_end; $day++) {
                        $reservationDays[$j] = $roomTimeDay[$i];
                        $j += 1;
                        echo $j;
                        echo '<br>';
                    }
                    $passengerArrivalTimeDay = $day;
                }
            } elseif ($passengerDepartureTimeDay < $roomTimeDay[$i]->date_booking_end)
                if ($passengerArrivalTimeDay >= $roomTimeDay[$i]->date_booking_start && $passengerArrivalTimeDay <= $roomTimeDay[$i]->date_booking_end) {
                    {
                        for ($day = $passengerArrivalTimeDay; $day <= $passengerDepartureTimeDay; $day++) {
                            $reservationDays[$j] = $roomTimeDay[$i];
                            $j += 1;
                        }
                    }
                }
        }
        $parcham = 0;
        for ($i = 0; $i < count($roomTimeDay); $i++) {
            var_dump($roomTimeDay[$i]->reserve_type_id);
            if ($roomTimeDay[$i]->reserve_type_id == 4) {
                if ($checkStart >= $roomTimeDay[$i]->date_booking_start && $checkStart <= $roomTimeDay[$i]->date_booking_end) {
                    $parcham = 1;
                }
                if ($checkEnd >= $roomTimeDay[$i]->date_booking_start && $checkEnd <= $roomTimeDay[$i]->date_booking_end) {
                    $parcham = 1;
                }
                if ($checkStart <= $roomTimeDay[$i]->date_booking_start && $checkEnd >= $roomTimeDay[$i]->date_booking_end) {
                    $parcham = 1;
                }
            }
        }
        $room = Room::where('id', 15410)->get();
        for ($i = count($reservationDays); $i < $dayNumber; $i++) {
            $reservationDays[$i] = $room[0];
            $reservationDays[$i]->number = 1;
        }
        $reservationDays['parcham'] = $parcham;
        dd($reservationDays, $parcham);
    }

    public function testpa()
    {
//        $invoice = (new Invoice)->amount(1000);
//        $invoice->detail(['detailName' => 'your detail goes here']);
//// 2
//        $invoice->detail('detailName', 'your detail goes here');
//// 3
//        $invoice->detail(['name1' => 'detail1', 'name2' => 'detail2']);
//// 4
//        $invoice->detail('detailName1', 'your detail1 goes here')
//            ->detail('detailName2', 'your detail2 goes here');
//        $driver = $invoice->getDriver();
//        $transactionId = 6546546456;
////        return Payment::purchase($invoice, function ($driver, $transactionId) {
////            // Store transactionId in database as we need it to verify payment in the future.
////        })->pay();
        $amount 			= 100; 												// Price Toman
        $merchantCode 		= "";										// Merchant Code
        $terminalCode 		= ""; 										// Terminal Code
        $privateKey 		= ""; 												// Private Key
        $redirectAddress 	= ""; 	// Callback URL

        $amount 			= $amount * 10;
        $invoiceDate 		= date("Y/m/d H:i:s");
        $processor 			= new RSAProcessor($privateKey);

        date_default_timezone_set('Asia/Tehran');
        $timeStamp 			= date("Y/m/d H:i:s");
        $action 			= "1003";

        $InvoiceNumber=423456465;

        $data 				= "#". $merchantCode ."#". $terminalCode ."#". $InvoiceNumber ."#". $invoiceDate ."#". $amount ."#". $redirectAddress ."#". $action ."#". $timeStamp ."#";
        $data 				= sha1($data,true);
        $data 				= $processor->sign($data);
        $result 			= base64_encode($data);

        echo "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
	<body>
		<div id='WaitToSend' style='margin:0 auto; width: 600px; text-align: center;'>درحال انتقال به درگاه بانک<br>لطفا منتظر بمانید .</div>
		<form Id='GateWayForm' Method='post' Action='https://pep.shaparak.ir/gateway.aspx' style='display: none;'>
			InvoiceNumber<input type='text' name='InvoiceNumber' value='$InvoiceNumber' />
			invoiceDate<input type='text' name='invoiceDate' value='$invoiceDate' />
			amount<input type='text' name='amount' value='$amount' />
			terminalCode<input type='text' name='terminalCode' value='$terminalCode' />
			merchantCode<input type='text' name='merchantCode' value='$merchantCode' />
			redirectAddress<input type='text' name='redirectAddress' value='$redirectAddress' />
			timeStamp<input type='text' name='timeStamp' value='$timeStamp' />
			action<input type='text' name='action' value='$action' />
			sign<input type='text' name='sign' value='$result' />
		</form>
		<script language='javascript'>document.forms['GateWayForm'].submit();</script>
	</body>
</html>";
    }
}
