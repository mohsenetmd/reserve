<?php

namespace App\Http\Controllers;

use App\Fard;
use App\Hotel;
use App\RoomType;
use App\SaveReserve;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Illuminate\Support\Facades\Cookie;
use SoapClient;

class FardController extends Controller
{
    public function save()
    {
        $condition = SaveReserve::where('id', request()->save_reserve_id)->get();
        $conditionif = $condition[0]->condition;
        if ($conditionif == 0) {
            SaveReserve::where('id', request()->save_reserve_id)->update([
                'condition' => 1,
                'time' => time()
            ]);
            Fard::create([
                'name' => \request()->name ?? 'none',
                'family' => \request()->family ?? 'none',
                'melli_number' => \request()->melli_number,
                'sex' => \request()->sex,
                'save_reserve_id' => \request()->save_reserve_id
            ]);
        }
        $saveReserve = SaveReserve::find(request()->save_reserve_id);
        $time = $saveReserve->time;
        $totalarray = json_decode($saveReserve->total);
        $roomarray = json_decode($saveReserve->room);
        $hotel = Hotel::find($roomarray->hotel_id);
        $roomtype = RoomType::find($roomarray->room_type_id);
        $totalprice = $totalarray->discount_price + $saveReserve->total_bed_price;
        $tedad = $saveReserve->tedad;
        $date_booking_start = $saveReserve->date_booking_start;
        $date_booking_end = $saveReserve->date_booking_end;
        $name = \request()->name;
        $family = \request()->family;
        $mobile = Auth::user()->mobile;
        $hotelName = $hotel->name;
        $roomName = $roomtype->name;
        $voucher = $saveReserve->voucher;
        $online = $saveReserve->online;
        cookie()->queue(cookie('voucher', $voucher, 60));
        if ($online == 0) {
            if ($conditionif == 0) {
                $send = $family . "\r\n" . 'شماره موبایل :' . $mobile . "\r\n" .
                    'هتل : ' . $hotelName . "\r\n" . 'اتاق : ' . $roomName . "\r\n" . 'تاریخ ورود : ' . $date_booking_start . "\r\n" . 'تاریخ خروج : ' . $date_booking_end . "\r\n" . 'قیمت : ' .
                    $totalprice . "\r\n" . 'کد واچر : ' . $voucher . "\r\n" . 'تعداد اتاق : ' . $tedad;
                Smsirlaravel::send($send, '09158438332');
                Smsirlaravel::send($send, '0907198001');
                Smsirlaravel::send($send, '0907198002');
                Smsirlaravel::send($send, '0907198003');
                Smsirlaravel::send($send, '0907198004');
                Smsirlaravel::send($send, '0907198005');
                Smsirlaravel::send($send, '0907198006');
                Smsirlaravel::send($send, '0907198007');
                Smsirlaravel::send($send, '0907198008');
                Smsirlaravel::send($send, '0907198009');
                Smsirlaravel::send($send, '09152222987');
                Smsirlaravel::send($send, '09035115939');
            }
            return view('voucher.insert', ['voucher' => $voucher, 'time' => $time, 'now' => time(), 'condition' => $conditionif, 'price'=>($totalprice * $tedad)]);
        } else {
            $s = SaveReserve::where('voucher', $voucher)->get();
            $tedad = $s[0]->tedad;
            $total = $s[0]->total;
            $room = $s[0]->room;
            $roomarr = json_decode($room);
            $hotel = Hotel::find($roomarr->hotel_id);
            $roomtype = RoomType::find($roomarr->room_type_id);
            $MerchantID = '53c8bfb4-8216-11e9-80a0-000c29344814'; //Required
            $Amount = $totalprice * $tedad; //Amount will be based on Toman - Required
            $Description = $hotel->name . ': ' . $roomtype->name; // Required
            $Mobile = Auth::user()->mobile; // Optional
            $CallbackURL = 'https://hihotel.org/pardakht/verify?id=' . request()->save_reserve_id; // Required
            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            Smsirlaravel::send($Description, '09158438332');
            $result = $client->PaymentRequest(
                [
                    'MerchantID' => $MerchantID,
                    'Amount' => $Amount,
                    'Description' => $Description,
                    'Mobile' => $Mobile,
                    'CallbackURL' => $CallbackURL,
                ]
            );
            if ($result->Status == 100) {
                $link = 'https://www.zarinpal.com/pg/StartPay/' . $result->Authority;
            } else {
                echo 'ERR: ' . $result->Status;
            }
            return redirect($link);
        }
    }

    public function show()
    {
        $voucher = Cookie::get('voucher');
        if (empty($voucher)) {
            $voucher = \request()->voucher;
        }
        if (!empty($voucher)) {
            $condition = SaveReserve::where('voucher', $voucher)->get();
            $time = $condition[0]->time;
            $conditionif = $condition[0]->condition;
            $online = $condition[0]->online;
            if ($online == 1) {
                $s = SaveReserve::where('voucher', $voucher)->get();
                $total = $s[0]->total;
                $room = $s[0]->room;
                $roomarr = json_decode($room);
                $hotel = Hotel::find($roomarr->hotel_id);
                $roomtype = RoomType::find($roomarr->room_type_id);
                $MerchantID = '53c8bfb4-8216-11e9-80a0-000c29344814'; //Required
                $Amount = 100; //Amount will be based on Toman - Required
                $Description = $hotel->name . ': ' . $roomtype->name; // Required
                $Mobile = Auth::user()->mobile; // Optional
                $CallbackURL = 'http://localhost:8000/pardakht/verify?id=' . $voucher; // Required
                $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
                $result = $client->PaymentRequest(
                    [
                        'MerchantID' => $MerchantID,
                        'Amount' => $Amount,
                        'Description' => $Description,
                        'Mobile' => $Mobile,
                        'CallbackURL' => $CallbackURL,
                    ]
                );
                if ($result->Status == 100) {
                    $link = 'https://www.zarinpal.com/pg/StartPay/' . $result->Authority;
                } else {
                    echo 'ERR: ' . $result->Status;
                }
                return view('pardakht', ['link' => $link]);
            }
            return view('voucher.insert', ['voucher' => $voucher, 'time' => $time, 'now' => time(), 'condition' => $conditionif]);
        } else {
            return view('voucher.insertvoucher');
        }
    }
}
