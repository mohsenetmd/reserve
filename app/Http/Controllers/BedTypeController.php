<?php

namespace App\Http\Controllers;

use App\BedType;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Gate;

class BedTypeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | No Insert Method Because
    | Dont Pass Any Variable To Page bedtype.insert
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | Save BedType
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save()
    {
        BedType::create(['name' => \request()->name,]);
        alert()->success('نوع تخت با موفقیت ذخیره شد.', 'ذخیره نوع تخت');
        return redirect(route('bedtype.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update BedType
    | Pass Variable To Page bedtype.update
    |--------------------------------------------------------------------------
     */
    public function update($id)
    {
            return view('bedtype.update', ['bedtype' => BedType::find($id)]);
    }


    /*
    |--------------------------------------------------------------------------
    | Update BedType
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated()
    {
            BedType::where('id', \request()->id)->update(['name' => \request()->name]);
            return redirect(route('bedtype.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For BedType Table
    | Pass Variable To Page bedtype.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request)
    {
            if ($request->ajax()) {
                $data = BedType::latest()->get();
                return Datatables::of($data)
                    ->addColumn('jalali_create', function ($row) {
                        return jalaliCreat($row);
                    })
                    ->rawColumns(['jalali_create'])
                    ->addColumn('jalali_update', function ($row) {
                        return jalaliUpdate($row);
                    })
                    ->rawColumns(['jalali_update'])
                    ->addIndexColumn()
                    ->addColumn('link_bedtype', function ($row) {
                        $btn = '<a href="/admin/bedtype/update/' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
                        return $btn;
                    })
                    ->rawColumns(['link_bedtype'])
                    ->make(true);
            }
            return view('bedtype.show');
    }
}
