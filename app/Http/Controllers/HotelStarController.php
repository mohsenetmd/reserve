<?php

namespace App\Http\Controllers;

use App\HotelStar;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class HotelStarController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | No Insert Method Because
    | Dont Pass Any Variable To Page hotelstar.insert
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | Save HotelStar
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save()
    {
        HotelStar::create(['name' => \request()->name,]);
        alert()->success('ستاره هتل با موفقیت ذخیره شد.', 'ذخیره ستاره هتل');
        return redirect(route('hotelstar.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update HotelStar
    | Pass Variable To Page hotelstar.update
    |--------------------------------------------------------------------------
     */
    public function update(Request $request)
    {
        return view('hotelstar.update', ['hotelstar' => HotelStar::find($request->id)]);
    }

    /*
    |--------------------------------------------------------------------------
    | Update HoteStar
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated()
    {
        HotelStar::where('id', \request()->id)->update(['name' => \request()->name]);
        return redirect(route('hotelstar.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For HotelStar Table
    | Pass Variable To Page hotelstar.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = HotelStar::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('link_hotelstar', function ($row) {
                    $btn = '<a href="/admin/hotelstar/update?id=' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['link_hotelstar'])
                ->make(true);
        }
        return view('hotelstar.show');
    }
}
