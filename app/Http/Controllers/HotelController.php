<?php

namespace App\Http\Controllers;

use App\City;
use App\Date;
use App\GroupChanges;
use App\Hotel;
use App\HotelStar;
use App\HotelType;
use App\Http\Requests\HotelValidate;
use App\ReserveType;
use App\RoomType;
use App\WpPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;
use Yajra\DataTables\Facades\DataTables;

class HotelController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Create View For Add Hotel
    | Pass Variable To Page hotel.insert
    |--------------------------------------------------------------------------
     */
    public function insert()
    {
        return view('hotel.insert', $this->getVariableHotel());
    }

    /*
    |--------------------------------------------------------------------------
    | Save Hotel
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save(HotelValidate $request)
    {
        Hotel::create($request->except('_token'));
        alert()->success('هتل با موفقیت ذخیره شد.', 'ذخیره هتل');
        return redirect(route('hotel.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update Hotel
    | Pass Variable To Page hotel.update
    |--------------------------------------------------------------------------
     */
    public function update($id)
    {
        $data=$this->getVariableHotel();
        $data['hotel']=Hotel::find($id);
        return view('hotel.update', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | Update Hotel
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated(HotelValidate $request)
    {
        $removeKey = ['_token', 'wp_post_id', '_method'];
        Hotel::where('id', $request->id)->update($request->except($removeKey));
        alert()->success('هتل با موفقیت بروزرسانی شد.', 'بروزرسانی هتل');
        return redirect(route('hotel.show'));
    }


    /*
    |--------------------------------------------------------------------------
    | Delete Hotel
    |--------------------------------------------------------------------------
     */
    public function delete(HotelValidate $request)
    {
        Hotel::where('id', $request->id)->update(['status' => '0']);
        alert()->success('هتل با موفقیت حذف شد.', 'حذف هتل');
        return redirect(route('hotel.show'));
    }


    /*
    |--------------------------------------------------------------------------
    | Create View For Hotel Table
    | Pass Variable To Page hotel.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request)
    {
        /*
        |--------------------------------------------------------------------------
        | Select Data For Filter Hotel
        |--------------------------------------------------------------------------
         */
        $city = City::all();
        $hotelStar = HotelStar::all();
        $hotelType = HotelType::all();
        $keySession = ['city_id', 'hotel_star_id', 'hotel_type_id'];
        $this->SessionForget($request, $keySession);
        $wher = $this->CreateWher($request, $keySession);
        $data = Hotel::where($wher)->get();
        /*
        |--------------------------------------------------------------------------
        | Create Table With Data
        |--------------------------------------------------------------------------
         */
        if ($request->ajax()) {
            return Datatables::of($data)
                ->addColumn('city', function ($row) {
                    return $row->City->name;
                })
                ->rawColumns(['city'])
                ->addColumn('start_time', function ($row) {
                    $group = GroupChanges::where('hotel_id', '["' . $row->id . '"]')->get();
                    if (count($group) > 0) {
                        $i = 0;
                        foreach ($group as $item) {
                            $time[$i] = $item->date_booking_start;
                            $i += 1;
                        }
                        return $time;
                    } else {
                        return 0;
                    }
                })
                ->rawColumns(['start_time'])
                ->addColumn('jalali_update', function ($row) {
                    return jalaliUpdate($row);
                })
                ->rawColumns(['jalali_update'])
                ->addColumn('more', function ($row) {
                    $group = GroupChanges::where('hotel_id', '["' . $row->id . '"]')->get();
                    if (count($group) > 0) {
                        return '+';
                    } else {
                        return '';
                    }
                })
                ->rawColumns(['more'])
                ->addColumn('end_time', function ($row) {
                    $group = GroupChanges::where('hotel_id', '["' . $row->id . '"]')->get();
                    if (count($group) > 0) {
                        $i = 0;
                        foreach ($group as $item) {
                            $time[$i] = $item->date_booking_end;
                            $i += 1;
                        }
                        return $time;
                    } else {
                        return 0;
                    }
                })
                ->rawColumns(['end_time'])
                ->addColumn('id', function ($row) {
                    $group = GroupChanges::where('hotel_id', '["' . $row->id . '"]')->get();
                    if (count($group) > 0) {
                        $i = 0;
                        foreach ($group as $item) {
                            $time[$i] = $item->id;
                            $i += 1;
                        }
                        return $time;
                    } else {
                        return 0;
                    }
                })
                ->rawColumns(['id'])
                ->addColumn('hotelstar', function ($row) {
                    return $row->HotelStar->name;
                })
                ->rawColumns(['hotelstar'])
                ->addColumn('hoteltype', function ($row) {
                    return $row->HotelType->name;
                })
                ->rawColumns(['hoteltype'])
                ->addColumn('link_room', function ($row) {
                    $btn = '<a href="/admin/room?hotel_id=' . $row->id . '" class="edit btn btn-primary btn-sm" style="margin-bottom: 5px;"> مشاهده اتاق ها</a>
                            <a href="/admin/hotel/update/' . $row->id . '" class="edit btn btn-success btn-sm">ویرایش</a>
                            <a href="/group/add/' . $row->id . '" class="edit btn btn-success btn-sm">افزودن تاریخ</a>
                            <a href="/edit/time/hotel/' . $row->id . '" class="edit btn btn-success btn-sm">ویرایش تاریخ</a>';
                    return $btn;
                })
                ->rawColumns(['link_room'])
                ->make(true);
        }
        return view('hotel.show', ['city' => $city, 'hotelStar' => $hotelStar, 'hotelType' => $hotelType]);
    }


    /*
    |--------------------------------------------------------------------------
    | Create View For Edit Time Hotel
    | Pass Variable To Page hotel.time
    |--------------------------------------------------------------------------
     */
    public function editTime($id)
    {
        $hotel = Hotel::find($id);
        $room = $hotel->room;
        $i = 0;
        foreach ($room as $item) {
            $roomview[$i] = RoomType::find($item->room_type_id);
            $rooms[$i] = $item;
            $date[$i] = Date::where(['room_id' => $item->id, 'status' => 1])->get();
            $i += 1;
        }
        $reserve_type = ReserveType::all();
        return view('hotel.time', ['room' => $roomview, 'date' => $date, 'rooms' => $rooms, 'reserve_type' => $reserve_type]);
    }

    /*
    |--------------------------------------------------------------------------
    | Update Hotel Time
    |--------------------------------------------------------------------------
     */
    public function savetime()
    {
        $idArray = \request()->id;
        $delArray = \request()->del;
        $price = \request()->price;
        $hotel_price = \request()->hotel_price;
        $discount_price = \request()->discount_price;
        $date_booking_start = \request()->start;
        $date_booking_end = \request()->end;
        $reserve_type_id = \request()->reserve_type_id;
        $i = 0;
        foreach ($idArray as $item) {
            $date = Date::find($item)->update([
                'admin_updated' => Auth::user()->id,
                'price' => $price[$i],
                'hotel_price' => $hotel_price[$i],
                'discount_price' => $discount_price[$i],
                'date_booking_start' => $date_booking_start[$i],
                'date_booking_end' => $date_booking_end[$i],
                'reserve_type_id' => $reserve_type_id[$i]
            ]);
            $i += 1;
        }
        if ($delArray != null) {
            foreach ($delArray as $item) {
                Date::find($item)->update([
                    'status' => 0
                ]);
            }
        }
        return redirect(route('hotel.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Other Function
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | Select Variable For hotel.insert And hotel.update
    |--------------------------------------------------------------------------
     */
    public function getVariableHotel(): array
    {
        return ['city' => City::get(), 'hotelstar' => HotelStar::get(), 'hoteltype' => HotelType::get()];
    }

    /*
    |--------------------------------------------------------------------------
    | Create Post for Wordpress
    |--------------------------------------------------------------------------
     */
    public static function CreateWpPost()
    {
        $IDWpPost = WpPost::create([
            'post_author' => '5',
            'post_content' => '0',
            'post_excerpt' => '',
            'to_ping' => '',
            'pinged' => '',
            'post_content_filtered' => '',
            'post_date' => date("Y-m-d h:i:s"),
            'post_date_gmt' => date("Y-m-d h:i:s"),
            'post_title' => \request()->name,
            'post_status' => 'draft',
            'comment_status' => 'open',
            'ping_status' => 'open',
            'post_modified' => date("Y-m-d h:i:s"),
            'post_parent' => 0,
            'post_modified_gmt' => date("Y-m-d h:i:s"),
            'post_type' => 'post'
        ]);
        return $IDWpPost;
    }


    /*
    |--------------------------------------------------------------------------
    | Function For Filter Hotel
    |--------------------------------------------------------------------------
     */
    public function CreateWher(Request $request,$keySession): array
    {
        $wher = array();
        foreach ($keySession as $item) {
            if ($request->$item != 0) {
                session([$item => $request->$item]);
            }
            if (!is_null(session($item))) {
                $wher[$item] = session($item);
            }
        }
        $wher['status'] = 1;
        return $wher;
    }

    /*
    |--------------------------------------------------------------------------
    | Function For Reset Filter Hotel
    |--------------------------------------------------------------------------
     */
    public function SessionForget(Request $request, array $keySession): void
    {
        if ($request->destroy == 1) {
            foreach ($keySession as $item) {
                session()->forget($item);
            }
        }
    }
}
