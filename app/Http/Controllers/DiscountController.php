<?php

namespace App\Http\Controllers;

use App\Discount;
use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DiscountController extends Controller
{
    public function insert(){
        $hotel=Hotel::all();
        return view('discount.insert',['hotel'=>$hotel]);
    }

    public function save(){
        $validate=\request()->validate([
            'hotel_id'=>'required',
            'discount_code'=>'required|min:4|max:10',
            'number_use'=>'required',
            'price'=>'required',
            'percent'=>'required',
            'date_booking_start'=>'required',
            'date_booking_end'=>'required'
            ],[
                'hotel_id.required'=>'هتل های مورد استفاده را وارد کنید.',
                'discount_code.min'=>'تعداد کارکترها کمتر از 4 است',
                'discount_code.max'=>'تعداد کارکترها بیش از 10 است',
                'discount_code.require'=>'وارد کردن کد تخفیف الزامی است.',
                'number_use.required'=>'تعداد موارد استفاده را وارد کنید',
                'price.required'=>'مبلغ تخفیف را وارد کنید.',
                'percent.required'=>'درصد تخفیف را وارد کنید.',
                'date_booking_start.required'=>'تاریخ شروع را وارد کنید.',
                'date_booking_end.required'=>'تاریخ پایان را وارد کنید.',
        ]);
        Discount::create([
            'hotel_id'=>json_encode($validate['hotel_id']),
            'discount_code'=>$validate['discount_code'],
            'number_use'=>$validate['number_use'],
            'price'=>$validate['price'],
            'percent'=>$validate['percent'],
            'date_booking_start'=>$validate['date_booking_start'],
            'date_booking_end'=>$validate['date_booking_end']
        ]);
    }
}
