<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReserveType;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Gate;

class ReserveTypeController extends Controller
{
    public function update(Request $request)
    {
		if (Gate::allows('Admin')) {
        return view('reservetype.update', ['reservetype' => ReserveType::find($request->id)]);
		        } else {
            abort(404);
        }
    }


    public function save()
    {
		if (Gate::allows('Admin')) {
        ReserveType::create([
            'name' => \request()->name,
        ]);
        alert()->success('وضعیت رزرو اتاق با موفقیت ذخیره شد.', 'ذخیره وضعیت رزرو اتاق');
        return redirect(route('reservetype.show'));
		        } else {
            abort(404);
        }
    }

    public function updated()
    {
		if (Gate::allows('Admin')) {
        ReserveType::where('id', \request()->id)->update([
            'name' => \request()->name,
        ]);
        return redirect(route('reservetype.show'));
		        } else {
            abort(404);
        }
    }

    public function show(Request $request)
    {
		if (Gate::allows('Admin')) {
        if ($request->ajax()) {
            $data = ReserveType::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('link_reservetype', function ($row) {
                    $btn = '<a href="/admin/reservetype/update?id=' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['link_reservetype'])
                ->make(true);
        }
        return view('reservetype.show');
		        } else {
            abort(404);
        }
    }
}
