<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function show(){
        $user=Auth::user();
        return view('user.show',['user'=>$user]);
    }

    public function update(){
        $user=Auth::user();
        return view('user.update',['user'=>$user]);
    }

    public function change(){
        return view('user.change');
    }

    public function updated(){
        $user=Auth::user();
        User::where('id', $user->id)->update([
            'name'=>\request()->name,
            'email'=>$user->email,
            'admin_level'=>$user->admin_level,
            'family'=>\request()->family,
            'mobile'=>\request()->mobile,
            'password' => $user->password
        ]);
       return redirect(route('user.show'));
    }

    public function changePassword(){
        $user=Auth::user();
        User::where('id', $user->id)->update([
            'name'=>$user->name,
            'email'=>$user->email,
            'admin_level'=>$user->admin_level,
            'family'=>$user->family,
            'mobile'=>$user->mobile,
            'password' => Hash::make(\request()->password),
        ]);
    }


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('users');
    }
}
