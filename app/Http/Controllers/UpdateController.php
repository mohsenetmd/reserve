<?php

namespace App\Http\Controllers;

use App\BedType;
use App\City;
use App\Hotel;
use App\HotelStar;
use App\HotelType;
use App\Room;
use App\RoomType;
use App\WpPost;
use App\WpPostmeta;
use Illuminate\Http\Request;

/**
 * Class UpdateController
 * @package App\Http\Controllers
 */
class UpdateController extends Controller
{
    /**
     *
     */
    public function RoomUpdate()
    {
        $this->TypeInsert("App\RoomType", ['room_type']);
        $this->TypeInsert("App\BedType", ['bed_type']);
        $data_room = $this->TypeFromWpPostmeta([
                'bed_number',
                'bed_type',
                'breakfast',
                'dinner',
                'discounted_prices',
                'lunch',
                'more_bed',
                'order',
                'pic',
                'price',
                'room_type']
            , [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        $breakfast = $data_room['breakfast'];
        $dinner = $data_room['dinner'];
        $discounted_prices = $data_room['discounted_prices'];
        $lunch = $data_room['lunch'];
        $pic = $data_room['pic'];
        $price = $data_room['price'];
        for($i=0;$i<count($price);$i++){
            $string=str_replace(',','',$price[$i]);
            $string=str_replace('.','',$string);
            $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

            $num = range(0, 9);
            $convertedPersianNums = str_replace($persian, $num, $string);
            $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);
            $price[$i]=$englishNumbersOnly;
        }
        for($i=0;$i<count($discounted_prices);$i++){
            $string=str_replace(',','',$discounted_prices[$i]);
            $string=str_replace('.','',$string);
            $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

            $num = range(0, 9);
            $convertedPersianNums = str_replace($persian, $num, $string);
            $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);
            $discounted_prices[$i]=$englishNumbersOnly;
        }
        $order = $data_room['order'];
        $bed_number = $data_room['bed_number'];

        $data_room_id = WpPostmeta::where('meta_key', 'like', '%otagh%')->select('post_id', 'meta_id')->get();
        $i = 0;
        foreach ($data_room_id as $item) {
            $hotel_id[$i] = Hotel::where('wp_post_id', $item->post_id)->select('id')->get();
            $i += 1;
        }
        $i = 0;
        $room_type = $data_room['room_type'];
        foreach ($room_type as $item) {
            $room_type_id[$i] = RoomType::where('name', $item)->get();
            $i += 1;
        }
        $i = 0;
        $bed_type = $data_room['bed_type'];
        foreach ($bed_type as $item) {
            $bed_type_id[$i] = BedType::where('name', $item)->get();
            $i += 1;
        }
        for ($i=0;$i<count($hotel_id);$i++) {
            Room::create([
                    'admin_created' => '1',
                    'admin_updated' => '1',
                    'bed_number' => '0' ,
                    'bed_price' => '0',
                    'bed_type_id' => $bed_type_id[$i][0]->id,
                    'breakfast' => $breakfast[$i],
                    'breakfast_price' => '0',
                    'child' => '0',
                    'child_number' => '0',
                    'child_price' => '0',
                    'dinner' => $dinner[$i],
                    'dinner_price' => '0',
                    'discount_price' => $discounted_prices[$i],
                    'food_type_id' => '1',
                    'hotel_id' => count($hotel_id[$i]) ? $hotel_id[$i][0]->id:2,
                    'lunch' => $lunch[$i],
                    'lunch_price' => '0',
                    'more_bed' => '0',
                    'person_number' => $bed_number[$i] || 1,
                    'pic' => $pic[$i],
                    'price' => $price[$i],
                    'reserve_type_id' => '1',
                    'room_number' => '0',
                    'room_type_id' => $room_type_id[$i][0]->id,
                    'sample_data' => '1',
                    'sort' => $order[$i],
                    'wp_postmeta'=>$data_room_id[$i]->meta_id
                ]
            );
        }
    }

    public function HotelUpdate()
    {
        $data = $this->DataFromWpPost(['ID', 'post_title']);
        $ID = $data['ID'];
        $PostTitle = $data['post_title'];
        for ($i = 0; $i < count($ID); $i++) {
            $star[$i] = WpPostmeta::where('meta_key', 'star_hotel')->where('post_id', $ID[$i])->get();
            if (!empty($star[$i][0])) {
                $star_type[$i] = $star[$i][0]->meta_value;
            } else {
                $star_type[$i] = 1;
            }
            $city[$i] = WpPostmeta::where('meta_key', 'city_name')->where('post_id', $ID[$i])->get();
            if (!empty($city[$i][0])) {
                $city_type[$i] = $city[$i][0]->meta_value;
            } else {
                $city_type[$i] = 'مشهد';
            }
            $local[$i] = WpPostmeta::where('meta_key', 'local_name')->where('post_id', $ID[$i])->get();
            if (!empty($local[$i][0])) {
                $local_type[$i] = $local[$i][0]->meta_value;
            } else {
                $local_type[$i] = 'هتل';
            }
            $start_price[$i] = WpPostmeta::where('meta_key', 'start_price')->where('post_id', $ID[$i])->select('meta_value')->get();
            if (isset($start_price[$i][0]->meta_value)) {
                $start_price2[$i] = $start_price[$i][0]->meta_value;
            } else {
                $start_price2[$i] = '00000000000000000';
            }
        }
        $star_type_uniq = array_unique($star_type);
        $city_type_uniq = array_unique($city_type);
        $local_type_uniq = array_unique($local_type);
        $star_db = $this->TypeFromDB('App\HotelStar');
        $city_db = $this->TypeFromDB('App\City');
        $local_db = $this->TypeFromDB('App\HotelType');
        $star_insert = array_diff($star_type_uniq, $star_db);
        $city_insert = array_diff($city_type_uniq, $city_db);
        $local_insert = array_diff($local_type_uniq, $local_db);
        foreach ($star_insert as $item) {
            HotelStar::create(
                [
                    'name' => $item
                ]
            );
        }
        foreach ($city_insert as $item) {
            City::create(
                [
                    'name' => $item
                ]
            );
        }
        foreach ($local_insert as $item) {
            HotelType::create(
                [
                    'name' => $item
                ]
            );
        }
        for ($i = 0; $i < count($star_type); $i++) {
            $hotel_star_id[$i] = HotelStar::where('name', $star_type[$i])->get();
        }
        for ($i = 0; $i < count($local_type); $i++) {
            $hotel_type_id[$i] = HotelType::where('name', $local_type[$i])->get();
        }
        for ($i = 0; $i < count($city_type); $i++) {
            $city_type_id[$i] = City::where('name', $city_type[$i])->get();
        }
        for ($i = 0; $i < count($ID); $i++) {
            Hotel::create([
                'admin_created' => 1,
                'admin_updated' => 1,
                'city_id' => $city_type_id[$i][0]->id,
                'hotel_star_id' => $hotel_star_id[$i][0]->id,
                'hotel_type_id' => $hotel_type_id[$i][0]->id,
                'name' => $PostTitle[$i],
                'start_price' => $start_price2[$i],
                'status' => 1,
                'wp_post_id' => $ID[$i]
            ]);
        }
    }

    /**
     * @return WpPostmeta
     */
    public function DataFromWpPostmeta($search)
    {
        $data = new WpPostmeta();
        $data = $data::where('meta_key', 'like', $search)->select('meta_value')->get();
        return $data;
    }

    public function DataFromWpPost(array $select)
    {
        $data = new WpPost();
        $data = $data::where('post_type', 'post')->where('post_status', 'NOT LIKE', '%draft%')->get();
        for ($i = 0; $i < count($data); $i++) {
            foreach ($select as $index) {
                $item[$index][$i] = $data[$i]->$index;
            }
        }
        return $item;
    }

    /**
     * @param $inputTypeFromDB
     * @return mixed
     */
    public function TypeFromDB($inputTypeFromDB)
    {
        $item = $inputTypeFromDB::select('name')->get();
        $output = [];
        for ($i = 0; $i < count($item); $i++) {
            $output[$i] = $item[$i]['name'];
        }
        return $output;
    }

    /**
     * @param $inputTypeFromWpPostmeta
     * @return array
     */
    public function TypeFromWpPostmeta(array $inputTypeFromWpPostmeta, array $uniq)
    {
        $data = $this->DataFromWpPostmeta('%otagh%');
        for ($i = 0; $i < count($data); $i++) {
            $jsonData[$i] = json_decode($data[$i]['meta_value']);
            foreach ($inputTypeFromWpPostmeta as $item) {
                $type[$item][$i] = $jsonData[$i]->$item;
            }
        }
        for ($i = 0; $i < count($inputTypeFromWpPostmeta); $i++) {
            if ($uniq[$i] == 1) {
                $output[$inputTypeFromWpPostmeta[$i]] = array_unique($type[$item]);
            } else {
                $output[$inputTypeFromWpPostmeta[$i]] = $type[$inputTypeFromWpPostmeta[$i]];
            }
        }
        return $output;
    }

    /**
     * @param $inputTypeFromDB
     * @param $inputTypeFromWpPostmeta
     * @return array
     */
    public function TypeDiff($inputTypeFromDB, $inputTypeFromWpPostmeta)
    {
        $typeFromWpPostmeta = $this->TypeFromWpPostmeta($inputTypeFromWpPostmeta, [1]);
        $typeFromDB = $this->TypeFromDB($inputTypeFromDB);
        $output = array_diff($typeFromWpPostmeta[$inputTypeFromWpPostmeta[0]], $typeFromDB);
        return $output;
    }

	public function numberBed(){
        $room=Room::all();
        foreach ($room as $item){
            $test=WpPostmeta::where('meta_id',$item->wp_postmeta)->get();
            if(count($test) == 1){
                $jtest=json_decode($test[0]->meta_value);
                $bed_number=$jtest->bed_number;
                if(strlen($bed_number) == 0){
                    $bed_number=1;
                }
                Room::where('wp_postmeta',$item->wp_postmeta)->update([
                    'person_number'=>$bed_number
                ]);
            }
        }
    }

    /**
     * @param $inputTypeFromDB
     * @param $inputTypeFromWpPostmeta
     */
    public function TypeInsert($inputTypeFromDB, $inputTypeFromWpPostmeta)
    {
        $type_diff = $this->TypeDiff($inputTypeFromDB, $inputTypeFromWpPostmeta);
        foreach ($type_diff as $item) {
            $inputTypeFromDB::create(
                [
                    'name' => $item
                ]
            );
        }
    }

}
