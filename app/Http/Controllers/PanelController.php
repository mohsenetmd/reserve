<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PanelController extends Controller
{

    public function update(Request $request){
        $user=User::find($request->id);
        return view('panel.update',['user'=>$user]);
    }

    public function updated(Request $request){
        $user=User::find($request->id);
        dd(\request()->name);
        User::where('id', $user->id)->update([
            'name'=>\request()->name,
            'email'=>$user->email,
            'admin_level'=>\request()->admin_level,
            'family'=>\request()->family,
            'mobile'=>\request()->mobile,
            'password' => $user->password
        ]);
        return redirect(route('panel.show'));
    }


    public function show(Request $request){
        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addColumn('admin_level_show', function ($row) {
                    switch ($row->admin_level) {
                        case 0:
                            return 'کاربر عادی';
                            break;
                        case 1:
                            return 'ادمین هتل';
                            break;
                        case 2:
                            return 'ادمین فروش';
                            break;
                        case 3:
                            return 'ادمین سرپرست';
                            break;
                    }
                })
                ->rawColumns(['admin_level_show'])
                ->addColumn('link_panel', function ($row) {
                    $btn = '<a href="/panel/update?id=' . $row->id . '" class="edit btn btn-primary btn-sm" style="margin-bottom: 5px;"> تغییر سطح دسترسی</a>';
                    return $btn;
                })
                ->rawColumns(['link_panel'])
                ->make(true);
        }
        return view('panel.show');
    }
}
