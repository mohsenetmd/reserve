<?php

namespace App\Http\Controllers;

use App\Date;
use App\Fard;
use App\User;
use App\Hotel;
use App\Room;
use App\RoomType;
use App\HotelStar;
use App\BedType;
use App\SaveReserve;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Yajra\DataTables\Facades\DataTables;
use SoapClient;
use Illuminate\Support\Facades\Gate;

class SaveReserveController extends Controller
{

    public function save()
    {
        $online = $this->checkOnline($this->getTotalAll());
        $voucher=(int)microtime(true);
        $data = SaveReserve::create($this->getArrCreateSaveReserve($online, $voucher));
        return view('fard.insert', ['tedad' => \request()->tedad, 'save_reserve_id' => $data->id]);
    }

    public function upsavereserve(){
        $data = SaveReserve::where('id',request()->id)->get();
        $saveReserve = SaveReserve::find(request()->id);
        $totalarray = json_decode($saveReserve->total);
        $roomarray = json_decode($saveReserve->room);
        $hotel = Hotel::find($roomarray->hotel_id);
        $roomtype = RoomType::find($roomarray->room_type_id);
        $totalprice = $totalarray->discount_price + $saveReserve->total_bed_price;
        $hotelName = $hotel->name;
        $roomName = $roomtype->name;
        $update=Auth::user()->id;
        return view('savereserve.updatereserve',['data'=>$data,'update'=>$update,'hotel'=>$hotelName,'room'=>$roomName,'totalprice'=>$totalprice]);
    }

    public function updatesave(){
        $data=SaveReserve::find(\request()->id)->update([
            'condition'=>\request()->condition,
            'admin_updated'=>\request()->admin_updated,
            'time'=>time()
        ]);
        $saveReserve=SaveReserve::find(\request()->id);
        $voucher=$saveReserve->voucher;
        $user=User::find('2');
        $mobile=$user->mobile;
        if(\request()->condition == 3) {
            $send = "رزرو شما با کد رزرواسیون : " . $voucher . " مورد تایید قرار گرفت" ;
                Smsirlaravel::send($send, $mobile);
        }
        return redirect(route('savereserve.show'));

    }

    public function vouchershow()
    {
        $reserve = SaveReserve::where('voucher', \request()->voucher)->get();
        return view('voucher.show', ['reservre' => $reserve]);
    }

    public function pardakht()
    {
        $s = SaveReserve::where('voucher', request()->voucher)->get();
		$tedad=$s[0]->tedad;
        $total = $s[0]->total;
        $totalarr = json_decode($total);
        $room = $s[0]->room;
        $roomarr = json_decode($room);
        $hotel = Hotel::find($roomarr->hotel_id);
        $roomtype = RoomType::find($roomarr->room_type_id);
        $MerchantID = '53c8bfb4-8216-11e9-80a0-000c29344814'; //Required
        $Amount = ($totalarr->discount_price) * $tedad; //Amount will be based on Toman - Required
        //$Amount = 100; //Amount will be based on Toman - Required
        $Description = $hotel->name . ': ' . $roomtype->name; // Required
        $Mobile = Auth::user()->mobile; // Optional
        $CallbackURL = 'https://hihotel.org/pardakht/verify?id=' . $s[0]->id; // Required
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );
        if ($result->Status == 100) {
            $link = 'https://www.zarinpal.com/pg/StartPay/' . $result->Authority;
        } else {
            echo 'ERR: ' . $result->Status;
        }
        return redirect($link);
    }

    public function complete()
    {
        $savereserve=SaveReserve::where('voucher', request()->id)->get();
        $room = $savereserve[0]->room;
        $room = json_decode($room);
        $total = $savereserve[0]->total;
        $total = json_decode($total);
        Date::create([
            'admin_created' => 'customer',
            'admin_updated' => 'customer',
            'bed_price' => $room->bed_price,
            'child_price' => $room->child_price,
            'date_booking_end' => $savereserve[0]->date_booking_end,
            'date_booking_start' => $savereserve[0]->date_booking_start,
            'discount_price' => $room->discount_price,
            'number' => '0',
            'reserve_type_id' => 4,
            'room_id' => $room->id,
        ]);
        dd($savereserve, \request()->id, $room, $total);
    }

    public function pardakhtVerify()
    {
		$save=SaveReserve::where('id', request()->id)->get();
		$fard=Fard::where('save_reserve_id', $save[0]-> id)->get();
		$roomarray=json_decode($save[0]->room);
		$totalarray=json_decode($save[0]->total);
		$pic=$roomarray->pic;
		$hotel_id=$roomarray->hotel_id;
		$room_type_id=$roomarray->room_type_id;
		$bed_type_id=$roomarray->bed_type_id;
		$person_nyumber=$roomarray->person_number;
		$breakfast=$roomarray->breakfast;
		$dinner=$roomarray->dinner;
		$lunch=$roomarray->lunch;
		$hotel=Hotel::where('id',$hotel_id)->get();
		$hotel_name=$hotel[0]->name;
		$hotel_star_id=$hotel[0]->hotel_star_id;
		$room_type=RoomType::where('id',$room_type_id)->get();
		$room_type_name=$room_type[0]->name;
		$bed_type=BedType::where('id',$bed_type_id)->get();
		$bed_type_name=$bed_type[0]->name;
		$hotel_star=HotelStar::where('id',$hotel_star_id)->get();
		$hotel_star_name=$hotel_star[0]->name;
		$hotel_star_view=['0'=>'','1'=>'یک ستاره','2'=>'دو ستاره','3'=>'سه ستاره','4'=>'چهار ستاره','5'=>'پنج ستاره'];
        $MerchantID = '53c8bfb4-8216-11e9-80a0-000c29344814';
        $s = SaveReserve::where('id', request()->id)->get();
        $total = $s[0]->total;
		$tedad = $s[0]->tedad;
        $totalarr = json_decode($total);
        $Amount = ($totalarr->discount_price) * $tedad; //Amount will be based on Toman
        $Authority = $_GET['Authority'];
        if ($_GET['Status'] == 'OK') {
            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ]
            );
            if ($result->Status == 100) {
                echo 'Transaction success. RefID:' . $result->RefID;
                $s = SaveReserve::where('id', request()->id)->update([
                    'condition'=>4,
                    'ref_id'=>$result->RefID
                ]);
                $checkOnline=SaveReserve::where('voucher', request()->id)->get();
				return view('factor', ['save_reserve_id' => request()->id, 'room_type_name'=>$room_type_name
							,'bed_type_name'=>$bed_type_name,'hotel_name'=>$hotel_name , 'voucher'=>$save[0]->voucher , 'roomarray'=>$roomarray ,
							'hotel_star_name'=>$hotel_star_view[$hotel_star_name] ,'start'=>$save[0]->date_booking_start,
							'end'=>$save[0]->date_booking_end,'total'=>$totalarray , 'pic'=>$pic , 'fard'=>$fard]);
            } else {
                echo 'Transaction failed. Status:' . $result->Status;
            }
        } else {
            echo 'Transaction canceled by user';
        }
    }

    public function show(Request $request)
    {
		if (Gate::allows('Admin')) {
        if ($request->ajax()) {
            $data = SaveReserve::where('condition','>',0)->get();
            return Datatables::of($data)
                ->addColumn('user', function ($row) {
                    $user=User::find($row->admin_created)->mobile;
                    return $user;
                })
                ->rawColumns(['user'])
                ->addColumn('condition', function ($row) {
					if($row->online == 1 && $row->condition != 4){
						$text='رزرو آنلاین، منتظر پرداخت';
						return $text;
					}
                    if($row->condition == 0){
                        $text= 'عدم ورود اطلاعات شخصی';
                    }
                    if($row->condition == 1){
                        $text= 'نیازمند استعلام';
                    }
                    if($row->condition == 2){
                        $text= 'پاسخ منفی به استعلام';
                    }
                    if($row->condition == 3){
                        $text= 'جواب مثبت به استعلام و منتظر پرداخت';
                    }
                    if($row->condition == 4){
                        $text= 'پرداخت شده';
                    }
                    return $text;
                })
                ->rawColumns(['user'])
                ->addColumn('fard', function ($row) {
                    if($row->condition > 0){
                    $fard=Fard::where('save_reserve_id',$row->id)->get();
                    return $fard[0]->family;}else{
                        return "none";
                    }
                })
                ->rawColumns(['fard'])
				->addColumn('houre', function ($row) {
                    $houre=$row->created_at->format('H:i:s');
                    return $houre;
                })
                ->rawColumns(['houre'])
				->addColumn('houre_update', function ($row) {
                    $houre=$row->updated_at->format('H:i:s');
                    return $houre;
                })
                ->rawColumns(['houre_update'])
				->addColumn('jalali', function ($row) {
				return jalaliCreat($row);
                })
                ->rawColumns(['jalali'])
				->addColumn('hotel', function ($row) {
                    $json=json_decode($row->room);
					$hotel=Hotel::find($json->hotel_id);
                    return $hotel->name;
                })
				->rawColumns(['hotel'])
				->addColumn('admin', function ($row) {
                    $user_id=$row->admin_updated;
					$user=User::find($user_id);
					if($user->name == 'empty'){
						return 'تا الان به استعلام پاسخی داده نشده است.';
					}
                    return $user->name;
                })
				->rawColumns(['admin'])
				->addColumn('room', function ($row) {
                    $json=json_decode($row->room);
					$room=Room::find($json->id);
					$roomName=RoomType::find($room->room_type_id);
                    return $roomName->name;
                })
                ->rawColumns(['room'])
				->addColumn('edit', function ($row) {
                    $now=time();
                    $time=$row->time;
                    $id=$row->id;
                    $btn='<a  href="https://hihotel.org/admin/savereserve/update?id=' . $row->id . '" class="btn btn-primary">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['edit'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('reserve.showtable');
		        } else {
            abort(404);
        }
    }

    /**
     * @return array
     */
    public function getTotalAll(): array
    {
        $total = json_decode(\request()->total);
        $totalAll = $total[0]->all;
        $totalAll = ((array)$totalAll);
        array_pop($totalAll);
        return $totalAll;
    }

    /**
     * @param array $totalAll
     * @return int
     */
    public function checkOnline(array $totalAll): int
    {
        $online = 1;
        foreach ($totalAll as $item) {
            if ($item->reserve_type_id == 1) {
                $online = 0;
            }
        }
        return $online;
    }

    /**
     * @param int $online
     * @param int $voucher
     * @return array
     */
    public function getArrCreateSaveReserve(int $online, int $voucher): array
    {
        return [
            'admin_created' => Auth::user()->id,
            'admin_updated' => Auth::user()->id,
            'condition' => 0,
            'online' => $online,
            'date_booking_end' => \request()->date_booking_end,
            'date_booking_start' => \request()->date_booking_start,
            'room' => \request()->room,
            'status' => 1,
            'tedad' => \request()->tedad,
            'total' => \request()->total,
            'total_bed_price' => \request()->total_bed_price,
            'voucher' => $voucher,
            'time' => time()
        ];
    }
}
