<?php

namespace App\Http\Controllers;

use App\Date;
use App\FoodType;
use App\Http\Requests\DateValidate;
use App\ReserveType;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class DateController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Create View For Add Date
    | Pass Variable To Page date.insert
    |--------------------------------------------------------------------------
     */
    public function insert(){
        return view('date.insert', $this->getVariableDate());
    }

    /*
    |--------------------------------------------------------------------------
    | Save Room
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save(DateValidate $request){
        Date::create($request);
        return redirect(route('date.show',['room_id'=>\request()->room_id]));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update Date
    | Pass Variable To Page date.update
    |--------------------------------------------------------------------------
     */
    public function update(){
        $data=$this->getVariableDate();
        $data['date']=Date::find(\request()->date_id);
        return view('date.update',$data);
    }

    /*
    |--------------------------------------------------------------------------
    | Update Room
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated(DateValidate $request){
        Date::where('id',\request()->date_id)->update($request);
        return redirect(route('date.show',['room_id'=>$request->room_id]));
    }

    /*
    |--------------------------------------------------------------------------
    | Delete Date
    |--------------------------------------------------------------------------
     */
    public function delete(){
        Date::where('id',\request()->date_id)->update([
            'status'=>'0',
        ]);
        alert()->success('تاریخ با موفقیت حذف شد.', 'حذف تاریخ');
        return redirect(route('date.show',['room_id'=>\request()->room_id]));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Date Table
    | Pass Variable To Page date.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request){
        $hotel = Room::find($request->room_id)->Hotel;
        if ($request->ajax()) {
            $data = Room::find($request->room_id)->Date->where('status', '1');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="/admin/date/update?date_id='.$row->id.'&room_id='.$row->Room->id.'" class="edit btn btn-success btn-sm">ویرایش</a>
                            <a href="/admin/date/delete?date_id='.$row->id.'&room_id='.$row->Room->id.'" class="edit btn btn-danger btn-sm">حذف</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('date.show',['hotel_id'=>$hotel->id]);
    }

    /*
    |--------------------------------------------------------------------------
    | Select Variable For date.insert And date.update
    |--------------------------------------------------------------------------
     */
    public function getVariableDate(): array
    {
        return ['reservetype' => ReserveType::get(), 'foodtype' => FoodType::get()];
    }

}
