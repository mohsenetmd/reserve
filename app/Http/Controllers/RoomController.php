<?php

namespace App\Http\Controllers;

use App\Date;
use App\Hotel;
use App\BedType;
use App\FoodType;
use App\Http\Requests\RoomValidate;
use App\ReserveType;
use App\RoomType;
use App\Room;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class RoomController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Create View For Add Room
    | Pass Variable To Page room.insert
    |--------------------------------------------------------------------------
     */
    public function insert()
    {
        return view('room.insert', $this->getVariableRoom());
    }


    /*
    |--------------------------------------------------------------------------
    | Save Room
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save(RoomValidate $request)
    {
        $namesave = $this->SaveImage($request);
        $data=$request->except('_token');
        $data['pic']=$namesave;
        Room::create($data);
        return redirect(route('room.show', ['hotel_id' => \request()->hotel_id]));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update Room
    | Pass Variable To Page room.update
    |--------------------------------------------------------------------------
     */
    public function update()
    {
        $data=$this->getVariableRoom();
        $data['room']=Room::find(\request()->room_id);
        return view('room.update', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | Update Room
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated(RoomValidate $request)
    {
        $room=Room::find(\request()->id);
        $namesave=$room->pic;
        if ($request->hasFile('pic')) {
           $namesave=$this->SaveImage();
        }
        $data=$request->except('_token','_method');
        $data['pic']=$namesave;
        Room::where('id', \request()->id)->update($data);
        return redirect(route('room.show', ['hotel_id' => \request()->hotel_id]));
    }

    /*
    |--------------------------------------------------------------------------
    | Delete Room
    |--------------------------------------------------------------------------
     */
    public function delete()
    {
        Room::where('room_id', \request()->id)->update([
            'status' => '0',
        ]);
        alert()->success('اتاق با موفقیت حذف شد.', 'حذف اتاق');
        return redirect(route('room.show', ['hotel_id', \request()->hotel_id]));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Room Table
    | Pass Variable To Page room.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = Hotel::find($request->hotel_id)->Room->where('status', '1');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('room_type', function ($row) {
                    $btn = $row->RoomType->name;
                    return $btn;
                })
                ->rawColumns(['room_type'])
                ->addColumn('action', function ($row) {
                    $btn = '<input type="checkbox" value="' . $row->id . '" name="copy[]" >
                    <a href="/admin/date?room_id=' . $row->id . '" class="edit btn btn-primary btn-sm" style="margin-bottom: 5px;">  مشاهده تاریخ ها</a>
                            <a href="/admin/room/update?room_id=' . $row->id . '&hotel_id=' . $row->Hotel->id . '" class="edit btn btn-success btn-sm">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('room.show');
    }

    /*
    |--------------------------------------------------------------------------
    | Copy And Delete Room
    |--------------------------------------------------------------------------
     */
    public function copy()
    {
        $copy = \request()->copy;
        for ($i = 0; $i < count($copy); $i++) {
            $room[$i] = Room::find($copy[$i]);
            $hotel_id=$room[$i]->hotel_id;
            if(\request()->cd == 1){
                Room::create([
                    'admin_created' => $room[$i]->admin_created,
                    'admin_updated' => $room[$i]->admin_updated,
                    'bed_number' => $room[$i]->bed_number,
                    'bed_price' => $room[$i]->bed_price,
                    'bed_type_id' => $room[$i]->bed_type_id,
                    'breakfast' => $room[$i]->breakfast,
                    'dinner' => $room[$i]->dinner,
                    'lunch' => $room[$i]->lunch,
                    'breakfast_price' => $room[$i]->breakfast_price,
                    'child' => $room[$i]->child,
                    'child_number' => $room[$i]->child_number,
                    'child_price' => $room[$i]->child_price,
                    'dinner_price' => $room[$i]->dinner_price,
                    'discount_price' => $room[$i]->discount_price,
                    'food_type_id' => $room[$i]->food_type_id,
                    'hotel_id' => $room[$i]->hotel_id,
                    'lunch_price' => $room[$i]->lunch_price,
                    'more_bed' => $room[$i]->more_bed,
                    'person_number' => $room[$i]->person_number,
                    'price' => $room[$i]->price,
                    'pic' => $room[$i]->pic,
                    'reserve_type_id' => $room[$i]->reserve_type_id,
                    'room_number' => $room[$i]->room_number + 1,
                    'room_type_id' => $room[$i]->room_type_id,
                    'sample_data' => $room[$i]->sample_data,
                    'sort' => $room[$i]->sort
                ]);}else{
                Room::destroy($copy[$i]);
            }
        }
        return redirect(route('room.show',['hotel_id'=>$hotel_id]));
    }

    /*
    |--------------------------------------------------------------------------
    | Other Function
    |--------------------------------------------------------------------------
     */


    /*
    |--------------------------------------------------------------------------
    | Select Variable For room.insert And room.update
    |--------------------------------------------------------------------------
     */
    public function getVariableRoom(): array
    {
        return ['bedtype' => BedType::get(), 'reservetype' => ReserveType::get(), 'foodtype' => FoodType::get(), 'roomtype' => RoomType::get()];
    }


    /*
    |--------------------------------------------------------------------------
    | Save Image
    |--------------------------------------------------------------------------
     */
    public function SaveImage(RoomValidate $request): string
    {
        if ($request->hasFile('pic')) {
            $image = $request->file('pic');
            $name = rand(1, 999) . $image->getClientOriginalName();
            $destinationPath = public_path('/../public/images' . '/' . date("Y") . '/' . date("m") . "/" . date("d"));
            $image->move($destinationPath, $name);
            $namesave = 'https://hihotel.org/images' . '/' . date("Y") . '/' . date("m") . "/" . date("d") . "/" . $name;
        }
        return $namesave;
    }
}
