<?php

namespace App\Http\Controllers;

use App\City;
use App\Hotel;
use Illuminate\Http\Request;
use UxWeb\SweetAlert\SweetAlert;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Gate;

class CityController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | No Insert Method Because
    | Dont Pass Any Variable To Page city.insert
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | Save City
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save()
    {
        City::create(['name' => \request()->name,]);
        alert()->success('شهر با موفقیت ذخیره شد.', 'ذخیره شهر');
        return redirect(route('city.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update City
    | Pass Variable To Page city.update
    |--------------------------------------------------------------------------
     */
    public function update(Request $request)
    {
        return view('city.update', ['city' => City::find($request->id)]);
    }

    /*
    |--------------------------------------------------------------------------
    | Update City
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated()
    {
		if (Gate::allows('Admin')) {
        City::where('id', \request()->id)->update([
            'name' => \request()->name,
        ]);
        alert()->success('شهر با موفقیت بروزرسانی شد.', 'بروزرسانی شهر');
        return redirect(route('city.show'));
		   } else {
            abort(404);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For City Table
    | Pass Variable To Page bedtype.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = City::latest()->get();
            return Datatables::of($data)
				->addColumn('jalali_create', function ($row) {
				return jalaliCreat($row);
                })
                ->rawColumns(['jalali_create'])
				->addColumn('jalali_update', function ($row) {
				return jalaliUpdate($row);
                })
                ->rawColumns(['jalali_update'])
				->addColumn('number_hotel', function ($row) {
					$hotel = Hotel::where('city_id',$row->id)->get();
					return count($hotel);
                })
                ->rawColumns(['number_hotel'])
                ->addIndexColumn()
                ->addColumn('link_city', function ($row) {
                    $btn = '<a href="/admin/city/update?id=' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['link_city'])
                ->make(true);
        }
        return view('city.show');
    }
}
