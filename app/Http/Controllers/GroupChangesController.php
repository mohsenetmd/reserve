<?php

namespace App\Http\Controllers;

use App\Date;
use App\GroupChanges;
use App\Hotel;
use App\ReserveType;
use App\Room;
use App\RoomType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class GroupChangesController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Add Group Date
    |--------------------------------------------------------------------------
     */
    public function add($id)
    {
        $reserve_type = ReserveType::all();
        $roomType = RoomType::all();
        $hotel = Hotel::find($id);
        $hotelName = $hotel->name;
        $roomArray = Room::where('hotel_id', $hotel->id)->orderBy('sort')->get();
        $i = 0;
        foreach ($roomArray as $room) {
            $keyRoom = ['price', 'discount_price', 'breakfast', 'lunch', 'dinner', 'person_number'];
            foreach ($keyRoom as $item) {
                $data[$i][$item] = $room->$item;
            }
            $room_type_id = $room->room_type_id;
            $roomName = $roomType[$room_type_id]->name;
            $data[$i]['room_name'] = $roomName;
            $data[$i]['hotel_name'] = $hotelName;
            $date[$i] = Date::create($this->CraetDate($room));
            $i += 1;
        }
        $dateId=$this->CreateJsonDateId($date);
        $idArray = array($id);
        $hotelId = json_encode($idArray);
        $group = GroupChanges::create($this->creatGroup($dateId, $hotelId));
        return view('group.add', ['date' => $date, 'data' => $data, 'reserve_type' => $reserve_type, 'group_id' => $group->id]);
    }

    /*
    |--------------------------------------------------------------------------
    | Edit Group Date
    |--------------------------------------------------------------------------
     */
    public function edit($id)
    {
        $group = GroupChanges::find($id);
        $hotel_id = json_decode($group->hotel_id);
        $hotel = Hotel::find($hotel_id);
        $hotelName = $hotel[0]->name;
        $dateArray = json_decode($group->date_id);
        $i = 0;
        $reserve_type = ReserveType::all();
        foreach ($dateArray as $item) {
            $date[$i] = Date::where('id', $item)->get();
            $rooms = Room::where('id', $date[$i][0]->room_id)->get();
            $room=$rooms[0];
            $keyRoom = ['price', 'discount_price', 'breakfast', 'lunch', 'dinner', 'person_number'];
            foreach ($keyRoom as $item) {
                $data[$i][$item] = $room->$item;
            }
            $room_type_id = $room->room_type_id;
            $roomType = RoomType::find($room_type_id);
            $roomName = $roomType->name;
            $data[$i]['room_name'] = $roomName;
            $data[$i]['hotel_name'] = $hotelName;
            $i += 1;
        }
        return view('group.edit', ['date' => $date, 'data'=>$data , 'reserve_type' => $reserve_type]);
    }

    /*
    |--------------------------------------------------------------------------
    | Update Group Date
    |--------------------------------------------------------------------------
     */
    public function update()
    {
        $dateIdArray = \request()->id;
        $date_booking_end = \request()->date_booking_end;
        $date_booking_start = \request()->date_booking_start;
        $price = \request()->price;
        $hotel_price = \request()->hotel_price;
        $discount_price = \request()->discount_price;
        $reserve_type_id = \request()->reserve_type_id;
        $date = Date::where(['id' => $dateIdArray[0], 'status' => 1])->get();
        $room_id = $date[0]->room_id;
        $room_id_temp = Room::where('id', $room_id)->get();
        $hotel_id = $room_id_temp[0]->hotel_id;
        for ($i = 0; $i < count($dateIdArray); $i++) {
            Date::where('id', $dateIdArray[$i])->update([
                'admin_created' => Auth::user()->id,
                'admin_updated' => Auth::user()->id,
                'date_booking_end' => $date_booking_end,
                'date_booking_start' => $date_booking_start,
                'price' => $price[$i],
                'draft' => '0',
                'discount_price' => $discount_price[$i],
                'hotel_price' => $hotel_price[$i],
                'reserve_type_id' => $reserve_type_id[$i],
            ]);
        }
        GroupChanges::where('id', \request()->group_id)->update([
            'date_booking_end' => $date_booking_end,
            'date_booking_start' => $date_booking_start,
        ]);
        return redirect(route('hotel.show'));
    }

     /*
     |--------------------------------------------------------------------------
     | Delete Group Date
     |--------------------------------------------------------------------------
      */
    public function del()
    {
        $group = GroupChanges::find(\request()->id);
        $date_array = json_decode($group->date_id);
        $hotel_array = json_decode($group->hotel_id);
        foreach ($date_array as $item) {
            Date::destroy($item);
        }
        GroupChanges::destroy(\request()->id);
        return back();
    }

    /*
    |--------------------------------------------------------------------------
    | Copy Group Date
    |--------------------------------------------------------------------------
     */
    public function copy($id)
    {
        $group = GroupChanges::find($id);
        $date_id = json_decode($group->date_id);
        $newdate_id = array();
        foreach ($date_id as $item) {
            $date = Date::find($item);
            $newdate = $date->replicate();
            $newdate->admin_created = Auth::user()->id;
            $newdate->save();
            array_push($newdate_id, $newdate->id);
        }
        $newsgroup = $group->replicate();
        $newsgroup->date_id = json_encode($newdate_id);
        $newsgroup->admin_created = Auth::user()->id;
        $newsgroup->save();
        return back();
    }


    public function CraetDate($room): array
    {
        return [
            'admin_created' => Auth::user()->id,
            'admin_updated' => Auth::user()->id,
            'bed_price' => '0',
            'child_price' => '0',
            'date_booking_end' => '1499 / 12 / 31',
            'date_booking_start' => '1499 / 12 / 31',
            'price' => $room->price,
            'discount_price' => $room->discount_price,
            'hotel_price' => $room->discount_price,
            'number' => '0',
            'draft' => '1',
            'gr' => '1',
            'reserve_type_id' => '1',
            'room_id' => $room->id,
        ];
    }


    public function CreateJsonDateId($date){
        for ($i = 0; $i < count($date); $i++) {
            $dateIdArray[$i] = $date[$i]->id;
        }
        $dateId = json_encode($dateIdArray);
        return $dateId;
    }


    public function creatGroup(string $dateId, string $hotelId): array
    {
        return [
            'date_id' => $dateId,
            'hotel_id' => $hotelId,
            'date_booking_start' => '1499/12/31',
            'date_booking_end' => '1499/12/31',
            'draft' => '1',
            'admin_created' => Auth::user()->id
        ];
    }
}
