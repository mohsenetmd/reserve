<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FoodType;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Gate;

class FoodTypeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | No Insert Method Because
    | Dont Pass Any Variable To Page foodType.insert
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | Save FoodType
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save()
    {
        FoodType::create([
            'name' => \request()->name,
        ]);
        alert()->success('نوع غذا با موفقیت ذخیره شد.', 'ذخیره نوع غذا');
        return redirect(route('foodtype.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update FoodType
    | Pass Variable To Page foodtype.update
    |--------------------------------------------------------------------------
     */
    public function update(Request $request)
    {
        return view('foodtype.update', ['foodtype' => FoodType::find($request->id)]);
    }

    /*
    |--------------------------------------------------------------------------
    | Update FoodType
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated()
    {
        FoodType::where('id', \request()->id)->update([
            'name' => \request()->name,
        ]);
        return redirect(route('foodtype.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For FoodType Table
    | Pass Variable To Page foodtype.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = FoodType::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('link_foodtype', function ($row) {
                    $btn = '<a href="/admin/foodtype/update?id=' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['link_foodtype'])
                ->make(true);
        }
        return view('foodtype.show');
    }

}
