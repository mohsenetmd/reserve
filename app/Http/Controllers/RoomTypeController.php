<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomType;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Gate;

class RoomTypeController extends Controller
{
    public function update(Request $request)
    {
		if (Gate::allows('Admin')) {
        return view('roomtype.update', ['roomtype' => RoomType::find($request->id)]);
		        } else {
            abort(404);
        }
    }


    public function save()
    {
		if (Gate::allows('Admin')) {
        RoomType::create([
            'name' => \request()->name,
        ]);
        alert()->success('نوع اتاق با موفقیت ذخیره شد.', 'ذخیره نوع اتاق');
        return redirect(route('roomtype.show'));
				        } else {
            abort(404);
        }
    }

    public function updated()
    {
		if (Gate::allows('Admin')) {
        RoomType::where('id', \request()->id)->update([
            'name' => \request()->name,
        ]);
        return redirect(route('roomtype.show'));
						        } else {
            abort(404);
        }
    }

    public function show(Request $request)
    {
		if (Gate::allows('Admin')) {
        if ($request->ajax()) {
            $data = RoomType::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('link_roomtype', function ($row) {
                    $btn = '<a href="/admin/roomtype/update?id=' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['link_roomtype'])
                ->make(true);
        }
        return view('roomtype.show');
								        } else {
            abort(404);
        }
    }
}
