<?php

namespace App\Http\Controllers;

use App\HotelType;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class HotelTypeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | No Insert Method Because
    | Dont Pass Any Variable To Page hotelhype.insert
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | Save HotelType
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function save()
    {
        HotelType::create([
            'name' => \request()->name,
        ]);
        alert()->success('نوع هتل با موفقیت ذخیره شد.', 'ذخیره نوع هتل');
        return redirect(route('hoteltype.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For Update HotelType
    | Pass Variable To Page hoteltype.update
    |--------------------------------------------------------------------------
     */
    public function update(Request $request)
    {
        return view('hoteltype.update', ['hoteltype' => HotelType::find($request->id)]);
    }


    /*
    |--------------------------------------------------------------------------
    | Update HotelType
    | Validate Request And Save In MySql
    |--------------------------------------------------------------------------
     */
    public function updated()
    {
        HotelType::where('id', \request()->id)->update([
            'name' => \request()->name,
        ]);
        return redirect(route('hoteltype.show'));
    }

    /*
    |--------------------------------------------------------------------------
    | Create View For HotelType Table
    | Pass Variable To Page hoteltype.show
    |--------------------------------------------------------------------------
     */
    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = HotelType::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('link_hoteltype', function ($row) {
                    $btn = '<a href="/admin/hoteltype/update?id=' . $row->id . '" class="edit btn btn-danger btn-sm">ویرایش</a>';
                    return $btn;
                })
                ->rawColumns(['link_hoteltype'])
                ->make(true);
        }
        return view('hoteltype.show');
    }
}
