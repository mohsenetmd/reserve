<?php

namespace App\Http\Controllers;

use App\Date;
use App\Fard;
use App\User;
use App\Hotel;
use App\RoomType;
use App\SaveReserve;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Yajra\DataTables\Facades\DataTables;
use SoapClient;

class SaveReserveController extends Controller
{

    public function save()
    {
        $online = $this->checkOnline($this->getTotalAll());
        $voucher=(int)microtime(true);
        $data = SaveReserve::create($this->getArrCreateSaveReserve($online, $voucher));
        return view('fard.insert', ['tedad' => \request()->tedad, 'save_reserve_id' => $data->id]);
    }

    public function upsavereserve(){
        $data = SaveReserve::where('id',request()->id)->get();
        $saveReserve = SaveReserve::find(request()->id);
        $totalarray = json_decode($saveReserve->total);
        $roomarray = json_decode($saveReserve->room);
        $hotel = Hotel::find($roomarray->hotel_id);
        $roomtype = RoomType::find($roomarray->room_type_id);
        $totalprice = $totalarray->discount_price + $saveReserve->total_bed_price;
        $hotelName = $hotel->name;
        $roomName = $roomtype->name;
        $update=Auth::user()->id;
        return view('savereserve.updatereserve',['data'=>$data,'update'=>$update,'hotel'=>$hotelName,'room'=>$roomName,'totalprice'=>$totalprice]);
    }

    public function updatesave(){
        $data=SaveReserve::find(\request()->id)->update([
            'condition'=>\request()->condition,
            'admin_updated'=>\request()->admin_updated,
            'time'=>time()
        ]);
        $saveReserve=SaveReserve::find(\request()->id);
        $voucher=$saveReserve->voucher;
        $user=User::find('2');
        $mobile=$user->mobile;
        if(\request()->condition == 3) {
            $send = "رزرو شما با کد رزرواسیون : " . $voucher . " مورد تایید قرار گرفت" ;
                Smsirlaravel::send($send, $mobile);
        }
        return redirect(route('savereserve.show'));

    }

    public function vouchershow()
    {
        $reserve = SaveReserve::where('voucher', \request()->voucher)->get();
        return view('voucher.show', ['reservre' => $reserve]);
    }

    public function pardakht()
    {
        $s = SaveReserve::where('voucher', request()->voucher)->get();
        $total = $s[0]->total;
        $totalarr = json_decode($total);
        $room = $s[0]->room;
        $roomarr = json_decode($room);
        $hotel = Hotel::find($roomarr->hotel_id);
        $roomtype = RoomType::find($roomarr->room_type_id);
        $MerchantID = ''; //Required
        //$Amount = $totalarr[0]->discount_price; //Amount will be based on Toman - Required
        $Amount = 100; //Amount will be based on Toman - Required
        $Description = $hotel->name . ': ' . $roomtype->name; // Required
        $Mobile = Auth::user()->mobile; // Optional
        $CallbackURL = 'http://localhost:8000/pardakht/verify?id=' . request()->voucher; // Required
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );
        if ($result->Status == 100) {
            $link = 'https://www.zarinpal.com/pg/StartPay/' . $result->Authority;
        } else {
            echo 'ERR: ' . $result->Status;
        }
        return view('pardakht',['link'=>$link]);
    }

    public function complete()
    {
        $savereserve=SaveReserve::where('voucher', request()->id)->get();
        $room = $savereserve[0]->room;
        $room = json_decode($room);
        $total = $savereserve[0]->total;
        $total = json_decode($total);
        Date::create([
            'admin_created' => 'customer',
            'admin_updated' => 'customer',
            'bed_price' => $room->bed_price,
            'child_price' => $room->child_price,
            'date_booking_end' => $savereserve[0]->date_booking_end,
            'date_booking_start' => $savereserve[0]->date_booking_start,
            'discount_price' => $room->discount_price,
            'number' => '0',
            'reserve_type_id' => 4,
            'room_id' => $room->id,
        ]);
        dd($savereserve, \request()->id, $room, $total);
    }

    public function pardakhtVerify()
    {
        $MerchantID = '';
        $s = SaveReserve::where('voucher', request()->id)->get();
        $total = $s[0]->total;
        $totalarr = json_decode($total);
        $Amount = 100; //Amount will be based on Toman
        $Authority = $_GET['Authority'];

        if ($_GET['Status'] == 'OK') {
            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ]
            );
            if ($result->Status == 100) {
                echo 'Transaction success. RefID:' . $result->RefID;
                $s = SaveReserve::where('voucher', request()->id)->update([
                    'condition'=>4,
                    'ref_id'=>$result->RefID
                ]);
				$send= 'پرداخت انجام شد'.request()->id;
				Smsirlaravel::send($send, '09158438332');
				Smsirlaravel::send($send, '0907198001');
				Smsirlaravel::send($send, '0907198002');
				Smsirlaravel::send($send, '0907198003');
				Smsirlaravel::send($send, '0907198004');
				Smsirlaravel::send($send, '0907198005');
				Smsirlaravel::send($send, '0907198006');
				Smsirlaravel::send($send, '0907198007');
				Smsirlaravel::send($send, '0907198008');
				Smsirlaravel::send($send, '0907198009');
				Smsirlaravel::send($send, '09152222987');
				Smsirlaravel::send($send, '09035115939');
                $checkOnline=SaveReserve::where('voucher', request()->id)->get();
                if($checkOnline[0]->online == 1){
                    $this->complete();
                }
            } else {
                echo 'Transaction failed. Status:' . $result->Status;
            }
        } else {
            echo 'Transaction canceled by user';
        }
    }

    public function show(Request $request)
    {
        if ($request->ajax()) {
            $data = SaveReserve::all();
            return Datatables::of($data)
                ->addColumn('user', function ($row) {
                    $user=User::find($row->admin_created)->mobile;
                    return $user;
                })
                ->rawColumns(['user'])
                ->addColumn('condition', function ($row) {
                    if($row->condition == 0){
                        $text= 'عدم ورود اطلاعات شخصی';
                    }
                    if($row->condition == 1){
                        $text= 'نیازمند استعلام';
                    }
                    if($row->condition == 2){
                        $text= 'پاسخ منفی به استعلام';
                    }
                    if($row->condition == 3){
                        $text= 'جواب مثبت به استعلام و منتظر پرداخت';
                    }
                    if($row->condition == 4){
                        $text= 'پرداخت شده';
                    }
                    return $text;
                })
                ->rawColumns(['user'])
                ->addColumn('edit', function ($row) {
                    $now=time();
                    $time=$row->time;
                    if(($now - $time) < 2400){
                    $id=$row->id;
                    $btn='<a class="btn btn-primary" href="http://localhost:8000/admin/savereserve/update?id=' . $id . '">ویرایش</a>';
                    return $btn;}else{
                        return "پایان زمان ویرایش";
                    }
                })
                ->rawColumns(['edit'])
                ->addColumn('fard', function ($row) {
                    if($row->condition > 0){
                    $fard=Fard::where('save_reserve_id',$row->id)->get();
                    return $fard[0]->family;}else{
                        return "none";
                    }
                })
                ->rawColumns(['fard'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('reserve.showtable');
    }

    /**
     * @return array
     */
    public function getTotalAll(): array
    {
        $total = json_decode(\request()->total);
        $totalAll = $total[0]->all;
        $totalAll = ((array)$totalAll);
        array_pop($totalAll);
        return $totalAll;
    }

    /**
     * @param array $totalAll
     * @return int
     */
    public function checkOnline(array $totalAll): int
    {
        $online = 1;
        foreach ($totalAll as $item) {
            if ($item->reserve_type_id == 1) {
                $online = 0;
            }
        }
        return $online;
    }

    /**
     * @param int $online
     * @param int $voucher
     * @return array
     */
    public function getArrCreateSaveReserve(int $online, int $voucher): array
    {
        return [
            'admin_created' => Auth::user()->id,
            'admin_updated' => Auth::user()->id,
            'condition' => 0,
            'online' => $online,
            'date_booking_end' => \request()->date_booking_end,
            'date_booking_start' => \request()->date_booking_start,
            'room' => \request()->room,
            'status' => 1,
            'tedad' => \request()->tedad,
            'total' => \request()->total,
            'total_bed_price' => \request()->total_bed_price,
            'voucher' => $voucher,
            'time' => time()
        ];
    }
}
