<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class GoogleAuthController extends Controller
{
    public function redirect(){
        return Socialite::driver('google')->redirect();
    }

    public function callback(){
        $googleUser=Socialite::driver('google')->user();
        $conditionExistUser=User::where('email',$googleUser->email)->first();
        if($conditionExistUser){
            $newUser=Auth::loginUsingId($conditionExistUser->id);
        }else{
            $newUser=User::create([
                'name'=>$googleUser->name,
                'family'=>$googleUser->name,
                'mobile'=>'0911111111',
                'email'=>$googleUser->email,
                'password'=>bcrypt(Str::random(16)),
                'adminlevel'=>'0'
            ]);
            Auth::loginUsingId($newUser->id);
        }
        return view('welcome',['user'=>$newUser->name]);
    }


}
