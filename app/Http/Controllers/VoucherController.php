<?php

namespace App\Http\Controllers;

use App\SaveReserve;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class VoucherController extends Controller
{
    public function ajax()
    {
        $saveReserve = SaveReserve::where('voucher', \request()->voucher)->get();
        $value = \request()->voucher;
        $time = $saveReserve[0]->time;
        if ($saveReserve[0]->condition == 3) {
            $returnHTML = view('test', ['voucher' => request()->voucher,'time'=>$time])->render();
            return response()->json($returnHTML);
        }else{
            return false;
        }
    }
}
