<?php

namespace App\Http\Controllers;

use App\BedType;
use App\Discount;
use App\Fard;
use App\Hotel;
use App\HotelStar;
use App\RoomType;
use App\SaveReserve;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use SoapClient;

class PardakhtController extends Controller
{

    public function mobileInsert()
    {
        Cookie::queue(Cookie::forget('voucher'));
        return view('pardakht.mobile-insert');
    }

    public function fardInsert()
    {
        $this->checkLogin();
        $online = $this->checkOnline($this->getTotalAll());
        $voucher = (int)microtime(true);
        $data = SaveReserve::create($this->getArrCreateSaveReserve($online, $voucher));
        $saveReserve = SaveReserve::find($data->id);
        $this->deleteDiscount($saveReserve);
        $roomarray = json_decode(request()->room);
        $totalarray = json_decode(request()->total);
        $name = $this->getName($saveReserve);
        $amount = $this->getPrice($saveReserve);
        $hotel_star_view = ['0' => '', '1' => 'یک ستاره', '2' => 'دو ستاره', '3' => 'سه ستاره', '4' => 'چهار ستاره', '5' => 'پنج ستاره'];
        return view('pardakht.fard-insert', ['save_reserve_id' => $data->id, 'voucher' => $voucher, 'roomarray' => $roomarray,
            'amount' => $amount, 'hotel_star_name' => $hotel_star_view[$name['hotel_star_name']], 'request' => request(), 'total' => $totalarray, 'name' => $name]);
    }

    public function inquiry()
    {
        $checkSavePerson = $this->fardSave();
        $saveReserve = SaveReserve::find(request()->save_reserve_id);
        cookie()->queue(cookie('voucher', $saveReserve->voucher, 60));
        $Amount = $this->getPrice($saveReserve);
        if ($saveReserve->online == 0) {
            if ($checkSavePerson == 0) {
                $this->sendSmsToMobile($saveReserve,$Amount);
            }
            return view('pardakht.inquiry', ['save_reserve' => $saveReserve, 'now' => time(), 'condition' => $checkSavePerson, 'price' => $Amount]);
        } else {
            $Description = $this->getDescriptin($saveReserve); // Required
            $result = $this->getLinkPardakht($Amount, $Description);
            $this->sendSmsToMobileOnline($saveReserve,$Amount);
            if ($result->Status == 100) {
                $link = 'https://www.zarinpal.com/pg/StartPay/' . $result->Authority;
            } else {
                echo 'ERR: ' . $result->Status;
            }
            return redirect($link);
        }
    }

    public function ajax()
    {
        $saveReserve = SaveReserve::where('voucher', \request()->voucher)->get();
        $time = $saveReserve[0]->time;
        if ($saveReserve[0]->condition == 3) {
            $returnHTML = view('pardakht.ajax', ['voucher' => request()->voucher, 'time' => $time,'save_reserve_id'=>$saveReserve[0]->id])->render();
            return response()->json($returnHTML);
        } else {
            return false;
        }
    }

    public function link()
    {
        $s = SaveReserve::where('voucher', request()->voucher)->get();
        $saveReserve = $s[0];
        $totalarray = json_decode($saveReserve->total);
        $Amount = $this->getPrice($saveReserve);
        $Description = $this->getDescriptin($saveReserve);
        $result = $this->getLinkPardakht($Amount, $Description);
        if ($result->Status == 100) {
            $link = 'https://www.zarinpal.com/pg/StartPay/' . $result->Authority;
        } else {
            echo 'ERR: ' . $result->Status;
        }
        return redirect($link);
    }

    public function verify()
    {
        $saveReserve = SaveReserve::find(request()->id);
        $fard = Fard::where('save_reserve_id', $saveReserve->id)->get();
        $mobileCustomer = $this->getMobileCustomer($saveReserve);
        $roomarray = json_decode($saveReserve->room);
        $totalarray = json_decode($saveReserve->total);
        $name = $this->getName($saveReserve);
        $hotel_star_view = ['0' => '', '1' => 'یک ستاره', '2' => 'دو ستاره', '3' => 'سه ستاره', '4' => 'چهار ستاره', '5' => 'پنج ستاره'];
        $MerchantID = '53c8bfb4-8216-11e9-80a0-000c29344814';
        $Amount = $this->getPrice($saveReserve); //Amount will be based on Toman
        $Authority = $_GET['Authority'];
        if ($_GET['Status'] == 'OK') {
            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ]
            );
            if ($result->Status == 100) {
                echo 'Transaction success. RefID:' . $result->RefID;
                $s = SaveReserve::where('id', request()->id)->update([
                    'condition' => 4,
                    'ref_id' => $result->RefID
                ]);
                $discount=Discount::where('save_reserve_id',request()->id)->get();
                Discount::where('discount_code',$discount[0]->discount_code)->update([
                    'number_use'=> $discount[0]->number_use - 1
                ]);
                $this->sendVerifyToMobile($saveReserve,$Amount);
                return view('pardakht.factor', ['save_reserve_id' => request()->id, 'save_reserve' => $saveReserve,
                    'roomarray' => $roomarray, 'hotel_star_name' => $hotel_star_view[$name['hotel_star_name']],
                    'total' => $totalarray, 'fard' => $fard, 'mobile' => $mobileCustomer, 'name' => $name]);
            } else {
                echo 'Transaction failed. Status:' . $result->Status;
            }
        } else {
            echo 'Transaction canceled by user';
        }
    }

    /**
     * @param int $online
     * @param int $voucher
     * @return array
     */
    public function getArrCreateSaveReserve(int $online, int $voucher): array
    {
        return [
            'admin_created' => Auth::user()->id,
            'admin_updated' => Auth::user()->id,
            'condition' => 0,
            'online' => $online,
            'date_booking_end' => \request()->date_booking_end,
            'date_booking_start' => \request()->date_booking_start,
            'room' => \request()->room,
            'status' => 1,
            'tedad' => \request()->tedad,
            'total' => \request()->total,
            'time' => time(),
            'total_bed_price' => \request()->total_bed_price,
            'voucher' => $voucher
        ];
    }

    /**
     * @param array $totalAll
     * @return int
     */
    public function checkOnline(array $totalAll): int
    {
        $online = 1;
        foreach ($totalAll as $item) {
            if ($item->reserve_type_id == 1) {
                $online = 0;
            }
        }
        return $online;
    }

    /**
     * @return array
     */
    public function getTotalAll(): array
    {
        $total = json_decode(\request()->total);
        $totalAll = $total->all;
        $totalAll = ((array)$totalAll);
        array_pop($totalAll);
        return $totalAll;
    }

    /**
     * @return array
     */
    public function getArrCreateMobile(): array
    {
        return [
            'name' => 'empty',
            'family' => 'empty',
            'mobile' => \request()->mobile,
            'email' => 'empty',
            'password' => Hash::make(Str::random(10)),
        ];
    }

    /**
     * @return array
     */
    public function getArr(): array
    {
        return [
            'name' => 'empty',
            'family' => 'empty',
            'mobile' => \request()->mobile,
            'email' => Hash::make(Str::random(10)) . '@yahoo.com',
            'password' => Hash::make(Str::random(10)),
            'admin_level' => '0'
        ];
    }

    public function getHotelName($roomarray)
    {
        $hotel_id = $roomarray->hotel_id;
        $hotel = Hotel::where('id', $hotel_id)->get();
        return $hotel[0]->name;
    }

    public function getRoomTypeName($roomarray)
    {
        $room_type_id = $roomarray->room_type_id;
        $room_type = RoomType::where('id', $room_type_id)->get();
        return $room_type[0]->name;
    }

    public function getHotelStarName($roomarray)
    {
        $hotel_id = $roomarray->hotel_id;
        $hotel = Hotel::where('id', $hotel_id)->get();
        $hotel_star_id = $hotel[0]->hotel_star_id;
        $hotel_star = HotelStar::where('id', $hotel_star_id)->get();
        return $hotel_star[0]->name;
    }

    public function getBedTypeName($roomarray)
    {
        $bed_type_id = $roomarray->bed_type_id;
        $bed_type = BedType::where('id', $bed_type_id)->get();
        return $bed_type[0]->name;
    }

    /**
     * @param $roomarray
     * @param $name
     * @return mixed
     */
    public function getName($saveReserve)
    {
        $roomarray = json_decode($saveReserve->room);
        $name = array();
        $name['hotel_name'] = $this->getHotelName($roomarray);
        $name['room_type_name'] = $this->getRoomTypeName($roomarray);
        $name['hotel_star_name'] = $this->getHotelStarName($roomarray);
        $name['bed_type_name'] = $this->getBedTypeName($roomarray);
        return $name;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function getRequest($request)
    {
        $request['start'] = request()->date_booking_start;
        $request['end'] = request()->date_booking_end;
        $request['tedad'] = request()->tedad;
        return $request;
    }

    public function checkLogin(): void
    {
        if (count(User::where('mobile', \request()->mobile)->get()) == 0) {
            $user = User::create($this->getArr());
            Auth::loginUsingId($user->id);
        } else {
            $user = User::where('mobile', \request()->mobile)->get();
            Auth::loginUsingId($user[0]->id);
        }
    }

    /**
     * @return mixed
     */
    public function fardSave()
    {
        $condition = SaveReserve::where('id', request()->save_reserve_id)->get();
        $conditionif = $condition[0]->condition;
        if ($conditionif == 0) {
            SaveReserve::where('id', request()->save_reserve_id)->update([
                'condition' => 1,
                'time' => time()
            ]);
            Fard::create([
                'name' => \request()->name ?? 'none',
                'family' => \request()->family ?? 'none',
                'melli_number' => \request()->melli_number,
                'sex' => \request()->sex,
                'save_reserve_id' => \request()->save_reserve_id
            ]);
        }
        return $conditionif;
    }

    /**
     * @param $mobile
     * @param $name
     * @param $saveReserve
     * @param $totalarray
     */
    public function sendSmsToMobile($saveReserve,$Amount): void
    {
        $totalarray = json_decode($saveReserve->total);
        $name = $this->getName($saveReserve);
        $mobile = Auth::user()->mobile;
        $send = \request()->family . "\r\n" . 'شماره موبایل :' . $mobile . "\r\n" .
            'هتل : ' . $name['hotel_name'] . "\r\n" . 'اتاق : ' . $name['room_type_name'] . "\r\n" . 'تاریخ ورود : ' . $saveReserve->date_booking_start . "\r\n" . 'تاریخ خروج : ' . $saveReserve->date_booking_end . "\r\n" . 'قیمت : ' .
            $totalarray->discount_price . "\r\n" . 'کد واچر : ' . $saveReserve->voucher . "\r\n" . 'تعداد اتاق : ' . $saveReserve->tedad. "\r\n" . 'مبلغ کل با کد تخفیف' . $Amount;
        Smsirlaravel::send($send, '09158438332');
        // Smsirlaravel::send($send, '09057198001');
        // Smsirlaravel::send($send, '09057198002');
        // Smsirlaravel::send($send, '09057198003');
        // Smsirlaravel::send($send, '09057198004');
        // Smsirlaravel::send($send, '09057198005');
        // Smsirlaravel::send($send, '09057198006');
        Smsirlaravel::send($send, '09057198007');
        // Smsirlaravel::send($send, '09057198008');
        // Smsirlaravel::send($send, '09057198009');
        Smsirlaravel::send($send, '09152222987');
        Smsirlaravel::send($send, '09035115939');
    }

    public function sendSmsToMobileOnline($saveReserve,$Amount): void
    {
        $totalarray = json_decode($saveReserve->total);
        $name = $this->getName($saveReserve);
        $mobile = Auth::user()->mobile;
        $send =": ثبت درخواست رزرو انلاین توسط" . \request()->family . "\r\n" . 'شماره موبایل :' . $mobile . "\r\n" .
            'هتل : ' . $name['hotel_name'] . "\r\n" . 'اتاق : ' . $name['room_type_name'] . "\r\n" . 'تاریخ ورود : ' . $saveReserve->date_booking_start . "\r\n" . 'تاریخ خروج : ' . $saveReserve->date_booking_end . "\r\n" . 'قیمت : ' .
            $totalarray->discount_price . "\r\n" . 'کد واچر : ' . $saveReserve->voucher . "\r\n" . 'تعداد اتاق : ' . $saveReserve->tedad. "\r\n" . 'مبلغ کل با کد تخفیف' . $Amount . "\r\n" . "توجه نیاز به استعلام برای این رزرو نمی باشد.";
        Smsirlaravel::send($send, '09158438332');
        // Smsirlaravel::send($send, '09057198001');
        // Smsirlaravel::send($send, '09057198002');
        // Smsirlaravel::send($send, '09057198003');
        // Smsirlaravel::send($send, '09057198004');
        // Smsirlaravel::send($send, '09057198005');
        // Smsirlaravel::send($send, '09057198006');
        Smsirlaravel::send($send, '09057198007');
        Smsirlaravel::send($send, '09057198008');
        // Smsirlaravel::send($send, '09057198009');
        Smsirlaravel::send($send, '09152222987');
        Smsirlaravel::send($send, '09035115939');
    }

    public function sendVerifyToMobile($saveReserve,$Amount): void
    {
        $totalarray = json_decode($saveReserve->total);
        $name = $this->getName($saveReserve);
        $mobile = Auth::user()->mobile;
        $send = 'اتاق با مشخصات زیر پرداخت شد'. "\r\n" . "\r\n" . 'شماره موبایل :' . $mobile . "\r\n" .
            'هتل : ' . $name['hotel_name'] . "\r\n" . 'اتاق : ' . $name['room_type_name'] . "\r\n" . 'تاریخ ورود : ' . $saveReserve->date_booking_start . "\r\n" . 'تاریخ خروج : ' . $saveReserve->date_booking_end . "\r\n" . 'قیمت : ' .
            $totalarray->discount_price . "\r\n" . 'کد واچر : ' . $saveReserve->voucher . "\r\n" . 'تعداد اتاق : ' . $saveReserve->tedad . "\r\n" . 'مبلغ کل با کد تخفیف' . $Amount;
        Smsirlaravel::send($send, '09158438332');
        // Smsirlaravel::send($send, '09057198001');
        // Smsirlaravel::send($send, '09057198002');
        // Smsirlaravel::send($send, '09057198003');
        // Smsirlaravel::send($send, '09057198004');
        // Smsirlaravel::send($send, '09057198005');
        // Smsirlaravel::send($send, '09057198006');
        Smsirlaravel::send($send, '09057198007');
        // Smsirlaravel::send($send, '09517198008');
        // Smsirlaravel::send($send, '09057198009');
        Smsirlaravel::send($send, '09152222987');
        Smsirlaravel::send($send, '09035115939');
    }

    /**
     * @param $totalarray
     * @param $saveReserve
     * @return float|int
     */
    public function getPrice($saveReserve)
    {
        $totalarray = json_decode($saveReserve->total);
        $discount = Discount::where('save_reserve_id', $saveReserve->id)->get();
        if (count($discount) > 0) {
            if (strlen($discount[0]->percent) > 0) {
                return (($totalarray->discount_price * $saveReserve->tedad) * (100 - $discount[0]->percent) / 100);
            }
            if (strlen($discount[0]->price) > 0) {
                return (($totalarray->discount_price * $saveReserve->tedad) - $discount[0]->price);
            }
        };
        return $totalarray->discount_price * $saveReserve->tedad ;
    }

    /**
     * @param $saveReserve
     */
    public function getMobileCustomer($saveReserve)
    {
        $user = User::find($saveReserve->admin_created);
        return $user->mobile;
    }

    /**
     * @param string $MerchantID
     * @param $Amount
     * @param string $Description
     * @return mixed
     */
    public function getLinkPardakht($Amount, string $Description)
    {
        $MerchantID = '53c8bfb4-8216-11e9-80a0-000c29344814';
        $CallbackURL = 'https://hihotel.org/payment/verify?id=' . request()->save_reserve_id; // Required
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'CallbackURL' => $CallbackURL,
            ]
        );
        return $result;
    }

    /**
     * @param $name
     * @return string
     */
    public function getDescriptin($saveReserve): string
    {
        $name = $this->getName($saveReserve);
        $Description = $name['hotel_name'] . ': ' . $name['room_type_name'];
        return $Description;
    }

    public function discount()
    {
        $saveReserve = SaveReserve::find(request()->id);
        $check = $this->checkDiscountCode();
        if ($this->checkDiscountCode() == 'save') {
            $this->acceptDiscountCode();
        }
        $message = ['save' => $this->getPrice($saveReserve), 'time' => 'این کد در این بازه زمانی قابلیت استفاده را ندارد.', 'number' => 'این کد بیش از حد مجاز مورد استفاده قرار گرفته است.', 'fault' => 'کد ورودی اشتباه است', 'hotel_id' => 'کد وارد شده برای این هتل قابلیت استفاده را ندارد'];
        $returnHTML = view('pardakht.discount', ['test' => $message[$check]])->render();
        return response()->json($returnHTML);
    }

    /**
     * @param $saveReserve
     * @return mixed
     */
    public function checkDiscountCode()
    {
        $discount = Discount::where('discount_code', request()->discount_code)->get();
        if (count($discount) == 0) {
            return 'fault';
        }
        $saveReserve = SaveReserve::find(request()->id);
        $hotel_id_discount = json_decode($discount[0]->hotel_id);
        $roomarray = json_decode($saveReserve->room);
        $hotel_id_reserve = $roomarray->hotel_id;
        $checkfind = array_search($hotel_id_reserve, $hotel_id_discount);
        if($checkfind == 0){
            $checkfind=1;
        }
        if ($checkfind) {
            if ($discount[0]->number_use > 0) {
                if ($this->checkTimeDiscount($saveReserve)) {
                    if (count(Discount::where('save_reserve_id', $saveReserve->id)->get()) < 1) {
                        return 'save';
                    }
                } else {
                    return 'time';
                }
            } else {
                return 'number';
            }
            return false;
        }
        return 'hotel_id';
    }

    /**
     * @param $discount
     * @param $saveReserve
     */
    public function acceptDiscountCode()
    {
        $discount = Discount::where('discount_code', request()->discount_code)->get();
        $saveReserve = SaveReserve::find(request()->id);
        $newTask = $discount[0]->replicate();
        $newTask->admin_created = Auth::user()->id; // the new project_id
        $newTask->save_reserve_id = $saveReserve->id; // the new project_id
        $newTask->save();
    }

    function nthDay($inputday)
    {
        $inputday = str_replace("/", '', $inputday);
        $time = str_split($inputday, 2);
        $year = $time[0] . $time[1];
        $month = $time[2];
        $day = $time[3];
        if ($year > 1399) {
            echo 'ok';
        }

        if ($month > 6) {
            $days = 6 * 31 + ($month - 7) * 30;
        } else {
            $days = 31 * ($month - 1);
        }

        $days = $days + $day;
        return $days;
    }

    public function deleteDiscount($saveReserve)
    {
        Discount::where('save_reserve_id', $saveReserve->id)->delete();
    }

    public function checkTimeDiscount($saveReserve)
    {
        $start = $this->nthDay($saveReserve->date_booking_start);
        $end = $this->nthDay($saveReserve->date_booking_end);
        $discount = Discount::where('discount_code', request()->discount_code)->get();
        if (count($discount) > 0) {
            $check_start = $this->nthDay($discount[0]->date_booking_start);
            $check_end = $this->nthDay($discount[0]->date_booking_end);
            if ($check_start >= $start) {
                if ($check_start <= $end) {
                    return true;
                }
            }
            if ($check_end >= $start) {
                if ($check_end <= $end) {
                    return true;
                }
            }
            if ($check_start <= $start) {
                if ($check_end >= $end) {
                    return true;
                }
            }
        }
        return false;
    }
}
