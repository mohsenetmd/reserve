<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DateValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'admin_created' => 'required',
            'admin_updated' => 'required',
            'bed_price' => 'nullable',
            'child_price' => 'nullable',
            'date_booking_end' => 'nullable',
            'date_booking_start' => 'nullable',
            'price' => 'required',
            'discount_price' => 'required',
            'hotel_price' => 'required',
            'number' => 'required',
            'reserve_type_id' => 'required',
            'room_id' => 'required',
        ];
    }
}
