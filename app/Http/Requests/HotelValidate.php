<?php

namespace App\Http\Requests;

use App\Http\Controllers\HotelController;
use App\WpPost;
use Illuminate\Foundation\Http\FormRequest;

class HotelValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'city_id' => 'required',
            'hotel_star_id' => 'required',
            'hotel_type_id' => 'required',
            'start_price' => 'required',
            'admin_created' => 'required',
            'admin_updated' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $IDWpPost = HotelController::CreateWpPost();
        $this->merge([
            'wp_post_id'=>$IDWpPost->ID
        ]);
    }

}
