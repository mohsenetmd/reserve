<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'admin_created' => 'required',
            'admin_updated' => 'required',
            'bed_number' => 'nullable',
            'bed_price' => 'nullable',
            'bed_type_id' => 'required',
            'breakfast' => 'nullable',
            'dinner' => 'nullable',
            'lunch' => 'nullable',
            'breakfast_price' => 'nullable',
            'child' => 'required',
            'child_number' => 'nullable',
            'child_price' => 'nullable',
            'dinner_price' => 'nullable',
            'discount_price' => 'required',
            'food_type_id' => 'nullable',
            'hotel_id' => 'required',
            'lunch_price' => 'nullable',
            'more_bed' => 'required',
            'person_number' => 'required',
            'price' => 'required',
            'reserve_type_id' => 'required',
            'room_number' => 'nullable',
            'room_type_id' => 'required',
            'sample_data' => 'nullable',
            'sort' => 'nullable'
        ];
    }
}
