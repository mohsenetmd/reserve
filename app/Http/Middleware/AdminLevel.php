<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $admin_level = Auth::user()->admin_level;
            if ($admin_level == 3) {
                return $next($request);
            }
        }
        abort(404);
    }
}
