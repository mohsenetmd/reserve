<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupChanges extends Model
{
    use SoftDeletes;
    protected $guarded=[];
    protected $table='group_changes';
}