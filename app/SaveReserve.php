<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveReserve extends Model
{
    protected $guarded=[];
    public function Room(){
        return $this->belongsTo(Room::class);
    }
}
