<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveType extends Model
{
    protected $guarded=[];
    public function Room(){
        return $this->hasMany(Room::class);
    }
}
