<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $guarded=[];
    public function Hotel(){
        return $this->belongsTo(Hotel::class);
    }
    public function FoodType(){
        return $this->belongsTo(FoodType::class);
    }
    public function BedType(){
        return $this->belongsTo(BedType::class);
    }
    public function ReserveType(){
        return $this->belongsTo(ReserveType::class);
    }
    public function RoomType(){
        return $this->belongsTo(RoomType::class);
    }
    public function Date(){
        return $this->hasMany(Date::class);
    }
    public function SaveReserve(){
        return $this->hasMany(SaveReserve::class);
    }
}
