<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WpPostmeta extends Model
{
    protected $guarded=[];
    protected $table="wp_postmeta";
    protected $primaryKey="meta_id";

}
