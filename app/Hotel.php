<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $guarded=[];

    public function City(){
        return $this->belongsTo(City::class);
    }

    public function HotelStar(){
        return $this->belongsTo(HotelStar::class);
    }

    public function HotelType(){
        return $this->belongsTo(HotelType::class);
    }

    public function Room(){
        return $this->hasMany(Room::class);
    }

}
