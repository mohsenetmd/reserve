<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded=[];
    public function Hotel(){
        return $this->hasMany(Hotel::class);
    }
}
