
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-2">تاریخ شروع</div>
                <div class="col-lg-2">
                <?php echo e($item->date_booking_start); ?>

                </div>
                <div class="col-lg-2">تاریخ پایان</div>
                <div class="col-lg-2">
                <?php echo e($item->date_booking_end); ?>

                </div>
                <a href="<?php echo e(route('group.editgroup',['id'=>$item->id])); ?>" class="btn btn-success" type="submit">ویرایش</a>
                <a href="<?php echo e(route('group.delgroup',['id'=>$item->id])); ?>" class="btn btn-danger" type="submit">حذف</a>
                <div class="col-lg-12"><hr></div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/group/groupedit.blade.php ENDPATH**/ ?>