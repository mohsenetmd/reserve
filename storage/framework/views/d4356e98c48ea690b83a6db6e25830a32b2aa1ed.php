<?php $__env->startSection('content'); ?>
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form method="post" action="<?php echo e(route('date.updated')); ?>">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('put'); ?>
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات اتاق</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    <?php $__currentLoopData = $reservetype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"
                                        <?php if($date->reserve_type_id == $item->id): ?>
                                            selected
                                            <?php endif; ?>
                                            ><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد اتاق</label>
                                <input type="text" class="form-control" name="number" required
                                       value="<?php echo e($date->number); ?>">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت مصوب</label>
                                <input type="text" class="form-control" name="price"
                                       value="<?php echo e($date->price); ?>"
                                       required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نرخ واریزی مسافر</label>
                                <input type="text" class="form-control" name="discount_price"
                                       value="<?php echo e($date->discount_price); ?>"
                                       required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نرخ واریزی هتل</label>
                                <input type="text" class="form-control" name="hotel_price"
                                       value="<?php echo e($date->hotel_price); ?>"
                                       required>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price" value="<?php echo e($date->bed_price); ?>">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price" value="<?php echo e($date->child_price); ?>">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4" style="display: none;">
                                <label>نوع غذا</label>
                                <select class="foodtype_search form-control" name="food_type_id">
                                    <?php $__currentLoopData = $foodtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                            value="<?php echo e($item->id); ?>"
                                                <?php if($date->food_type_id == $item->id): ?>
                                                selected
                                            <?php endif; ?>><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>تاریخ شروع</label>
                                <input type="text" class="form-control" name="date_booking_start" value="<?php echo e($date->date_booking_start); ?>">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>تاریخ پایان</label>
                                <input type="text" class="form-control" name="date_booking_end" value="<?php echo e($date->date_booking_end); ?>">
                            </div>
                            <input type="hidden" class="form-control" name="room_id" value="<?php echo e(request()->room_id); ?>">
                            <input type="hidden" class="form-control" name="date_id" value="<?php echo e(request()->date_id); ?>">
                            <input type="hidden" class="form-control" name="admin_created" value="<?php echo e($date->admin_created); ?>">
                            <input type="hidden" class="form-control" name="admin_updated" value="<?php echo e(Auth::user()->name); ?>">
                        </div>
                    </div>
                    <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>

    </script>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/date/update.blade.php ENDPATH**/ ?>