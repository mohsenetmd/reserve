<?php $__env->startSection('content'); ?>
    <form method="post" action="<?php echo e(route('group.update')); ?>">
        <?php echo csrf_field(); ?>
        <?php echo method_field('put'); ?>
        <div class="container">
            <div class="row">
                <?php for($i=0;$i<count($date);$i++): ?>
                    <div class="col-lg-3"><?php echo e($data[0]['hotel_name']); ?></div>
                    <div class="col-lg-3"><?php echo e($data[$i]['room_name']); ?></div>
                    <div class="col-lg-3">
                        <label>انتخاب نوع رزرو</label>
                        <select class="form-control" name="reserve_type_id[]">
                            <?php for($j=0;$j<count($reserve_type);$j++): ?>
                                <option value="<?php echo e($reserve_type[$j]->id); ?>"
                                        <?php if($date[$i]->reserve_type_id == $reserve_type[$j]->id): ?>
                                        selected
                                    <?php endif; ?>
                                ><?php echo e($reserve_type[$j]->name); ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="col-lg-12"></div>
                    <div class="col-lg-3">
                        <label style="margin-left: 20px;">تعداد افراد اتاق</label>
                        <span><?php echo e($data[$i]['person_number']); ?></span>
                    </div>
                    <div class="col-lg-3">
                        <label style="margin-left: 20px;">قیمت مصوب</label>
                        <input class="form-control" name="price[]" type="text" value="<?php echo e($date[$i]->price); ?>">
                    </div>
                    <div class="col-lg-3">
                        <label style="margin-left: 20px;">نرخ واریزی هتل</label>
                        <input class="form-control" name="hotel_price[]" type="text" value="<?php echo e($date[$i]->hotel_price); ?>">
                    </div>
                    <div class="col-lg-3">
                        <label style="margin-left: 20px;">نرخ واریزی مسافر</label>
                        <input class="form-control" name="discount_price[]" type="text" value="<?php echo e($date[$i]->discount_price); ?>">
                    </div>
                    <div class="col-lg-12" style="margin-top: 5px;margin-bottom: 5px;"></div>
                    <?php if($data[$i]['breakfast']==1): ?>
                        <div class="col-lg-3">
                        <span class="badge-pill btn-success">
                        صبحانه دارد
                        </span>
                        </div>
                    <?php else: ?>
                        <div class="col-lg-3">
                        <span class="badge-pill btn-danger">
                        صبحانه ندارد
                        </span>
                        </div>
                    <?php endif; ?>
                    <?php if($data[$i]['lunch']==1): ?>
                        <div class="col-lg-3">
                        <span class="badge-pill badge-success">
                        ناهار دارد
                        </span>
                        </div>
                    <?php else: ?>
                        <div class="col-lg-3">
                        <span class="badge-pill badge-danger">
                        ناهار ندارد
                        </span>
                        </div>
                    <?php endif; ?>
                    <?php if($data[$i]['dinner']==1): ?>
                        <div class="col-lg-3">
                        <span class="badge-pill badge-success">
                        شام دارد
                        </span>
                        </div>
                    <?php else: ?>
                        <div class="col-lg-3">
                        <span class="badge-pill badge-danger">
                        شام ندارد
                        </span>
                        </div>
                    <?php endif; ?>
                    <input type="hidden" name="id[]" value="<?php echo e($date[$i]->id); ?>">
                    <div class="col-lg-12" style="margin-bottom: 5px;"></div>
                    <div class="col-lg-12">
                        <hr>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
        <div id="app">
            <date-component></date-component>
        </div>
        <input type="hidden" name="group_id" value="<?php echo e($group_id); ?>">
        <button class="btn btn-success" type="submit">ذخیره</button>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laracast\reserve\resources\views/group/add.blade.php ENDPATH**/ ?>