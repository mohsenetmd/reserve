<?php $__env->startSection('content'); ?>
      <!-- SLIDER -->
    <section>
	 	<div class="backvid">
	    		<video autoplay="" loop="" muted="" class="vide"><source src="https://hihotel.ir/wp-content/uploads/2020/07/video.mp4" type="video/mp4"><source src="video/video.webm" type="video/webm"><source src="video/video.ogv" type="video/ogg">
	<!--                 <img alt="Third slide" src="https://hihotel.ir/wp-content/uploads/2020/07/video.mp4" class="img-responsive">
	-->                	<div class="carousel-caption">
	            		<h1>سامانه رزرواسیون های هتل</h1>
	            		<p><span class="line-t"></span>خدمات بی نظیر با امکانات عالی <span class="line-b"></span></p>
	        		</div>
				</video>
				<div class="group-sm group-middle social-box" style="display:grid;">
					<a  href="https://www.facebook.com/hihotel" style="margin-top: 5px;"><img src="https://hihotel.ir/wp-content/uploads/2019/07/facebook.svg" width="30" height="30"></a>
					<a href="https://www.linkedin.com/in/hihotel/" style="margin-top: 5px;"><img src="https://hihotel.ir/wp-content/uploads/2019/07/linkedin.svg" width="30" height="30"></a>
					<a href="http://www.instagram.com/hihotel.ir" style="margin-top: 5px;"><img src="https://hihotel.ir/wp-content/uploads/2019/07/instagram.svg" width="30" height="30"></a>
					<a href="https://t.me/hi_hotel" style="margin-top: 5px;"><img src="https://hihotel.ir/wp-content/uploads/2019/07/telegram.svg" width="30" height="30"></a>	
				</div>
	    </div>
    <div class="row" style="margin-left: 0px;">
       <div class="col-lg-3 textreserve">
            <div class="text_mobile">
                برای استفاده از خدمات رزرواسیون "های هتل" و پیگیری رزرو لطفا شماره موبایل خود را به صورت صحیح وارد کنید.
            </div>
        </div>
                
        <div class="col-lg-4">
            <div class="insert_shomare_mobile wow fadeInUp">
                <div class="input-group" style="margin-top: 20px;margin-bottom: 20px;display: flex; !importan">
                        <div class="input-group-text"
                              style="border-radius: 5px !important;background-color: rgb(185,38,46);"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 503.604 503.604" style="margin-top: 5px;enable-background:new 0 0 503.604 503.604;" xml:space="preserve" width="20px" height="20px" class=""><g><g>
	<g>
		<path d="M337.324,0H167.192c-28.924,0-53.5,23.584-53.5,52.5v398.664c0,28.916,24.056,52.44,52.98,52.44l170.412-0.184    c28.92,0,52.58-23.528,52.58-52.448l0.248-398.5C389.908,23.452,366.364,0,337.324,0z M227.68,31.476h49.36    c4.336,0,7.868,3.52,7.868,7.868c0,4.348-3.532,7.868-7.868,7.868h-49.36c-4.348,0-7.868-3.52-7.868-7.868    C219.812,34.996,223.332,31.476,227.68,31.476z M198.02,33.98c2.916-2.912,8.224-2.952,11.136,0c1.46,1.456,2.324,3.5,2.324,5.588    c0,2.048-0.864,4.088-2.324,5.548c-1.452,1.46-3.504,2.32-5.548,2.32c-2.084,0-4.088-0.86-5.588-2.32    c-1.452-1.456-2.28-3.5-2.28-5.548C195.736,37.48,196.568,35.436,198.02,33.98z M250.772,488.008    c-12.984,0-23.544-10.568-23.544-23.548c0-12.984,10.56-23.548,23.544-23.548s23.544,10.564,23.544,23.548    C274.316,477.44,263.752,488.008,250.772,488.008z M365.488,424.908H141.232V74.756h224.256V424.908z"
              data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
	</g>
</g></g> </svg></div>
  <form method="post" name="form" action="<?php echo e(route('pardakht.fardinsert')); ?>" onsubmit="return phonenumber(document.form.mobile);">
                            <?php echo csrf_field(); ?>
                            <input type="tel" class="form-control" name="mobile" placeholder="شماره همراه">
                            <input type="hidden" name="room" value="<?php echo e(request()->room); ?>">
                            <input type="hidden" name="total" value="<?php echo e(request()->total); ?>">
                            <input type="hidden" name="date_booking_start" value="<?php echo e(request()->date_booking_start); ?>">
                            <input type="hidden" name="date_booking_end" value="<?php echo e(request()->date_booking_end); ?>">
                            <input class="input_total_bed_price" type="hidden" name="total_bed_price"
                                   value="<?php echo e(request()->total_bed_price); ?>">
                            <input name="online" type="hidden" value="<?php echo e(request()->online); ?>">
                            <input name="noReserve" type="hidden" value="<?php echo e(request()->noReserve); ?>">
                            <input name="inquiry" type="hidden" value="<?php echo e(request()->inquiry); ?>">
                            <input name="tedad" type="hidden" value="<?php echo e(request()->tedad); ?>">
                </div>
                <button class="btn btn-danger" style="width: 100px;background-color: rgb(185,38,46);margin-top: 5px;">
ثبت                </button>
</form>
            </div>
        </div>


</div>
    <!-- END / SLIDER -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.newdesign', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/pardakht/mobile-insert.blade.php ENDPATH**/ ?>