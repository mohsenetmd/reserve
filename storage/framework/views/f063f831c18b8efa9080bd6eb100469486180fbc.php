<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست اتاق ها</h1>
    <a class="btn btn-primary" href="<?php echo e(route('room.insert',['hotel_id'=>request()->hotel_id])); ?>">افزودن</a>
    <a class="btn btn-success" href="<?php echo e(route('hotel.show')); ?>" style="float: left;margin-left: 20px;">بازگشت</a>
    <hr>
    <form method="post" action="<?php echo e(route('room.copy')); ?>">
        <?php echo csrf_field(); ?>
        <table class="table table-bordered data-table">
            <thead>
            <tr>
                <th>آپدیت شده توسط</th>
                <th>نوع اتاق</th>
                <th>قیمت با تخفیف</th>
                <th>تاریخ بروزرسانی</th>
                <th>مشاهده</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <select name="cd">
            <option value="1">کپی</option>
            <option value="0">حذف</option>
        </select>
        <button type="submit">ارسال</button>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "https://hihotel.org/admin/room?hotel_id=<?php echo e(request()->hotel_id); ?>",
                    method: 'POST'
                },
                columns: [
                    {data: 'admin_updated', name: 'admin_updated'},
                    {data: 'room_type', name: 'room_type'},
                    {data: 'discount_price', name: 'discount_price'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'otagh', orderable: false, searchable: false},
                ]
            });

        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/room/show.blade.php ENDPATH**/ ?>