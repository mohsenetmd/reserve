<?php $__env->startSection('content'); ?>
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form enctype="multipart/form-data" method="post" action="<?php echo e(route('room.updated')); ?>">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('put'); ?>
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات اتاق</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    <?php $__currentLoopData = $reservetype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>

                                        <?php if($room->resrerve_type_id == $item->id): ?>
                                            selected
                                            <?php endif; ?>
                                            "><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع اتاق</label>
                                <select class="roomtype_search form-control" name="room_type_id">
                                    <?php $__currentLoopData = $roomtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"
                                                <?php if($room->room_type_id == $item->id): ?>
                                                selected
                                            <?php endif; ?>><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع تخت</label>
                                <select class="bedtype_search form-control" name="bed_type_id">
                                    <?php $__currentLoopData = $bedtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"
                                                <?php if($room->bed_type_id == $item->id): ?>
                                                selected
                                            <?php endif; ?>
                                        ><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div id="app2" class="input-group">
								<label for="imageInput">عکس را وارد کنید</label>
                                <input class="form-control-file" data-preview="#preview" name="pic" type="file" id="imageInput">
                                <div v-if="showimage" style="display: block;width: 100%;">
                                <img style="margin: auto;display: block;height: 300px;" src="<?php echo e($room->pic); ?>" >
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد افراد</label>
                                <input type="text" class="form-control" name="person_number" required
                                       value="<?php echo e($room->person_number); ?>">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت</label>
                                <input type="text" class="form-control" name="price" required value="<?php echo e($room->price); ?>">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت با تخفیف</label>
                                <input type="text" class="form-control" name="discount_price"
                                       value="<?php echo e($room->discount_price); ?>"
                                       required>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تخت اضافه</label>
                                <select name="more_bed" class="form-control">
                                    <option value="0"
                                            <?php if($room->more_bed == 0): ?>
                                            selected
                                        <?php endif; ?>>ندارد
                                    </option>
                                    <option value="1"
                                            <?php if($room->more_bed == 1): ?>
                                            selected
                                        <?php endif; ?>>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_number" value="<?php echo e($room->bed_number); ?>">
                                <small class="form-text text-muted">تعداد تختی که قابلیت اضافه کردن را دارد انتخاب
                                    کنید</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price" value="<?php echo e($room->bed_price); ?>">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>کودک</label>
                                <select name="child" class="form-control">
                                    <option value="0"
                                            <?php if($room->child == 0): ?>
                                            selected
                                        <?php endif; ?>>ندارد
                                    </option>
                                    <option value="1"
                                            <?php if($room->child == 1): ?>
                                            selected
                                        <?php endif; ?>>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد کودک</label>
                                <input type="text" class="form-control" name="child_number" value="<?php echo e($room->child_number); ?>">
                                <small class="form-text text-muted">تعداد کودکی که قابلیت اضافه شدن را دارد مشخص
                                    کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price" value="<?php echo e($room->child_price); ?>">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>صبحانه</label>
                                <input type="text" class="form-control" name="breakfast_price" value="<?php echo e($room->breakfast_price); ?>">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای صبحانه را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>ناهار</label>
                                <input type="text" class="form-control" name="lunch_price" value="<?php echo e($room->lunch_price); ?>">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای ناهار را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>شام</label>
                                <input type="text" class="form-control" name="dinner_price" value="<?php echo e($room->dinner_price); ?>">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای شام را وارد کنید.</small>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>صبحانه رایگان</label>
                                <select name="breakfast" class="form-control">
                                    <option value="0"
                                            <?php if($room->breakfast == 0): ?>
                                            selected
                                        <?php endif; ?>>ندارد
                                    </option>
                                    <option value="1"
                                            <?php if($room->breakfast == 1): ?>
                                            selected
                                        <?php endif; ?>>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ناهار رایگان</label>
                                <select name="lunch" class="form-control">
                                    <option value="0"
                                            <?php if($room->lunch == 0): ?>
                                            selected
                                        <?php endif; ?>>ندارد
                                    </option>
                                    <option value="1"
                                            <?php if($room->lunch == 1): ?>
                                            selected
                                        <?php endif; ?>>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>شام رایگان</label>
                                <select name="dinner" class="form-control">
                                    <option value="0"
                                            <?php if($room->dinner == 0): ?>
                                            selected
                                        <?php endif; ?>>ندارد
                                    </option>
                                    <option value="1"
                                            <?php if($room->dinner == 1): ?>
                                            selected
                                        <?php endif; ?>>دارد
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع غذا</label>
                                <select class="foodtype_search form-control" name="food_type_id">
                                    <?php $__currentLoopData = $foodtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"
                                                <?php if($room->food_type_id == $item->id): ?>
                                                selected
                                            <?php endif; ?>><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>قیمت نوع غذا</label>
                                <input type="text" class="form-control" name="price_food_type" value="<?php echo e($room->price_food_type); ?>">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ترتیب اتاق</label>
                                <input type="text" class="form-control" name="sort" value="<?php echo e($room->sort); ?>">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>شماره اتاق</label>
                                <input type="text" class="form-control" name="room_number" value="<?php echo e($room->room_number); ?>">
                            </div>
                            <div class="form-check col-lg-12">
                                <label>در صورت نبود اطلاعات در یک تاریخ این اطلاعات جایگزین شود؟</label>
                                <select class="form-control" name="sample_data">
                                    <option value="0"
                                            <?php if($room->sample_data == 0): ?>
                                            selected
                                        <?php endif; ?>>بله
                                    </option>
                                    <option value="1"
                                            <?php if($room->sample_data == 1): ?>
                                            selected
                                        <?php endif; ?>>خیر
                                    </option>
                                </select>
                            </div>
                            <div class="form-check col-lg-12" style="margin-top: 20px">
                                <label class="form-check-label" for="defaultCheck1">
                                    راهنما مطالعه شده است؟
                                </label>
                            </div>
                            <input type="hidden" class="form-control" name="hotel_id" value="<?php echo e(request()->hotel_id); ?>">
                            <input type="hidden" class="form-control" name="id" value="<?php echo e(request()->room_id); ?>">
                            <input type="hidden" class="form-control" name="admin_created" value="<?php echo e($room->admin_created); ?>">
                            <input type="hidden" class="form-control" name="admin_updated" value="<?php echo e(Auth::user()->name); ?>">
                            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        let app2 = new Vue({
            el: '#app2',
            data: {
                showimage: true,
            },
        });
        $(document).ready(function () {
            $('.reservetype_search').select2();
            $('.foodtype_search').select2();
            $('.bedtype_search').select2();
            $('.roomtype_search').select2();
        });
        $('#lfm').filemanager('image');
    </script>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/room/update.blade.php ENDPATH**/ ?>