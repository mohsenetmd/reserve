<?php $__env->startSection('content'); ?>
    <form action="<?php echo e(route('roomtype.save')); ?>" method="POST" style="">
        <?php echo csrf_field(); ?>
        <h1 class="titr">وارد کردن نوع اتاق</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نوع اتاق</label>
                <input type="text" class="form-control" name="name" required value="<?php echo e(old('name')); ?>">
            </div>
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/roomtype/insert.blade.php ENDPATH**/ ?>