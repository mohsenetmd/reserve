<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title>سامانه رزرواسیون های هتل</title>

    <!-- Scripts -->
    <script src="<?php echo e(mix('js/app.js')); ?>"></script>

    <!-- Fonts -->

    <!-- Styles -->
    <link href="<?php echo e(mix('css/app.css')); ?>" rel="stylesheet">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-152335471-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-152335471-2');
</script>

</head>


<body>
<style>
    #content .btn-primary{
        float: left;
    }
</style>
<!-- Start vertical navbar -->
<header id="header">
</header>
<!-- Start Page content holder -->
</body>
<div class="" id="">
    <?php echo $__env->yieldContent('content'); ?>
</div>
</body>
<?php echo $__env->yieldContent('script'); ?>
</html>
<?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/layouts/user.blade.php ENDPATH**/ ?>