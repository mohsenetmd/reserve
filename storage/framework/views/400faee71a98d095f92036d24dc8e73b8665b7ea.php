
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <?php for($i=0;$i<count($hotel);$i++): ?>
                <div class="col-lg-3">
                    <?php echo e($hotel[$i]); ?>

                </div>
                <div class="col-lg-3">
                    <?php echo e($roomName[$i]); ?>

                </div>
                <div class="col-lg-3">
                    قیمت مصوب:
                    <?php echo e($price[$i]); ?>

                </div>
                <div class="col-lg-3">
                    قیمت رزرو مسافر
                    <?php echo e($discount_price[$i]); ?>

                </div>
                <?php if($breakfast[$i]==1): ?>
                    <div class="col-lg-3">
                        <span class="badge-pill btn-success">
                        صبحانه دارد
                        </span>
                    </div>
                <?php else: ?>
                    <div class="col-lg-3">
                        <span class="badge-pill btn-danger">
                        صبحانه ندارد
                        </span>
                    </div>
                <?php endif; ?>
                <?php if($lunch[$i]==1): ?>
                    <div class="col-lg-3">
                        <span class="badge-pill badge-success">
                        ناهار دارد
                        </span>
                    </div>
                <?php else: ?>
                    <div class="col-lg-3">
                        <span class="badge-pill badge-danger">
                        ناهار ندارد
                        </span>
                    </div>
                <?php endif; ?>
                <?php if($dinner[$i]==1): ?>
                    <div class="col-lg-3">
                        <span class="badge-pill badge-success">
                        شام دارد
                        </span>
                    </div>
                <?php else: ?>
                    <div class="col-lg-3">
                        <span class="badge-pill badge-danger">
                        شام ندارد
                        </span>
                    </div>
                <?php endif; ?>
                <div class="col-lg-12">
                    <hr>
                </div>
            <?php endfor; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/group/room.blade.php ENDPATH**/ ?>