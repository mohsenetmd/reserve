<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست هتل ها</h1>
    <a class="btn btn-primary" href="<?php echo e(route('hotel.insert')); ?>">افزودن</a>
    <hr>
    <form method="post" action="<?php echo e(route('hotel.show')); ?>">
        <?php echo csrf_field(); ?>
        <div class="row">
            <div class="col-lg-3">
                <select class="form-control" name="city_id">
                    <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option class="form-control" value="<?php echo e($item->id); ?>"
                                <?php if(session('city_id')==$item->id): ?>
                                selected
                            <?php endif; ?>
                        ><?php echo e($item->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="col-lg-3">
                <select class="form-control" name="hotel_star_id">
                    <?php $__currentLoopData = $hotelStar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($item->id); ?>"
                                <?php if(session('hotel_star_id')==$item->id): ?>
                                selected
                            <?php endif; ?>
                        ><?php echo e($item->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="col-lg-3">
                <select class="form-control" name="hotel_type_id">
                    <?php $__currentLoopData = $hotelType; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($item->id); ?>"
                                <?php if(session('hotel_type_id')==$item->id): ?>
                                selected
                            <?php endif; ?>
                        ><?php echo e($item->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="col-lg-3">
                <button class="btn btn-success" type="submit">جستجو</button>
                <?php if(session('hotel_type_id')): ?>
                    <a class="btn btn-danger" href="<?php echo e(route('hotel.show',['destroy'=>'1'])); ?>">حذف جستجو</a>
                <?php endif; ?>
            </div>
        </div>
    </form>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>تاریخ</th>
            <th>نام هتل</th>
            <th>بروزرسانی</th>
            <th>تاریخ بروزرسانی</th>
            <th>مشاهده</th>
        </tr>
        </thead>
        <tbody>
        <tfoot>
        <tr>
            <th>مشاهده جزئیات</th>
            <th>نام هتل</th>
            <th>بروزرسانی</th>
            <th>تاریخ بروزرسانی</th>
            <th>مشاهده</th>
        </tr>
        </tfoot>
    </table>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        function format(d) {
            // `d` is the original data object for the row
            var show;
            d.start_time.forEach(function (item) {
                if (show == undefined) {
                    show = '<div class="container"><div class="row"><div class="col-3"><div>' + item + '</div>';
                } else {
                    show = show + '<div>' + item + '</div>';
                }
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.end_time.forEach(function (item) {
                show = show + '<div>' + item + '</div>';
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.id.forEach(function (item) {
                show = show + '<div><a href="https://hihotel.org/group/edit/' + item + '" >edit</a></div>';
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.id.forEach(function (item) {
                show = show + '<div><a href="https://hihotel.org/group/copy/' + item + '" >copy</a></div>';
                return show;
            });
            show = show + '</div><div class="col-2">';
            d.id.forEach(function (item) {
                show = show + '<div><a href="https://hihotel.org/group/del/' + item + '" >del</a></div>';
                return show;
            });
            show = show + '</div></div></div>';
            return show;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "<?php echo e(route('hotel.show')); ?>",
                    method: 'POST'
                },
                columns: [
                    {
                        className: 'details-control',
                        orderable: false,
                        data: 'more',
                        defaultContent: '+'
                    },
                    {data: 'name', name: 'name'},
                    {data: 'admin_updated', name: 'admin_updated'},
                    {data: 'jalali_update', name: 'jalali_update'},
                    {data: 'link_room', name: 'link_room'},
                ]
            });
            $('.data-table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/hotel/show.blade.php ENDPATH**/ ?>