<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <form action="<?php echo e(route('hoteltype.updated')); ?>" method="POST" style="">
        <?php echo csrf_field(); ?>
        <?php echo method_field('put'); ?>
        <h1 class="titr">وارد کردن اطلاعات نوع تخت</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نوع تخت</label>
                <input type="text" class="form-control" name="name" required value="<?php echo e($bedtype->name); ?>">
                <input type="hidden" name="id" value="<?php echo e($bedtype->id); ?>">
            </div>
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/bedtype/update.blade.php ENDPATH**/ ?>