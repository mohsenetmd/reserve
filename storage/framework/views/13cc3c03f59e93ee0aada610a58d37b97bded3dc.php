<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <form action="<?php echo e(route('hotel.save')); ?>" method="POST" style="">
        <?php echo csrf_field(); ?>
        <?php echo method_field('put'); ?>
        <h1 class="titr">وارد کردن اطلاعات هتل</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>نام هتل</label>
                <input type="text" class="form-control" name="name" required value="<?php echo e($hotel->name); ?>">
            </div>
            <div class="form-group col-lg-4">
                <label>شروع قیمت</label>
                <input type="text" class="form-control" name="start_price" required value="<?php echo e($hotel->start_price); ?>">
            </div>
            <div class="form-group col-lg-4">
                <label>شهر</label>
                <select class="city_search form-control" name="city_id" value="26">
                    <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($item->id); ?>" <?php if($item->id == $hotel->city_id): ?> selected <?php endif; ?> ><?php echo e($item->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label>ستاره</label>
                <select class="star_search form-control" name="hotel_star_id" value="5">
                    <?php $__currentLoopData = $hotelstar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($item->id); ?>" <?php if($item->id == $hotel->hotel_star_id): ?> selected <?php endif; ?> ><?php echo e($item->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label>نوع هتل</label>
                <select class="type_search form-control" name="hotel_type_id">
                    <?php $__currentLoopData = $hoteltype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($item->id); ?>" <?php if($item->id == $hotel->hotel_type_id): ?> selected <?php endif; ?> ><?php echo e($item->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <input type="hidden" name="admin_created" value="<?php echo e($hotel->admin_created); ?>">
            <input type="hidden" name="admin_updated" value="<?php echo e(Auth::user()->name); ?>">
            <input type="hidden" name="id" value="<?php echo e($hotel->id); ?>">
            <input type="hidden" name="wp_post_id" value="<?php echo e($hotel->wp_post_id); ?>">
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.city_search').select2();
            $('.star_search').select2();
            $('.type_search').select2();
        });
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/hotel/update.blade.php ENDPATH**/ ?>