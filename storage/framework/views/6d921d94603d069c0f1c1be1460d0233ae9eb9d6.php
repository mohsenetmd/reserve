<?php $__env->startSection('content'); ?>
    <?php $i = 0; ?>
    <form method="post" action="/test">
        <?php echo csrf_field(); ?>
        <?php $__currentLoopData = $room; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <h5><?php echo e($item->name); ?>

            <?php if($rooms[$i]->breakfast == 1): ?>
                صبحانه
            <?php endif; ?>
            <?php if($rooms[$i]->lunch == 1): ?>
                ناهار
            <?php endif; ?>
            <?php if($rooms[$i]->dinner == 1): ?>
                شام
            <?php endif; ?>
            </h5>
            <?php $__currentLoopData = $date[$i]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row">
                    <div class="col-lg-3">
                        <label>انتخاب نوع رزرو</label>
                        <select class="form-control" name="reserve_type_id[]">
                            <?php for($j=0;$j<count($reserve_type);$j++): ?>
                                <option value="<?php echo e($reserve_type[$j]->id); ?>"
                                        <?php if($item2->reserve_type_id == $reserve_type[$j]->id): ?>
                                        selected
                                    <?php endif; ?>
                                ><?php echo e($reserve_type[$j]->name); ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <input type="hidden" name="id[]" value="<?php echo e($item2->id); ?>">
                        <label>تاریخ شروع</label>
                        <?php if($item2->gr != 1): ?>
                        <input class="form-control" type="text" name="start[]" value="<?php echo e($item2->date_booking_start); ?>">
                         <?php else: ?>
                            <input class="form-control" type="hidden" name="start[]" value="<?php echo e($item2->date_booking_start); ?>">
                        <p><?php echo e($item2->date_booking_start); ?></p>
                         <?php endif; ?>
                    </div>
                    <div class="col-lg-3">
                        <label>تاریخ پایان</label>
                        <?php if($item2->gr != 1): ?>
                            <input class="form-control" type="text" name="end[]" value="<?php echo e($item2->date_booking_end); ?>">
                        <?php else: ?>
                            <input class="form-control" type="hidden" name="end[]" value="<?php echo e($item2->date_booking_end); ?>">
                            <p><?php echo e($item2->date_booking_end); ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-3">
                        <label>قیمت مصوب</label>
                        <input class="form-control" type="text" name="price[]" value="<?php echo e($item2->price); ?>">
                    </div>
                    <div class="col-lg-3">
                        <label>قیمت مسافر</label>
                        <input class="form-control" type="text" name="discount_price[]"
                               value="<?php echo e($item2->discount_price); ?>">
                    </div>
                    <div class="col-lg-3">
                        <label>قیمت هتل</label>
                        <input class="form-control" type="text" name="hotel_price[]" value="<?php echo e($item2->hotel_price); ?>">
                    </div>
                    <div class="col-lg-3">
                        <label>حذف</label>
                        <input class="form-control" type="checkbox" name="del[]" value="<?php echo e($item2->id); ?>">
                    </div>
                </div>
                <hr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <hr style="border-bottom: 2px solid black">
            <?php $i += 1; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <button type="submit">ذخیره</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/hotel/time.blade.php ENDPATH**/ ?>