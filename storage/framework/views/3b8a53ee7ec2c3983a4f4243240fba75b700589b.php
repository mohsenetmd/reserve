<?php $__env->startSection('content'); ?>
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form method="post" action="<?php echo e(route('date.save')); ?>">
                    <?php echo csrf_field(); ?>
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات تاریخ</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    <?php $__currentLoopData = $reservetype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد اتاق</label>
                                <input type="text" class="form-control" name="number" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت مصوب</label>
                                <input type="text" class="form-control" name="price" required >
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نرخ واریزی مسافر</label>
                                <input type="text" class="form-control" name="discount_price" v-on:keyup="discount()"
                                       required v-model="discount_price">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نرخ واریزی هتل</label>
                                <input type="text" class="form-control" name="hotel_price" required>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>

                            <input type="hidden" class="form-control" name="room_id" value="<?php echo e(request()->room_id); ?>">
                            <input type="hidden" class="form-control" name="admin_created" value="<?php echo e(Auth::user()->name); ?>">
                            <input type="hidden" class="form-control" name="admin_updated" value="<?php echo e(Auth::user()->name); ?>">
                        </div>
                    </div>
                    <div id="app">
                        <date-component></date-component>
                    </div>
                    <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.reservetype_search').select2();
            $('.foodtype_search').select2();
        });
    </script>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/date/insert.blade.php ENDPATH**/ ?>