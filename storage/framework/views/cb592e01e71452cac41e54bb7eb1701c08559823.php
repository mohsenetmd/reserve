<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <h1 style="font-size: 24px;display: inline-block;margin-left: 20px;">لیست انواع تخت ها</h1>
    <a class="btn btn-primary" href="<?php echo e(route('bedtype.insert')); ?>">افزودن</a>
    <hr>
    <table class="table table-bordered data-table">
        <thead>
        <tr>
            <th>No</th>
            <th>نوع تخت</th>
            <th>تارخ ایجاد</th>
            <th>تاریخ بروزرسانی</th>
            <th>مشاهده</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "<?php echo e(route('bedtype.show')); ?>",
                    method: 'POST'
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'jalali_create', name: 'jalali_create'},
                    {data: 'jalali_update', name: 'jalali_update'},
                    {data: 'link_bedtype', name: 'link_bedtype', orderable: false, searchable: false},
                ]
            });
            console.log(table);
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laracast\reserve\resources\views/bedtype/show.blade.php ENDPATH**/ ?>