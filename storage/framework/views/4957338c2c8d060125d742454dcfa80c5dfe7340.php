<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" href="bootstrap.css">
    <style>
	@media (max-width: 1024px) {
		.insert_shomare_mobile {
			width:80vw !important;
		}
		form{
			width:70vw !important;
		}
		.btn{
			width:150px !important;
		}
	}
		.btn{
			width: 350px;
		}
        .slant-box {
            content: '';
            display: inline-block;
            height: 523px;
            transform: rotate(41deg);
            width: 3px;
            background: white;
        }

        .insert_number {
            background-color: rgb(203, 42, 48);
            width: 100vw;
            height: 100vh;
            background-image: url(https://hihotel.ir/wp-content/uploads/2020/06/backmobilefinal.jpg);
        }

        .insert_shomare_mobile {
            text-align: center;
            width: 400px;
            height: 200px;
            background-color: white;
            padding: 20px;
            display: block;
            margin: auto;
            margin-top: calc(50vh - 100px);
            border-radius: 20px;
            box-shadow: 14px 19px 20px 12px #1d212452;
        }

        .text_mobile {
            height: 200px;
            margin: auto;
            margin-top: calc(50vh - 80px);
            width: 300px;
            color: white;
            text-align: center;
            font-size: 18px;
            font-weight: bold;
        }

        html,
        body {
            padding: 0;
            margin: 0;
            font-family: IRANSans, sans-serif !important;
            direction: rtl;
            text-align: right;
        }

        #triangle-down {
            width: 0;
            height: 0;
            border-left: 120px solid transparent;
            border-right: 30px solid transparent;
            border-top: 30px solid white;
            display: block;
            margin: auto;
        }
    </style>
    <div class="container-fluid insert_number">
        <div class="row">
            <div class="col-lg-6">
                <div class="insert_shomare_mobile wow fadeInUp">
                    <div class="input-group " style="margin-top: 20px;margin-bottom: 20px;">
                        <div class="input-group-prepend">
                        <span class="input-group-text"
                              style="border-radius: 5px !important;background-color: rgb(185,38,46);"><svg
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 503.604 503.604"
                                    style="enable-background:new 0 0 503.604 503.604;" xml:space="preserve" width="20px"
                                    height="20px" class=""><g><g>
	<g>
		<path d="M337.324,0H167.192c-28.924,0-53.5,23.584-53.5,52.5v398.664c0,28.916,24.056,52.44,52.98,52.44l170.412-0.184    c28.92,0,52.58-23.528,52.58-52.448l0.248-398.5C389.908,23.452,366.364,0,337.324,0z M227.68,31.476h49.36    c4.336,0,7.868,3.52,7.868,7.868c0,4.348-3.532,7.868-7.868,7.868h-49.36c-4.348,0-7.868-3.52-7.868-7.868    C219.812,34.996,223.332,31.476,227.68,31.476z M198.02,33.98c2.916-2.912,8.224-2.952,11.136,0c1.46,1.456,2.324,3.5,2.324,5.588    c0,2.048-0.864,4.088-2.324,5.548c-1.452,1.46-3.504,2.32-5.548,2.32c-2.084,0-4.088-0.86-5.588-2.32    c-1.452-1.456-2.28-3.5-2.28-5.548C195.736,37.48,196.568,35.436,198.02,33.98z M250.772,488.008    c-12.984,0-23.544-10.568-23.544-23.548c0-12.984,10.56-23.548,23.544-23.548s23.544,10.564,23.544,23.548    C274.316,477.44,263.752,488.008,250.772,488.008z M365.488,424.908H141.232V74.756h224.256V424.908z"
              data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
	</g>
</g></g> </svg></span>
                        </div>
                        <form method="post" name="form" action="<?php echo e(route('mobile.insertsms')); ?>" onsubmit="return phonenumber(document.form.mobile);">
                            <?php echo csrf_field(); ?>
                            <input type="number" class="form-control" name="mobile" placeholder="شماره همراه">
                            <input type="hidden" name="room" value="<?php echo e(request()->room); ?>">
                            <input type="hidden" name="total" value="<?php echo e(request()->total); ?>">
                            <input type="hidden" name="date_booking_start" value="<?php echo e(request()->date_booking_start); ?>">
                            <input type="hidden" name="date_booking_end" value="<?php echo e(request()->date_booking_end); ?>">
                            <input class="input_total_bed_price" type="hidden" name="total_bed_price"
                                   value="<?php echo e(request()->total_bed_price); ?>">
                            <input name="online" type="hidden" value="<?php echo e(request()->online); ?>">
                            <input name="noReserve" type="hidden" value="<?php echo e(request()->noReserve); ?>">
                            <input name="inquiry" type="hidden" value="<?php echo e(request()->inquiry); ?>">
							<input name="tedad" type="hidden" value="<?php echo e(request()->tedad); ?>">
                            <button type="submit" class="btn btn-danger"
                                    style="background-color: rgb(185,38,46);margin-top: 20px;">
                                ثبت
                            </button>
                        </form>
                    </div>
                </div>
                <div id="triangle-down"></div>
            </div>
            <div class="slant-box"></div>
            <div class="col-lg-3" style="flex: 3 0 25%;max-width: 50%;">
                <div class="text_mobile"
                     style="width: 350px;color: white;text-align: justify;font-size: 15px;font-weight: bold;">
                    برای استفاده از خدمات رزرواسیون "های هتل" و پیگیری رزرو لطفا شماره موبایل خود را به صورت صحیح وارد
                    کنید.
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
function phonenumber(inputtxt)
{
  var phoneno = /^([0]{1})([9]{1})([0-9]{1})([0-9]{8})$/;
  var phoneno2 = /^\d{11}$/
  if(inputtxt.value.match(phoneno) && inputtxt.value.match(phoneno2))
  {
      return true;
  }
  else
  {
     swal ( "خطا" ,  "فرمت شماره تلفن ورودی اشتباه می باشد" ,  "error" , {
  button: false , timer: 2000});
     return false;
  }
  }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/mobile/insert.blade.php ENDPATH**/ ?>