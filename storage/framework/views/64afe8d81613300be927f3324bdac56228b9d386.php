<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <form action="<?php echo e(route('user.changepassword')); ?>" method="POST" style="">
        <?php echo csrf_field(); ?>
        <?php echo method_field('put'); ?>
        <h1 class="titr">وارد کردن مشخصات کاربری</h1>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                <label>رمز عبور</label>
                <input v-validate="'required'" type="password" class="form-control" name="password" required>
            </div>
            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
        </div>
    </form>
    <script>

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/user/change.blade.php ENDPATH**/ ?>