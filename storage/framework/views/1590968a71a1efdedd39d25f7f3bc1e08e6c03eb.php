<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                نام و نام خانوادگی
            </div>
            <div class="col-md-3">
                <?php echo e($user->family); ?>

            </div>
            <div class="col-md-3">
                شماره موبایل
            </div>
            <div class="col-md-3">
                <?php echo e($user->mobile); ?>

            </div>
            <div class="col-md-3">
                ایمیل
            </div>
            <div class="col-md-3">
                <?php echo e($user->email); ?>

            </div>
            <div>
            <a href="<?php echo e(route('user.update')); ?>" class="btn btn-primary">بروز رسانی اطلاعات کاربران</a>
            <a href="<?php echo e(route('user.changepassword')); ?>" class="btn btn-primary">تغییر پسورد</a>
            <a href="<?php echo e(route('user.logout')); ?>" class="btn btn-primary">خروج</a>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/user/show.blade.php ENDPATH**/ ?>