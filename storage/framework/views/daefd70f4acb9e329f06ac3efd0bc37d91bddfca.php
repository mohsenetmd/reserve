
<?php $__env->startSection('content'); ?>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <form method="get" action="<?php echo e(route('group.editselectdate')); ?>">
        <label>انتخاب هتل</label>
        <select class="js-example-basic-multiple" name="id" style="width:100%;height:300px;">
            <?php for($i=0;$i<count($hotel);$i++): ?>
                <option value="<?php echo e($hotel[$i]->id); ?>"><?php echo e($hotel[$i]->name); ?></option>
            <?php endfor; ?>
        </select>
        <button type="submit">ارسال</button>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/group/selectdate.blade.php ENDPATH**/ ?>