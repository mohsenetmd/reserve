<?php $__env->startSection('content'); ?>
    <form method="post" action="<?php echo e(route('mobile.acceptsms')); ?>">
        <?php echo csrf_field(); ?>
		<label>کد تایید را وارد کنید. </label>
        <input type="number" id="accept" name="accept" required>
        <input type="hidden" value="<?php echo e(request()->mobile); ?>" name="mobile" required>
        <input type="hidden" name="room" value="<?php echo e(request()->room); ?>">
        <input type="hidden" name="total" value="<?php echo e(request()->total); ?>">
        <input type="hidden" name="date_booking_start" value="<?php echo e(request()->date_booking_start); ?>">
        <input type="hidden" name="date_booking_end" value="<?php echo e(request()->date_booking_end); ?>">
        <input class="input_tedad" type="hidden" name="tedad" value="<?php echo e(request()->tedad); ?>">
        <input class="input_total_bed_price" type="hidden" name="total_bed_price" value="<?php echo e(request()->total_bed_price); ?>">
        <button type="submit">send</button>
    </form>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/mobile/insertsms.blade.php ENDPATH**/ ?>