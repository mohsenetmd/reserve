<?php $__env->startSection('content'); ?>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <?php if($errors->any()): ?>
        <ul class="alert alert-danger">
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li> <?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endif; ?>
    <style>
        .required:after {
            content: " *";
            color: red;
        }
    </style>
    <form method="post" action="<?php echo e(route('discount.save')); ?>">
        <?php echo csrf_field(); ?>
        <div class="row">
            <div class="col-lg-12">
                <label class="required">انتخاب هتل ها</label>
                <select class="js-example-basic-multiple" name="hotel_id[]" multiple="multiple"
                        style="width:100%;height:300px;">
                    <?php for($i=0;$i<count($hotel);$i++): ?>
                        <option value="<?php echo e($hotel[$i]->id); ?>"><?php echo e($hotel[$i]->name); ?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="col-lg-4">
                <label class="required">کد تخفیف</label>
                <input class="form-control" type="text" name="discount_code" required>
            </div>
            <div class="col-lg-4">
                <label class="required">تعداد استفاده</label>
                <input class="form-control" type="text" name="number_use" required>
            </div>
            <div class="col-lg-4">
                <label class="required">درصد تخفیف</label>
                <input class="form-control" type="text" name="percent" required>
            </div>
            <div class="col-lg-4">
                <label class="required">مبلغ تخفیف</label>
                <input class="form-control" type="text" name="price" required>
            </div>
            <div class="col-lg-10">
                <label>توضیحات</label>
                <textarea class="form-control" name="detail" placeholder="توضیحات خود را وارد کنید"></textarea>
            </div>
        </div>
        <div id="app">
            <date-component></date-component>
        </div>
        <button class="btn btn-primary" type="submit">ارسال</button>
    </form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/discount/insert.blade.php ENDPATH**/ ?>