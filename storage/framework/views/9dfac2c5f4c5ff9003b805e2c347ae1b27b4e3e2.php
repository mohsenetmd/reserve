<?php $__env->startSection('content'); ?>
<div id="formWrapper">
    <div class="cover" style="">
        <div class="num-step active"> ورود اطلاعات</div>
        <div class="num-step deactive-mobile">استعلام</div>
        <div class="num-step deactive-mobile">پرداخت</div>
    </div>

    <div id="form" class="mobilecover" style="direction:rtl;">
        <div class="row show-mobile-information">
            <div class="hotel-detail-first col-6" style="text-align: center">
                <img src="https://hihotel.ir/panel/images/1720hotel-ghasr-talaee-double-atrium-2-1024x683.gif "
                     width="150" height="150" style="border-radius:10px;">
                <div class="hotelname"><?php echo e($hotel_name); ?></div>
                <div class="hotel"><?php echo e($hotel_star_name); ?></div>
				<div class="hotel">تعداد اتاق: <?php echo e($tedad); ?></div>
                <div class="hotel">نام اتاق: <?php echo e($room_type_name); ?></div>
                <div class="hotel">نوع تخت : <?php echo e($bed_type_name); ?></div>
            </div>
            <div class="hotel-detail-two col-6" style="text-align: center">
                <div class="hotelname">کد رزرو: <?php echo e($voucher); ?></div>
                <div class="hotelname">تاریخ ورود : <?php echo e($start); ?></div>
                <div class="hotelname">تاریخ خروج : <?php echo e($end); ?></div>
                <div class="hotel">تعداد نفرات: <?php echo e($roomarray->person_number); ?> نفر</div>
                <div class="hotel"><?php if($roomarray->breakfast == 1): ?>
									صبحانه
									<?php endif; ?>
									<?php if($roomarray->dinner == 1): ?>
									ناهار
									<?php endif; ?>
									<?php if($roomarray->lunch == 1): ?>
									شام
									<?php endif; ?>
				</div>
                <div class="hotel">مجموع : <?php echo e(number_format($total->price * $tedad)); ?> تومان</div>
                <div class="hotel">تخفیف : <?php echo e(number_format((intval($total->price) - intval($total->discount_price))*$tedad)); ?> تومان</div>
                <div class="hotelname" style="color:red;font-size:18px;">مبلغ نهایی: <?php echo e(number_format($total->discount_price*$tedad)); ?></div>
            </div>
        </div>
     <div class="row">
            <div class="fard-form">
                <form action="https://hihotel.org/fard/save" method="POST" style="">
                    <?php echo csrf_field(); ?>
                    <div class="information">اطلاعات رزرو کننده</div>
                    <div class="row">
                        <div class="form-item">
                            <p class="formLabel">نام و نام خانوادگی</p>
                            <p class="mobile-label">نام و نام خانوادگی</p>
                            <input type="text" name="family" id="email" class="form-style" autocomplete="on" required>
                        </div>
                        <div class="form-item">
                            <p class="formLabel">ایمیل</p>
                            <p class="mobile-label">ایمیل</p>
                            <input type="email" name="name" id="mobile" class="form-style" autocomplete="on">
                        </div>
                    </div>
                    <div class="row">
                            <input type="hidden" name="melli_number" id="email" class="form-style" autocomplete="on"
                                   required value="000000000">
                        <div class="form-item">
                            <p class="" style="margin-bottom: 5px;">جنسیت</p>
                            <select name="sex" class="form-style" style="width: 100%;
    display: block;
    height: 44px;
    padding: 5px 5%;
    border: 1px solid #ccc;
    -moz-border-radius: 27px;
    -webkit-border-radius: 27px;
    border-radius: 27px;
    -moz-background-clip: padding;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    background-color: #fff;
    font-size: 105%;
    letter-spacing: .8px;">
                                <option value="0">مرد</option>
                                <option value="1">زن</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-item">
                        <input type="hidden" name="save_reserve_id" value="<?php echo e($save_reserve_id); ?>">
                        <input type="submit" class="login pull-right" value="تایید">
                        <div class="clear-fix"></div>
                    </div>
                </form>
            </div>
            <div class="hotel-detail-first desktop">
                <img src="<?php echo e($pic); ?>" width="150" height="150" style="border-radius:10px;">
                <div class="hotelname"><?php echo e($hotel_name); ?></div>
                <div class="hotel"><?php echo e($hotel_star_name); ?></div>
				<div class="hotel">تعداد اتاق: <?php echo e($tedad); ?></div>
                <div class="hotel">نام اتاق: <?php echo e($room_type_name); ?></div>
                <div class="hotel">نوع تخت : <?php echo e($bed_type_name); ?></div>
            </div>
            <div class="hotel-detail-two desktop">
                <div class="hotelname">کد رزرو: <?php echo e($voucher); ?></div>
                <div class="hotelname">تاریخ ورود : <?php echo e($start); ?></div>
                <div class="hotelname">تاریخ خروج : <?php echo e($end); ?></div>
                <div class="hotel">تعداد نفرات: <?php echo e($roomarray->person_number); ?> نفر</div>
                <div class="hotel"><?php if($roomarray->breakfast == 1): ?>
									صبحانه
									<?php endif; ?>
									<?php if($roomarray->dinner == 1): ?>
									ناهار
									<?php endif; ?>
									<?php if($roomarray->lunch == 1): ?>
									شام
									<?php endif; ?>
				</div>
                <div class="hotel">مجموع : <?php echo e(number_format($total->price * $tedad)); ?> تومان</div>
                <div class="hotel">تخفیف : <?php echo e(number_format((intval($total->price) - intval($total->discount_price))*$tedad)); ?> تومان</div>
                <div class="hotelname">مبلغ نهایی: <?php echo e(number_format($total->discount_price * $tedad)); ?></div>
            </div>
        </div>
    </div>
</div>
<style>
  body {
        background: url(https://hihotel.ir/wp-content/uploads/2020/06/backmobilefinal.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    @media (max-width: 1024px) {
        .num-step {
            background-color: #fff;
            width: 90%;
            padding-right: 10px;
            border-radius: 10px;
            height: 50px;
            padding-top: 10px;
            display: block;
            margin: auto;
        }

        .deactive-mobile {
            display: none !important;
        }

        .fard-form {
            width: 90%;
        }

        .information {
            display: block;
            margin: auto;
        }

        div.form-item {
            width: 100%;
        }

        input.form-style {
            width: 100% !important;
        }

        .formLabel {
            display: none;
        }

        div#form {
            left: 0px !important;
            right: 0px !important;
            top: 280px;
        }

        div.form-item {
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .mobile-label {
            margin-bottom: 5px;
        }
        .show-mobile-information{
        }
        .desktop{
            display: none;
        }
        .pull-right{
            width: 50% !important;
            display: block;
            margin: auto;
            float: none !important;
        }
        .information{
            margin-bottom: 5px !important;
			margin-top:20px;
        }
        .form-item{
            margin-right: 0px !important;
        }
        .fard-form{
            margin: auto !important;
        }
        .cover{
            margin-top: 10px;
            text-align: center;
        }
    }

    @media (min-width: 1025px) {
        .num-step {
            background-color: #fff;
            width: 28%;
            padding-right: 10px;
            border-radius: 10px;
            height: 50px;
            padding-top: 10px;
            display: inline-block;
        }
        .cover{
            margin-top: 50px;text-align: center;
        }
        .form-item {
            width: 45%;
        }

        .fard-form {
            width: 60%;
            border-left: 5px dotted;
        }

        .hotel-detail-first {
            background-color: #fff;
            width: 15%;
            position: absolute;
            right: 65%;
        }

        .hotel-detail-two {
            background-color: #fff;
            width: 15%;
            position: absolute;
            right: 80%;
        }

        div#form {
            margin-left: -180px !important;
            top: 320px;
        }

        div.form-item {
            margin-bottom: 20px;
            margin-top: 20px;
        }

        .mobile-label {
            display: none;
        }
        .show-mobile-information{
            display: none;
        }
    }

    .active {
        background-color: #e61b3e;
        color: #fff;
        font-weight: 800;
    }

    .hotelname {
        font-size: 12px;
        font-weight: 800;
        margin-top: 14px;
        margin-right: 5px;
    }

    .hotel {
        font-size: 12px;
        margin-top: 8px;
    }

    .information {
        margin-bottom: 45px;
        width: 150px;
        background-color: #f46694;
        font-weight: 800;
        color: white;
        text-align: center;
        height: 35px;
        padding-top: 8px;
        border-radius: 10px;
        font-size: 14px;
    }

    #formWrapper {
        background: rgba(0, 0, 0, .2);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        transition: all .3s ease;
    }

    .darken-bg {
        background: rgba(0, 0, 0, .5) !important;
        transition: all .3s ease;
    }
    
    div#form {
        position: absolute;
        width: 90%;
        height: 320px;
        height: auto;
        background-color: #fff;
        margin: auto;
        border-radius: 5px;
        padding: 20px;
        left: 220px;
        margin-top: -200px;
    }

    div.form-item {
        position: relative;
        display: block;
        margin-right: 20px;
    }

    input {
        transition: all .2s ease;
    }

    input.form-style {
        color: #8a8a8a;
        display: block;
        width: 90%;
        height: 44px;
        padding: 5px 5%;
        border: 1px solid #ccc;
        -moz-border-radius: 27px;
        -webkit-border-radius: 27px;
        border-radius: 27px;
        -moz-background-clip: padding;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        background-color: #fff;
        font-family: 'HelveticaNeue', 'Arial', sans-serif;
        font-size: 105%;
        letter-spacing: .8px;
    }

    div.form-item .form-style:focus {
        outline: none;
        border: 1px solid #5d5d5d;
        color: #5d5d5d;
    }

    div.form-item p.formLabel {
        position: absolute;
        right: 5%;
        top: 12px;
        transition: all .4s ease;
        color: #bbbbbbad;
        font-size: 13px;
    }

    .formTop {
        top: -22px !important;
        left: 0px;
        background-color: #fff;
        padding: 0 5px;
        font-size: 14px;
        color: #5d5d5d !important;
    }

    .formStatus {
        color: #f46694 !important;
    }

    input[type="submit"].login {
        float: right;
        width: 112px;
        height: 37px;
        -moz-border-radius: 19px;
        -webkit-border-radius: 19px;
        border-radius: 19px;
        -moz-background-clip: padding;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        background-color: rgb(230, 27, 62);
        border: 1px solid rgb(230, 27, 62);
        border: none;
        color: #fff;
        font-weight: bold;
    }

    input[type="submit"].login:hover {
        background-color: #fff;
        border: 1px solid #f46694;
        color: #f46694;
        cursor: pointer;
    }

    input[type="submit"].login:focus {
        outline: none;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    $(document).ready(function(){
    var formInputs = $('input[type="email"],input[type="tel"],input[type="text"]');
    formInputs.focus(function() {
       $(this).parent().children('p.formLabel').addClass('formTop');
       $('div#formWrapper').addClass('darken-bg');
    });
    formInputs.focusout(function() {
        if ($.trim($(this).val()).length == 0){
        $(this).parent().children('p.formLabel').removeClass('formTop');
        }
        $('div#formWrapper').removeClass('darken-bg');
    });
    $('p.formLabel').click(function(){
         $(this).parent().children('.form-style').focus();
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/fard/insert.blade.php ENDPATH**/ ?>