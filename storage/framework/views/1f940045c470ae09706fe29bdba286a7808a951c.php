<?php $__env->startSection('content'); ?>
    <div id="app2" class="container form-otah">
        <div class="row" style="">
            <div class="col-lg-9">
                <hr>
                <form enctype="multipart/form-data" method="post" action="<?php echo e(route('room.save')); ?>">
                    <?php echo csrf_field(); ?>
                    <div>
                        <h1 class="titr">وارد کردن اطلاعات اتاق</h1>
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>نوع رزرو</label>
                                <select class="reservetype_search form-control" name="reserve_type_id">
                                    <?php $__currentLoopData = $reservetype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع اتاق</label>
                                <select class="roomtype_search form-control" name="room_type_id">
                                    <?php $__currentLoopData = $roomtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="input-group">
                              <label for="imageInput">عکس را وارد کنید</label>
                                <input class="form-control-file" data-preview="#preview" name="pic" type="file" id="imageInput">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع تخت</label>
                                <select class="bedtype_search form-control" name="bed_type_id">
                                    <?php $__currentLoopData = $bedtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد افراد</label>
                                <input type="text" class="form-control" name="person_number" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت</label>
                                <input type="text" class="form-control" name="price" required v-model="price">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>قیمت با تخفیف</label>
                                <input type="text" class="form-control" name="discount_price" v-on:keyup="discount()"
                                       required v-model="discount_price">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>درصد تخفیف</label>
                                <input type="text" class="form-control" disabled v-model="discounted">
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تخت اضافه</label>
                                <select name="more_bed" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_number">
                                <small class="form-text text-muted">تعداد تختی که قابلیت اضافه کردن را دارد انتخاب
                                    کنید</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه تخت اضافه</label>
                                <input type="text" class="form-control" name="bed_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر تخت را وارد کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>کودک</label>
                                <select name="child" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>تعداد کودک</label>
                                <input type="text" class="form-control" name="child_number">
                                <small class="form-text text-muted">تعداد کودکی که قابلیت اضافه شدن را دارد مشخص
                                    کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>هزینه کودک</label>
                                <input type="text" class="form-control" name="child_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای هر کودک را وارد
                                    کنید.</small>
                            </div>
                            <div class="col-lg-12">
                                <hr>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>صبحانه</label>
                                <input type="text" class="form-control" name="breakfast_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای صبحانه را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>ناهار</label>
                                <input type="text" class="form-control" name="lunch_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای ناهار را وارد کنید.</small>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>شام</label>
                                <input type="text" class="form-control" name="dinner_price">
                                <small class="form-text text-muted">هزینه اضافه شده به ازای شام را وارد کنید.</small>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>صبحانه رایگان</label>
                                <select name="breakfast" class="form-control">
                                    <option value="1">دارد</option>
                                    <option value="0">ندارد</option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ناهار</label>
                                <select name="lunch" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>شام</label>
                                <select name="dinner" class="form-control">
                                    <option value="0">ندارد</option>
                                    <option value="1">دارد</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>نوع غذا</label>
                                <select class="foodtype_search form-control" name="food_type_id">
                                    <?php $__currentLoopData = $foodtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>قیمت نوع غذا</label>
                                <input type="text" class="form-control" name="price_food_type">
                            </div>
                            <div class="form-check col-lg-4">
                                <label>ترتیب اتاق</label>
                                <input type="text" class="form-control" name="sort" required>
                            </div>
                            <div class="form-check col-lg-4">
                                <label>شماره اتاق</label>
                                <input type="text" class="form-control" name="room_number">
                            </div>
                            <div class="form-check col-lg-12">
                                <label>در صورت نبود اطلاعات در یک تاریخ این اطلاعات جایگزین شود؟</label>
                                <select class="form-control" name="sample_data">
                                    <option value="1">بله</option>
                                    <option value="0">خیر</option>
                                </select>
                            </div>
                            <div class="form-check col-lg-12" style="margin-top: 20px">
                                <label class="form-check-label" for="defaultCheck1">
                                    راهنما مطالعه شده است؟
                                </label>
                            </div>
                            <input type="hidden" class="form-control" name="hotel_id" value="<?php echo e(request()->hotel_id); ?>">
                            <input type="hidden" class="form-control" name="admin_created" value="<?php echo e(Auth::user()->name); ?>">
                            <input type="hidden" class="form-control" name="admin_updated" value="<?php echo e(Auth::user()->name); ?>">
                            <button class="btn btn-danger mb-5 mr-2" value="submit" type="submit">ارسال</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        let app2 = new Vue({
            el: '#app2',
            data: {
                price: '',
                discount_price: '',
                discounted: ''
            },
            methods: {
                discount() {
                    this.discounted = ((1 - (this.discount_price / this.price)) * 100).toFixed(0);
                }
            }
        });
        $(document).ready(function () {
            $('.reservetype_search').select2();
            $('.foodtype_search').select2();
            $('.bedtype_search').select2();
            $('.roomtype_search').select2();
        });
        $('#lfm').filemanager('image');
    </script>
    <?php if($errors->any()): ?>
        <div class="alert alert-danger">
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Laracast\reserve\resources\views/room/insert.blade.php ENDPATH**/ ?>