
<?php $__env->startSection('content'); ?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<form method="post" action="<?php echo e(route('group.save')); ?>">
    <?php echo csrf_field(); ?>
    <label>انتخاب هتل ها</label>
    <select class="js-example-basic-multiple" name="id[]" multiple="multiple" style="width:100%;height:300px;">
        <?php for($i=0;$i<count($hotel);$i++): ?>
        <option value="<?php echo e($hotel[$i]->id); ?>"><?php echo e($hotel[$i]->name); ?></option>
        <?php endfor; ?>
    </select>
    <label>نوع رزرو</label>
    <select name="reserve_type_id">
        <?php for($i=0;$i<count($reserve_type);$i++): ?>
            <option value="<?php echo e($reserve_type[$i]->id); ?>"><?php echo e($reserve_type[$i]->name); ?></option>
        <?php endfor; ?>
    </select>
    <label> درصد تخفیف واریزی مسافر</label>
    <input type="number" name="percent">
    <label> درصد تخفیف واریزی هتل</label>
    <input type="number" name="percent_hotel">
    <div id="app">
        <date-component></date-component>
    </div>
    <button type="submit">send</button>
</form>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/group/select.blade.php ENDPATH**/ ?>