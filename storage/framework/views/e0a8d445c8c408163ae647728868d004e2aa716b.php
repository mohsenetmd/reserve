<?php $__env->startSection('content'); ?>
    <form method="post" action="<?php echo e(route('savereserve.update')); ?>">
        <?php echo csrf_field(); ?>
        <?php echo method_field('put'); ?>
        <div><?php echo e($hotel); ?></div>
        <div><?php echo e($room); ?></div>
        <div><?php echo e($totalprice); ?></div>
        <div><?php echo e($data[0]->date_booking_start); ?></div>
        <div><?php echo e($data[0]->date_booking_end); ?></div>
		<div>تعداد اتاق<?php echo e($data[0]->tedad); ?></div>
        <div class="form-group col-lg-4">
            <label>جواب استعلام</label>
            <select class="reservetype_search form-control" name="condition">
                    <option value="2"
                            <?php if($data[0]->condition == 2): ?>
                                selected
                            <?php endif; ?>
                    >عدم تایید استعلام</option>
                    <option
                        <?php if($data[0]->condition == 3): ?>
                        selected
                        <?php endif; ?>
                        value="3">تایید استعلام</option>
            </select>
            <input type="hidden" value="<?php echo e($update); ?>" name="admin_updated">
            <input type="hidden" value="<?php echo e($data[0]->id); ?>" name="id">
        </div>
        <button type="submit">ارسال</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/savereserve/updatereserve.blade.php ENDPATH**/ ?>