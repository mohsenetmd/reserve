<?php $__env->startSection('content'); ?>
    <div id="formWrapper">

        <div class="cover" style="">
            <div class="num-step deactive-mobile"> ورود اطلاعات</div>
            <div class="num-step active">استعلام</div>
            <div class="num-step deactive-mobile">پرداخت</div>
        </div>
        <?php if($condition == 3): ?>
            <?php if(($now - $save_reserve->time)<1800): ?>
                <div id="countTime" class="counter"><span class="view_time_min" style="font-size:24px;"></span>
                    <span style="font-size:24px;">:</span>
                    <span class="view_time_sec" style="font-size:24px;"></span>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php if($condition != 3): ?>
            <div id="countTime" class="counter">
                <span class="view_time_min" style="font-size:24px;"></span>
                <span style="font-size:24px;">:</span>
                <span class="view_time_sec" style="font-size:24px;"></span>
            </div>
            <div class="timer"></div>
    </div>
    <?php endif; ?>
    </div>
    <img src="https://hihotel.ir/wp-content/uploads/2020/06/timerpic.png" width="700" height="400" class="timercover">
    <span class="timertext1">کد رزرو: <?php echo e($save_reserve->voucher); ?> --------<span>  مبلغ نهایی :
        <span class="price">   <?php echo e($price); ?> </span>
            تومان </span></span>
    <?php if($condition == 3): ?>
        <?php if(($now - $save_reserve->time)<1800): ?>
            <div class="ajax_call timertext" style="list-style-type: none;
    text-align: center;
	margin-right:0px;">
                <div class="timing">
                    <form method="post" action="<?php echo e(route('pardakht.link')); ?>">
                        <?php echo csrf_field(); ?>
                        <input name="voucher" type="hidden" value="<?php echo e($save_reserve->voucher); ?>">
						<input name="save_reserve_id" type="hidden" value="<?php echo e($save_reserve->id); ?>">
                        <div class="timer" style="display: none;"><?php echo e($now - $save_reserve->time); ?></div>
                        <button type="submit" class="btn btn-success">پرداخت</button>
                    </form>
                </div>
            </div>
            <span class="timertext">کاربر گرامی استعلام شما تایید شد.</span>
            <span class="timertext">حداکثر مدت زمان برای پرداخت30 دقیقه می باشد</span>
            <span class="timertext">کد رزرو خود را تا انتهای سفر به همراه داشته باشید</span>
        <?php else: ?>
            <div class="ajax_call timertext" style="list-style-type: none;
    text-align: center;
    font-size: 24px;
	margin-right:0px;">
                زمان به پایان رسید.
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if($condition != 3): ?>
        <div class="ajax_call timertext" style="list-style-type: none;
    text-align: center;
    font-size: 24px;
	margin-right:0px;">
            منتظر استعلام بمانید
        </div>
        <span class="timertext show1">کاربر گرامی اطلاعات شما با موفقیت ثبت گردید</span>
        <span class="timertext show1">اطلاعات رزرو شما پس از تایید برای هتل مورد نظر ارسال خواهد شد</span>
        <span class="timertext show1">حداکثر مدت زمان انتظار استعلام 15 دقیقه</span>
        <span class="timertext show1">کد رزرو خود را تا انتهای سفر به همراه داشته باشید</span>
        <span class="timertext hide1">کاربر گرامی استعلام شما تایید شد.</span>
        <span class="timertext hide1">حداکثر مدت زمان برای پرداخت30 دقیقه می باشد</span>
        <span class="timertext hide1">کد رزرو خود را تا انتهای سفر به همراه داشته باشید</span>
        <?php endif; ?>
        </div>
        <style type="text/css">
            @media (max-width: 1024px) {
                .cover {
                    margin-top: 10px;
                    text-align: center;
                }

                .num-step {
                    background-color: #fff;
                    width: 90%;
                    padding-right: 10px;
                    border-radius: 10px;
                    height: 50px;
                    padding-top: 10px;
                    display: block;
                    margin: auto;
                }

                .deactive-mobile {
                    display: none !important;
                }

                .counttime {
                    font-size: 24px;
                    display: rtl;
                    direction: ltr;
                    text-align: center;
                    color: green;
                }

                .timercover {
                    width: 80vw;
                    display: block;
                    margin: auto;
                    margin-right: 10vw !important;
                    position: absolute;
                    z-index: -1;
                }

                .timertext1 {
                    display: block;
                    margin: auto !important;
                    margin-top: 130px !important;
                }

                .counter {
                    margin-right: 47vw !important;
                    margin-top: 80px !important;
                    font-size: 24px !important;
                }

                .timertext1 {
                    width: 70vw !important;
                    height: auto !important;
                }

                .timertext {
                    margin-right: 17vw !important;
                    width: 70vw;
                }
            }

            @media (min-width: 1025px) {
                .cover {
                    margin-top: 50px;
                    text-align: center;
                }

                .num-step {
                    background-color: #fff;
                    width: 28%;
                    padding-right: 10px;
                    border-radius: 10px;
                    height: 50px;
                    padding-top: 10px;
                    display: inline-block;
                }

                .timercover {
                    margin-top: 5%;
                    margin-right: 25%;
                    border-radius: 10px;
                    position: absolute;
                    z-index: -1;
                }
            }

            body {
                background: url(https://hihotel.ir/wp-content/uploads/2020/06/backmobilefinal.jpg) no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;

            }

            .counter {
                margin-right: 850px;
                margin-top: 182px;
                position: absolute;
                direction: ltr;
            }

            .timercover {
                margin-top: 5%;
                margin-right: 25%;
                border-radius: 10px;
                position: absolute;
                z-index: -1;
            }

            .timertext1 {
                margin-right: 29%;
                margin-top: 17%;
                margin-bottom: 15px;
                display: block;
                color: #fff;
                font-size: 18px;
                font-weight: 800;
                background-color: #cb2d47;
                width: 600px;
                text-align: center;
                border-radius: 5px;
                height: 30px;
            }

            .timertext {
                margin-right: 30%;
                margin-top: 5px;
                display: list-item;
                color: #000;
                font-size: 14px;
            }

            .num-step {
                background-color: #fff;
                width: 28%;
                padding-right: 10px;
                border-radius: 10px;
                height: 50px;
                padding-top: 10px;
                display: inline-block;
            }

            .active {
                background-color: #e61b3e;
                color: #fff;
                font-weight: 800;
            }
        </style>
<?php $__env->stopSection(); ?>

<?php if($condition != 3): ?>
<?php $__env->startSection('script'); ?>
    <script>
        var time =<?php echo e($now - $save_reserve->time); ?>;
        var intervalTimeEstelam = setInterval(timeestelam, 1000);
        var intervalCheckPayanEstelam = setInterval(timepayanestelam, 1000);

        function timeestelam() {
            time += 1;
            viewTimeMin = Math.floor(10 - time / 60);
            viewTimeSec = Math.floor(60 - time % 60);
            if (viewTimeSec == 60) {
                viewTimeSec = 0;
            }
            $('.view_time_min').html(viewTimeMin);
            $('.view_time_sec').html(viewTimeSec);
        };

        function timepayanestelam() {
            if (time > 600) {
                clearInterval(intervalTimeEstelam);
                clearInterval(intervalCheckPayanEstelam);
                $('.ajax_call').html('مدت زمان به پایان رسید.');
                $('.view_time_min').html(' ');
                $('.view_time_sec').html(' ');
            }
        }

        $(document).ready(function () {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $('.hide1').hide();
            if (time < 600) {
                var intervalAjax = setInterval(ajaxCall, 5000);

                function ajaxCall() {
                    if (time < 600) {
                        $.ajax({
                            /* the route pointing to the post function */
                            url: '<?php echo e(route('pardakht.ajax')); ?>',
                            type: 'POST',
                            /* send the csrf-token and the input to the controller */
                            data: {
                                _token: CSRF_TOKEN,
                                voucher: <?php echo e($save_reserve->voucher); ?>,
								save_reserve_id:<?php echo e($save_reserve->id); ?>

                            },
                            dataType: 'JSON',
                            /* remind that 'data' is the response of the AjaxController */
                            success: function (data) {
                                $('.ajax_call').html(data);
                                $('.view_time_min').html(' ');
                                $('.view_time_sec').html(' ');
                                $('.show1').hide();
                                $('.show2').hide();
                                $('.show3').hide();
                                $('.show4').hide();
                                $('.hide1').show();
                                clearInterval(intervalAjax);
                                clearInterval(intervalTimeEstelam);
                                clearInterval(intervalCheckPayanEstelam);
                                pardakht();
                            }
                        });
                    }
                };
            } else {
                $('.ajax_call').html('مدت زمان به پایان رسید.');
                $('.view_time_min').html(' ');
                $('.view_time_sec').html(' ');
                clearInterval(intervalAjax);
                clearInterval(intervalTimeEstelam);
                clearInterval(intervalCheckPayanEstelam);
            }
            var timePardakht = 0;

            function pardakht() {
                var intervalTimePardakht = setInterval(increseTimePardakht, 1000);

                function increseTimePardakht() {
                    timePardakht += 1;
                    viewTimeMin = Math.floor(30 - timePardakht / 60);
                    viewTimeSec = Math.floor(59 - timePardakht % 60);
                    $('.view_time_min').html(viewTimeMin);
                    $('.view_time_sec').html(viewTimeSec);
                    if (timePardakht > 1800) {
                        $('.view_time_min').html(' ');
                        $('.view_time_sec').html(' ');
                        $('.ajax_call').html('مدت زمان به پایان رسید.');
                        clearInterval(intervalTimePardakht)
                    }
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php endif; ?>
<?php if($condition == 3): ?>
<?php $__env->startSection('script'); ?>
    <script>
        var time = $('.timer').html();
        var timepay = setInterval(timepardakht, 1000);

        function timepardakht() {
            time = parseInt(time) + 1;
            viewTimeMin = Math.floor(30 - time / 60);
            viewTimeSec = Math.floor(60 - time % 60);
            if (viewTimeSec == 60) {
                viewTimeSec = 0;
            }
            $('.view_time_min').html(viewTimeMin);
            $('.view_time_sec').html(viewTimeSec);
            if (time > 1800) {
                clearInterval(timepay);
                $('.view_time_min').html(' ');
                $('.view_time_sec').html(' ');
                $('.ajax_call').html('زمان به پایان رسید');
            }
        }

        if (time > 1800) {
            $('.ajax_call').html('زمان به پایان رسید');
        }
    </script>
<?php $__env->stopSection(); ?>
<?php endif; ?>

<?php echo $__env->make('layouts.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/hihoteli/domains/hihotel.org/laravel/resources/views/pardakht/inquiry.blade.php ENDPATH**/ ?>